/**
 * @file Temps.h
 * @brief Déclaration de TTemps et temps
 *
 * @internal
 * @todo Créer une documentation avec des exemples pour ce module
 */

#ifndef Temps_h
#define Temps_h

// configTime
#include <Arduino.h>
// time
#include <time.h>

//! Temps dans le RTC du NodeMCU. Voir https://en.wikipedia.org/wiki/Unix_time
typedef uint32_t TTemps;

//! Obtenir l'heure en secondes depuis le Unix epoch
#define temps() time(nullptr)

//================================================================
// Constantes de temps en secondes
//================================================================
//! Nombre de secondes dans une seconde
#define SECONDE 1
//! Nombre de secondes dans une minute
#define MINUTE (60 * SECONDE)
//! Nombre de secondes dans une heure
#define HEURE (60 * MINUTE)
//! Nombre de secondes dans une journée
#define JOUR (24 * HEURE)

//================================================================
// Constantes de temps en millisecondes
//================================================================
//! Nombre de millisecondes dans une seconde
#define SECONDE_MS 1000
//! Nombre de millisecondes dans une minute
#define MINUTE_MS (60 * SECONDE_MS)
//! Nombre de millisecondes dans une heure
#define HEURE_MS (60 * MINUTE_MS)
//! Nombre de millisecondes dans une journée
#define JOUR_MS (24 * HEURE_MS)

#endif
