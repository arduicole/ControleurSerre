/**
 * @file Broches.h
 * @brief Déclaration des broches associées aux différentes fonctions
 */

#ifndef Broches_h
#define Broches_h

// Définition des broches
#include <Arduino.h>

//! Interrupteur de sélection du mode (automatique/manuel)
static const int SW_MODE = D5;
//! Niveau logique de SW_MODE en mode manuel
#define MODE_MANUEL HIGH
//! Niveau logique de SW_MODE en mode automatique
#define MODE_AUTOMATIQUE LOW

//! Relais pour le contrôle de la direction des moteurs
static const int DIRECTION_MOTEURS[2] = {10, D8};
//! Broche de l'interrupteur de direction de moteurs
static const int SW_DIRECTION_MOTEUR = A0;
//! Valeur minimale retournée par analogRead(SW_DIRECTION_MOTEURS)
//! signifiant de fermer les côtés ouvrants
static const int ETAT_DIRECTION_FERMER = (1024 / 3) * 1;
//! Valeur minimale retournée par analogRead(SW_DIRECTION_MOTEURS)
//! signifiant de laisser au neutre les côtés ouvrants
static const int ETAT_DIRECTION_NEUTRE = (1024 / 3) * 2;
//! Valeur minimale retournée par analogRead(SW_DIRECTION_MOTEURS)
//! signifiant de ouvrir les côtés ouvrant
static const int ETAT_DIRECTION_OUVRIR = (1024 / 3) * 0;

//! Relais pour la sélection du moteur
static const int SELECTION_MOTEUR = 3;
//! Interrupteur pour le contrôle de la sélection du moteur en mode manuel
static const int SW_SELECTION_MOTEUR = D0;
//! Niveau logique de SW_SELECTION_MOTEUR lorsque le moteur 0 est sélectionné
#define MOTEUR0_SELECTIONNE LOW
//! Niveau logique de SW_SELECTION_MOTEUR lorsque le moteur 1 est sélectionné
#define MOTEUR1_SELECTIONNE HIGH

//! Relais pour la commande du chauffage.
static const int CHAUFFAGE = D3;
//! Interrupteur pour la commande du chauffage en mode manuel.
static const int SW_CHAUFFAGE = D2;

//! Relais pour la commande de la ventilation
static const int VENTILATION = D4;
//! Interrupteur pour la commande de la ventilation en mode manuel.
static const int SW_VENTILATION = D1;

//! Niveau logique de SW_CHAUFFAGE ou SW_VENTILATION lorsqu'il faut allumer le
//! chauffage/ventilation
#define SW_ALLUMER LOW
//! Niveau logique de SW_CHAUFFAGE ou SW_VENTILATION lorsqu'il faut éteindre le
//! chauffage/ventilation
#define SW_ETEINDRE HIGH

//! Capteurs de température et d'humidité DHT22
static const int CAPTEURS[2] = {D6, D7};

#endif
