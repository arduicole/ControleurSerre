/**
 * @file TAlertesSynchronisees.h
 * @brief Déclaration de TCowString, TAlerte et TAlertesSynchronisees
 * TAlertesSynchronisees
 */

#ifndef TAlertesSynchronisees_h
#define TAlertesSynchronisees_h

#include "../TSynchroniseurTemps/TSynchroniseurTemps.h"
#include <Arduino.h>
#include <list>
#include <stdint.h>

//! Nombre d'alertes qui sont gardées en mémoire.
//! Si le nombre d'alertes dépasse ce nombre, l'alerte la plus vieille est
//! enlevée.
#define NB_ALERTES_MAX 10

//! ID d'une alerte temporaire.
#define ID_ALERTE_TEMPORAIRE 0
//! ID demande nouvelle alerte permanente.
#define ID_ALERTE_PERMANENTE 1

//! Possède une référence vers une chaîne de caractère
//! ou possède un String
class TCowString {
private:
    //! Si le pointeur est alloué dynamiquement.
    bool FAlloueDynamiquement;

    //! Pointeur vers la chaîne de caractères.
    const char *FPtr;

public:
    TCowString(const char *c, bool allouerDynamiquement = false,
               int32_t taille = -1);
    TCowString(String &s);
    TCowString(const TCowString &cw);

    ~TCowString();

    const char *c_str() const;
};

//! Noms des différents niveaux de sévérité des alertes.
//! @see TUrgenceAlerte
extern const char *NIVEAUX_URGENCES[];

//! Niveau de sévérité d'une alerte.
enum TUrgenceAlerte {
    uaBasse,
    uaMoyenne,
    uaElevee,

    uaPremier = uaBasse,
    uaDernier = uaElevee,
    uaDefaut = uaBasse,
};

//! Représente une alerte ayant un ID unique,
//! une date, un titre et un message
class TAlerte {
private:
    //! Identifiant unique de cette alerte.
    uint32_t FId;
    //! Moment auquel l'alerte est survenue.
    uint64_t FDate;

    //! Sévérité de l'alerte.
    TUrgenceAlerte FUrgence;

public:
    //! Titre de l'alerte
    TCowString Titre;
    //! Message de l'alerte
    TCowString Message;

    TAlerte(TCowString titre, TCowString message, TUrgenceAlerte urgence,
            uint32_t id, uint64_t date);

    uint32_t Id();
    bool EstTemporaire();
    void RendreTemporaire();

    uint64_t Date();
    void SynchroniserDate(int32_t delta);

    TUrgenceAlerte Urgence();
};

//! Gère une liste d'alertes et s'assure de la
//! synchronisation de la date/heure de chacune.
class TAlertesSynchronisees : public IObservateurHeure {
private:
    //! Liste d'alertes
    std::list<TAlerte> FAlertes;
    //! Dernier ID donné à une alerte.
    uint32_t FDernierId;

    //! Si le temps de toutes les alertes est synchronisé
    bool FSynchronise;

public:
    TAlertesSynchronisees(TSynchroniseurTemps &syncTemps);

    std::list<TAlerte> &Alertes();
    std::list<TAlerte>::iterator AlertesDepuis(uint64_t date);

    uint32_t AjouterAlerte(TCowString titre, TCowString message,
                           TUrgenceAlerte urgence,
                           uint32_t id = ID_ALERTE_TEMPORAIRE);
    TAlerte *AlerteId(uint32_t id);

    bool EstSynchronise() const;
    void SynchroniserHeure(int32_t delta) override;
};

#endif /* TAlertesSynchronisees_h */
