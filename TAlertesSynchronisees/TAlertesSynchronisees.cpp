/**
 * @file TAlertesSynchronisees.cpp
 * @brief Implémentation de TCowString, TAlerte et TAlertesSynchronisees
 */

#include "TAlertesSynchronisees.h"
#include "../Temps/Temps.h"
#include <sys/time.h>

using namespace std;

//! Noms des différents niveaux de sévérité des alertes.
//! @see TUrgenceAlerte
const char *NIVEAUX_URGENCES[] = {
    "basse",
    "moyenne",
    "élevée",
};

//==============================================================================
// Implémentation de TCowString
//==============================================================================

/**
 * @brief Initialise un nouveau TCowString avec un tampon alloué dynamiquement
 *
 */
TCowString::TCowString(String &s) : TCowString(s.c_str(), true, s.length()) {
}

/**
 * @brief Initialise un nouveau TCowString qui peut être
 * une référence vers c ou une copie de c si.
 *
 */
TCowString::TCowString(
    //! Chaîne de caractères constante
    const char *c,
    //! Si la chaîne doit être copiée et allouée dynamiquement
    bool allouerDynamiquement,
    //! Taille de la chaîne de caractères
    //! Si la taille est inconnue et doit être calculée avec strlen,
    //! assignez -1 à cette variable.
    int32_t taille) :
    FAlloueDynamiquement(allouerDynamiquement),
    FPtr(c) {
    if (allouerDynamiquement) {
        if (taille < 0 && c != nullptr) {
            taille = strlen(c);
        }

        if (taille > 0 && c != nullptr) {
            char *copie = (char *)malloc(taille + 1);
            memcpy(copie, c, taille);
            copie[taille] = 0;
            FPtr = copie;
        } else {
            FPtr = nullptr;
        }
    }
}

/**
 * @brief Copie l'objet donné.
 * Si le TCowString est une référence, la référence est copiée.
 * Si le TCowString est alloué dynamiquement, un deuxième buffer alloué
 * dynamiquement est créé.
 *
 */
TCowString::TCowString(const TCowString &cw) :
    TCowString(cw.FPtr, cw.FAlloueDynamiquement) {
}

/**
 * @brief Si le string est alloué dynamiquement, son tampon est libéré.
 *
 */
TCowString::~TCowString() {
    if (FAlloueDynamiquement) {
        free((void *)FPtr);
    }
}

/**
 * @brief Retourne une référence vers la chaîne de caractère
 *
 * @return Une référence vers la chaîne de caractère
 */
const char *TCowString::c_str() const {
    return FPtr;
}

//==============================================================================
// Implémentation de TAlerte
//==============================================================================

/**
 * @brief Créer une nouvelle alerte avec les propriétés données
 *
 */
TAlerte::TAlerte(
    //! Titre de l'alerte
    TCowString titre,
    //! Message de l'alerte
    TCowString message,
    //! Sévérité de l'alerte.
    //! Si la valeur donnée est invalide, uaDefaut est utilisé.
    TUrgenceAlerte urgence,
    //! ID de l'alerte
    uint32_t id,
    //! Date à laquelle l'alerte est survenue
    uint64_t date) :
    FId(id),
    FDate(date), FUrgence(urgence), Titre(titre), Message(message) {
    if (urgence < uaPremier || urgence > uaDernier) {
        FUrgence = uaDefaut;
    }
}

/**
 * @brief Retourne l'ID de l'alerte
 *
 * @return l'ID de l'alerte
 * @retval 0 L'alerte est temporaire
 * @retval * L'alerte est permanente
 */
uint32_t TAlerte::Id() {
    return FId;
}

/**
 * @brief Retourne si cette alerte est temporaire
 *
 * @return Si cette alerte est temporaire
 * @retval true Cette alerte est temporaire
 * @retval false Cette alerte est permanente
 */
bool TAlerte::EstTemporaire() {
    return FId == ID_ALERTE_TEMPORAIRE;
}

/**
 * @brief Rend cette alerte temporaire en modifiant son ID pour
 * ID_ALERTE_TEMPORAIRE
 *
 */
void TAlerte::RendreTemporaire() {
    FId = ID_ALERTE_TEMPORAIRE;
}

/**
 * @brief Retourne la date à laquelle l'alerte est survenue
 *
 * @return La date à laquelle l'alerte est survenue
 */
uint64_t TAlerte::Date() {
    return FDate;
}

/**
 * @brief Synchronise la date de l'alerte en ajoutant le delta donné à l'heure
 * de l'alerte
 *
 */
void TAlerte::SynchroniserDate(
    //! Delta à ajouter à l'heure pour l'ajuster
    int32_t delta) {
    // Si la date est inférieure à UINT32_MAX ms.
    // NOTE: cette valeur est due au fait que le contrôleur ne peut pas rester
    // allumé pendant plus de UINT32_MAX ms. Si la date/heure est
    // supérieure à cette valeur, on peut être sûr que la date a été
    // synchronisée. Nous pourrions aussi utiliser la date/heure de la
    // compilation, mais cela nécessite que la date/heure du système de
    // compilation soit bien ajustés.
    if (FDate < UINT32_MAX) {
        FDate += ((int64_t)delta) * 1000;
    }
}

/**
 * @brief Retourne le niveau de sévérité de l'alerte
 *
 * @return Le niveau de sévérité de l'alerte
 */
TUrgenceAlerte TAlerte::Urgence() {
    return FUrgence;
}

//==============================================================================
// Implémentation de TAlertesSynchronisees
//==============================================================================

/**
 * @brief Initialise une liste d'alertes désynchronisées et vides et
 * s'enregistre auprès du synchroniseur d'heure pour observer les
 * changements de l'heure.
 *
 */
TAlertesSynchronisees::TAlertesSynchronisees(
    //! Synchroniser l'heure de l'alerte.
    TSynchroniseurTemps &syncTemps) :
    FDernierId(ID_ALERTE_PERMANENTE),
    FSynchronise(false) {
    // Observer les changements de l'heure.
    syncTemps.AjouterObservateur(*this);
}

/**
 * @brief Retourne la liste d'alertes survenues
 *
 * @return La liste d'alertes survenues
 */
std::list<TAlerte> &TAlertesSynchronisees::Alertes() {
    return FAlertes;
}

/**
 * @brief Retourne un itérateur vers la première alerte qui est plus récente que
 * la date données
 * @note Si la date donnée est 0, toutes les alertes sont retournées
 *
 * @return La liste d'alertes survenues
 */
std::list<TAlerte>::iterator TAlertesSynchronisees::AlertesDepuis(
    //! Date dont les alertes retournées doivent être plus récentes.
    //! Si la date est 0, toutes les alertes sont retournées, même si la date de
    //! l'alerte est 0.
    uint64_t date) {
    std::list<TAlerte>::iterator resultat = FAlertes.begin();
    if (date != 0) {
        while (resultat->Date() <= date && resultat != FAlertes.end()) {
            resultat++;
        }
    }
    return resultat;
}

/**
 * @brief Ajoute une nouvelle alerte à la liste d'alertes et s'assure que la
 * liste d'alertes ne dépasse pas une taille maximale.
 *
 * @return l'ID de l'alerte créée
 */
uint32_t TAlertesSynchronisees::AjouterAlerte(
    //! Titre de l'alerte
    TCowString titre,
    //! Message de l'alerte
    TCowString message,
    //! Niveau de sévérité de l'alerte
    TUrgenceAlerte urgence,
    //! Id de l'alerte
    uint32_t id) {
    // S'assurer que la liste ne dépasse pas la taille maximale
    // en supprimant les plus vieux éléments.
    while (FAlertes.size() >= NB_ALERTES_MAX) {
        // Trouver la plus vieille alerte temporaire.
        auto it = FAlertes.begin();
        while (it != FAlertes.end() && !it->EstTemporaire()) {
            it++;
        }

        // S'il n'y a aucune alerte temporaire,
        // utiliser la plus vieille alerte permanente.
        if (it == FAlertes.end()) {
            it = FAlertes.begin();
        }

        // Supprimer la vieille alerte.
        FAlertes.erase(it);
    }

    bool creerAlerte = true;
    if (id == ID_ALERTE_PERMANENTE) {
        FDernierId += 1;
        id = FDernierId;
    } else if (id != ID_ALERTE_TEMPORAIRE) {
        // Si l'alerte existe encore
        TAlerte *alerte = AlerteId(id);
        if (alerte != nullptr) {
            // Modifier l'alerte.
            alerte->Titre = std::move(titre);
            alerte->Message = std::move(message);
            creerAlerte = false;
        } else {
            // Créer un nouvel ID pour la nouvelle alerte.
            FDernierId += 1;
            id = FDernierId;
        }
    }

    // Si l'alerte n'existe pas déjà.
    if (creerAlerte) {
        // La créer et l'ajouter à la liste.
        struct timeval tv;
        gettimeofday(&tv, NULL);
        uint64_t ms =
            ((uint64_t)tv.tv_sec) * 1000 + ((uint64_t)tv.tv_usec) / 1000;
        FAlertes.push_back(TAlerte(titre, message, urgence, id, ms));
    }

    return id;
}

/**
 * @brief Retourne l'alerte avec l'ID donné ou nullptr
 * si elle ne peut pas être trouvée
 *
 * @return L'alerte avec l'ID donné
 * @retval nullptr L'alerte avec l'ID donné n'a pas pu être trouvée
 * @retval * Pointeur vers l'alerte trouvée
 */
TAlerte *TAlertesSynchronisees::AlerteId(uint32_t id) {
    TAlerte *resultat = nullptr;

    // Si l'id donné pourrait être celui d'une alerte.
    if (id > ID_ALERTE_PERMANENTE) {
        // Trouver cette alerte.
        for (auto it = FAlertes.begin();
             resultat == nullptr && it != FAlertes.end(); it++) {
            TAlerte *alerte = &*it;
            if (alerte->Id() == id) {
                resultat = alerte;
            }
        }
    }

    return resultat;
}

/**
 * @brief Retourne si le temps de toutes les alertes est synchronisé
 *
 * @return Si le temps de toutes les alertes est synchronisé
 */
bool TAlertesSynchronisees::EstSynchronise() const {
    return FSynchronise;
}

/**
 * @brief Synchronise l'heure de toutes les alertes (à condition que ce soit la
 * première synhcronisation depuis que cet objet est abonné aux changements de
 * l'heure)
 * @note Implémentation de IObservateurHeure::SynchroniserHeure
 *
 */
void TAlertesSynchronisees::SynchroniserHeure(
    //! Différence entre l'heure précédente et l'heure synchronisée.
    //! Le delta donné peut être ajouté à n'importe quelle heure qui n'était
    //! pas synchronisée pour qu'elle le soit.
    int32_t delta) {
    if (!FSynchronise) {
        for (TAlerte &alerte : FAlertes) {
            alerte.SynchroniserDate(delta);
        }
        FSynchronise = true;
    }
}
