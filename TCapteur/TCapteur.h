/**
 * @file TCapteur.h
 * @brief Déclaration de TCapteur, TMesureCapteur et TSommaireMesures
 */

#ifndef TCapteur_h
#define TCapteur_h

#include "../TAlertesSynchronisees/TAlertesSynchronisees.h"

// DHT et DHT22
#include <DHTesp.h>

//! Nombre de mesures à court terme pour les capteurs
#define NB_MESURES 15

//! Représente la mesure d'un capteur
typedef float TMesureCapteur;

//! Représente le sommaire d'une liste des mesures d'un capteur
struct TSommaireMesures {
    //! Mesure minimale
    TMesureCapteur Min;
    //! Mesure maximale
    TMesureCapteur Max;
    //! Moyenne des mesures
    TMesureCapteur Moyenne;

    TSommaireMesures();
    TSommaireMesures(TMesureCapteur *mesures, unsigned int nbMesures);
    TSommaireMesures(TSommaireMesures *sommaires, unsigned int nbSommaires);
};

//! Contient un nombre maximal C de mesures de
//! capteurs et n'utilise aucune allocation dynamique
template <unsigned int C> class TMesures {
private:
    //! Liste de mesures
    TMesureCapteur FMesures[C];
    //! Nombre de mesures prises
    unsigned int FTaille;
    //! Index dans la liste de mesures
    //! Cet index ne dépasse jamais C
    unsigned int FIndex;

public:
    /**
     * @brief Calcul le minimum, le maximum, et la moyenne de la liste des
     * mesures données et les met dans ses propriétés
     * @note Si la taille de mesures est 0, Min, Max et Moyenne ne seront pas
     * des nombres.
     *
     */
    TMesures() {
        Reinitialiser();
    }

    /**
     * @brief Calcul le minimum, le maximum, et la moyenne de la liste des
     * mesures données et les met dans ses propriétés
     * @note Si la taille de mesures est 0, Min, Max et Moyenne ne seront pas
     * des nombres.
     *
     */
    void Reinitialiser() {
        FTaille = 0;
        FIndex = 0;
    }

    /**
     * @brief Ajoute la mesure à la liste de mesures
     *
     */
    void Ajouter(
        // Mesure à ajouter
        TMesureCapteur mesure) {
        FMesures[FIndex] = mesure;

        // Incrémenter l'index pour la prochaine écriture
        // dans le tampon de mesures.
        FIndex++;
        if (FIndex >= C)
            FIndex = 0;

        // Si la taille est inférieure à la taille du tampon.
        if (FTaille < C)
            FTaille++;
    }

    /**
     * @brief Calcul et retourne le sommaire des mesures
     *
     * @return Le sommaire des mesures
     */
    TSommaireMesures Sommaire() {
        return TSommaireMesures(FMesures, FTaille);
    }
};

//! Historique de mesures.
//! Ne conserve que la somme des mesures et le nombre de mesure et
//! permet seulement de calculer la moyenne.
class TMesuresHistoriques {
private:
    //! Nombre de mesures ajoutées à la somme
    unsigned int FNbMesures;
    //! Somme des mesures ajoutées
    TMesureCapteur FSommeMesures;

public:
    TMesuresHistoriques();

    TMesureCapteur Moyenne();
    void Ajouter(TMesureCapteur mesure);

    void Reinitialiser();
    unsigned int NbMesures();
};

//! Gère un seul capteur et ses données à court terme et à long terme
class TCapteur {
private:
    //! Code d'erreur de cette alerte.
    //! 0 signifie qu'il n'y a aucun problème avec les capteurs.
    //! 1 signifie que la lecture de la température ne fonctionne pas.
    //! 2 signifie que la lecture de l'humidité ne fonctionne pas.
    //! 3 signifie que la lecture de la température et de l'humidité ne
    //! fonctionne pas.
    uint8_t FCodeErreur;
    //! ID de l'alerte.
    uint32_t FIdAlerte;

    //! Nombre de lectures ratées d'affilée.
    uint32_t FLecturesRatees;

    //! Liste d'alertes synchronisées.
    TAlertesSynchronisees &FAlertesSync;

    void AjouterAlerte(uint8_t codeErreur, uint32_t id);

public:
    //! Nom de ce capteur
    const char *Nom;

    //! Capteur DHT22
    DHTesp Capteur;

    //! Mesures de température à court terme
    TMesures<NB_MESURES> TemperatureCourtTerme;
    //! Mesures d'humidité à court terme
    TMesures<NB_MESURES> HumiditeCourtTerme;

    //! Mesures historique de la température.
    TMesuresHistoriques TemperatureHistorique;
    //! Mesures historique d'humidité.
    TMesuresHistoriques HumiditeHistorique;

    TCapteur(const char *nom, int broche, TAlertesSynchronisees &alertesSync);

    int Lire();

    void ReinitialiserHistorique();
};

#endif
