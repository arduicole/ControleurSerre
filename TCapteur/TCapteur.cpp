/**
 * @file TCapteur.cpp
 * @brief Implémentation de TCapteur
 */

#include "TCapteur.h"

// INFINITY
#include <math.h>

//! Modèle du capteur
#define TYPE_CAPTEUR DHTesp::DHT22

//==============================================================================
// Implémentation de TSommaireMesures
//==============================================================================

/**
 * @brief Initialise Min à INFINITY, MAX à -INFINITY et Moyenne à 0
 *
 */
TSommaireMesures::TSommaireMesures() {
    Min = INFINITY;
    Max = -INFINITY;
    Moyenne = 0.0;
}

/**
 * @brief Calcul le minimum, le maximum, et la moyenne de la liste des mesures
 * données et les met dans ses propriétés
 * @note  Si la taille de mesures est 0, Min, Max et Moyenne ne seront pas des
 * nombres réels. Voir le constructeur par défaut pour leurs valeurs.
 *
 */
TSommaireMesures::TSommaireMesures(
    //! Mesures à analyser
    TMesureCapteur *mesures,
    //! Nombre de mesures à analyser
    unsigned int nbMesures) :
    TSommaireMesures() {
    // Pour chaque mesure
    for (unsigned int i = 0; i < nbMesures; i++, mesures++) {
        // Ajouter la mesure à la moyenne.
        Moyenne += *mesures;

        // Assigner la mesure à la valeur minimale si elle est inférieure à
        // celle-ci.
        if (*mesures < Min)
            Min = *mesures;

        // Assigner la mesure à la valeur maximale si elle est supérieure à
        // celle-ci.
        if (*mesures > Max)
            Max = *mesures;
    }

    // Diviser la somme des mesures par leur nombre pour obtenir la moyenne des
    // mesures.
    Moyenne /= nbMesures;
}

/**
 * @brief Calcul la moyenne des moyennes, le maximum des maximums et le minimum
 * des minimums de tous les sommaires
 * @note Si la taille de mesures est 0, Min, Max et Moyenne ne seront pas des
 * nombres réels. Voir le constructeur par défaut pour leurs valeurs.
 * @note Si au moins une des moyennes est un nombre normal, la moyenne sera un
 * nombre normal.
 *
 */
TSommaireMesures::TSommaireMesures(
    //! Liste des sommaires de mesures
    TSommaireMesures *sommaires,
    //! Nombre de sommaires dans la liste
    unsigned int nbSommaires) :
    TSommaireMesures() {
    size_t nbReels = 0; // Nombre de nombres réels
                        // dans les moyennes des sommaires.
    // Pour chaque sommaire
    for (size_t i = 0; i < nbSommaires; i++, sommaires++) {
        // Ajouter la moyenne du sommaire à la moyenne.
        if (isnormal(sommaires->Moyenne)) {
            Moyenne += sommaires->Moyenne;
            nbReels++;
        }

        // Assigner le minimum du sommaire à la valeur minimale si elle est
        // inférieure à celle-ci.
        if (sommaires->Min < Min)
            Min = sommaires->Min;

        // Assigner le maximum du sommaire à la valeur maximale si elle est
        // supérieure à celle-ci.
        if (sommaires->Max > Max)
            Max = sommaires->Max;
    }

    // Diviser la somme des moyennes par le nombre de valeurs additionnées.
    Moyenne /= nbReels;
}

//==============================================================================
// Implémentation de TMesuresHistoriques
//==============================================================================

/**
 * @brief Initialise un historique de mesure sans aucune mesure
 *
 */
TMesuresHistoriques::TMesuresHistoriques() {
    Reinitialiser();
}

/**
 * @brief Calcul la moyenne de l'historique
 *
 */
TMesureCapteur TMesuresHistoriques::Moyenne() {
    return FSommeMesures / FNbMesures;
}

/**
 * @brief Ajoute une mesure à la somme de mesures et incrémente le nb de mesures
 *
 */
void TMesuresHistoriques::Ajouter(
    //! Mesure à ajouter
    TMesureCapteur mesure) {
    // Incrémenter le nb de mesures.
    FNbMesures++;

    // Ajouter la mesure à la somme de mesures.
    FSommeMesures += mesure;
}

/**
 * @brief Supprime toutes les mesures de l'historique
 *
 */
void TMesuresHistoriques::Reinitialiser() {
    FNbMesures = 0;
    FSommeMesures = 0.0;
}

/**
 * @brief Nombre de mesures dans l'historique
 *
 */
unsigned int TMesuresHistoriques::NbMesures() {
    return FNbMesures;
}

//==============================================================================
// Implémentation de TCapteur
//==============================================================================

//! Messages d'alertes associés aux différents codes d'erreur de TAlerteCapteur
static const char *MESSAGES_ALERTE[] = {
    "La température et l'humidité de ce capteur peuvent de nouveau "
    "être lu", // 0
    "La température du capteur ne peut pas être lue. Vérifiez qu'il "
    "est bien "
    "connecté", // 1
    "L'humidité du capteur ne peut pas être lue. Vérifiez qu'il est "
    "bien "
    "connecté", // 2
    "La température et l'humidité du capteur ne peuvent pas être lues. "
    "Vérifiez qu'il est bien connecté", // 3
};

/**
 * @brief Initialise un capteur DHT22 sur la broche compatible
 *
 */
TCapteur::TCapteur(
    //! Nom de ce capteur
    const char *nom,
    //! Broche du capteur
    int broche,
    //! Liste d'alertes synchronisées
    TAlertesSynchronisees &alertesSync) :
    FCodeErreur(0),
    FIdAlerte(ID_ALERTE_PERMANENTE), FLecturesRatees(0),
    FAlertesSync(alertesSync), Nom(nom) {
    Capteur.setup(broche, TYPE_CAPTEUR);
}

/**
 * @brief Créé l'alerte pour le code d'erreur et l'ID donnés, l'ajoute à la
 * liste d'alertes synchronisées et enregistre son ID
 *
 */
void TCapteur::AjouterAlerte(
    //! Code d'erreur de l'alerte
    uint8_t codeErreur,
    //! ID de l'alerte à créer
    uint32_t id) {
    String titre = codeErreur != 0
                       ? String("Erreur de lecture du capteur ") + Nom
                       : String("Lecture du capteur ") + Nom + " rétablie";
    FIdAlerte = FAlertesSync.AjouterAlerte(titre, MESSAGES_ALERTE[codeErreur],
                                           uaMoyenne, id);
}

/**
 * @brief Lit la température et l'humidité du capteur et l'ajoute à ses mesures
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 La température n'a pas pu être lue
 * @retval 2 L'humidité n'a pas pu être lue
 * @retval 3 La température et l'humidité n'ont pas pues être lues
 */
int TCapteur::Lire() {
    int codeErreur = 0;

    // Lire la température et l'ajouter aux mesures de température.
    TMesureCapteur temperature = Capteur.getTemperature();
    if (isnan(temperature)) {
        codeErreur = 1;
    } else {
        TemperatureCourtTerme.Ajouter(temperature);
        TemperatureHistorique.Ajouter(temperature);
    }

    // Lire l'humidité et l'ajouter aux mesures d'humidité.
    TMesureCapteur humidite = Capteur.getHumidity();
    if (isnan(humidite)) {
        codeErreur += 2;
    } else {
        HumiditeCourtTerme.Ajouter(humidite);
        HumiditeHistorique.Ajouter(humidite);
    }

    if (codeErreur != 0) {
        FLecturesRatees++;
        if (FLecturesRatees < LECTURES_RATEES_MAX) {
            codeErreur = 0;
        }
    } else {
        FLecturesRatees = 0;
    }

    if (codeErreur != 0) {
        TemperatureCourtTerme.Reinitialiser();
        HumiditeCourtTerme.Reinitialiser();

        // S'il n'y a pas encore d'alerte pour cette erreur.
        TAlerte *alerte = FAlertesSync.AlerteId(FIdAlerte);
        if (alerte == nullptr) {
            AjouterAlerte(codeErreur, ID_ALERTE_PERMANENTE);
        }
        // Sinon s'il y a une alerte, mais que la cause a changée
        else if (alerte != nullptr && codeErreur != FCodeErreur) {
            // Rendre l'alerte précédente temporaire.
            alerte->RendreTemporaire();

            // Créer une nouvelle alerte.
            AjouterAlerte(codeErreur, ID_ALERTE_PERMANENTE);
        }
    }
    // S'il y avait une alerte, mais qu'elle est réglée
    else if (FCodeErreur != 0) {
        // Rendre l'alerte temporaire.
        TAlerte *alerte = FAlertesSync.AlerteId(FIdAlerte);
        if (alerte != nullptr) {
            alerte->RendreTemporaire();
        }

        // Créer une alerte temporaire pour signaler que le problème est réglé.
        AjouterAlerte(codeErreur, ID_ALERTE_PERMANENTE);
    }
    FCodeErreur = codeErreur;

    return codeErreur;
}

/**
 * @brief Réinitialise l'historique de température et d'humidité
 *
 */
void TCapteur::ReinitialiserHistorique() {
    TemperatureHistorique.Reinitialiser();
    HumiditeHistorique.Reinitialiser();
}
