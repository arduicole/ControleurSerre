/**
 * @file TReinitialisation.h
 * @brief Déclaration de TReinitialisation
 */

#ifndef TReinitialisation_h
#define TReinitialisation_h

#include "../ITache/ITache.h"
#include "../TAlertesSynchronisees/TAlertesSynchronisees.h"

//! Réinitialise le contrôleur après un intervalle de temps donné
//! lorsqu'il est l'heure donnée
class TReinitialisation : public ITache {
public:
    //! Durée en ms avant la réinitialisation du contrôleur.
    const TTemps Duree;
    //! Heure de la réinitialisation entre 0 et 23.
    const TTemps Heure;

    TReinitialisation(TTemps duree, uint8_t heure,
                      TAlertesSynchronisees &alertesSync);

    int Executer() override;
};

#endif
