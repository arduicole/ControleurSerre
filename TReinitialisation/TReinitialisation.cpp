/**
 * @file TReinitialisation.cpp
 * @brief Implémentation de TReinitialisation
 */

#include "TReinitialisation.h"
#include <Arduino.h>

//! Titre des alertes de redémarrage du contrôleur.
const char *TITRE_ALERTE = "Redémarrage du contrôleur";

//! Messages d'alertes pour les différentes raisons de réinitialisation.
//! @see rst_reason
const char *MSG_ALERTES[] = {
    "Le contrôleur vient de démarrer après avoir été alimenté. Si vous ne "
    "venez pas juste de le connecter à une alimentation, cela signifie "
    "qu'il a manqué d'électricité. Ceci est probablement du à une panne "
    "d'électricité.", // REASON_DEFAULT_RST
    "Le chien de garde matériel du contrôleur s'est déclenché. Cela signifie "
    "que le programme du contrôleur est resté bloqué trop longtemps au même "
    "endroit. Ceci ne devrait pas se produire à moins que le chien de garde "
    "logiciel ait été désactivé, ce qui ne devrait pas être le cas. Veuillez "
    "rapporter ceci aux développeurs.", // REASON_WDT_RST
    "Le programme du contrôleur a planté. Veuillez rapporter ceci aux "
    "développeurs.", // REASON_EXCEPTION_RST
    "Le chien de garde logiciel du contrôleur s'est déclenché. Cela signifie "
    "que le programme du contrôleur est resté bloqué trop longtemps au même "
    "endroit. Veuillez rapporter ce bogue aux développeurs.", // REASON_SOFT_WDT_RST
    nullptr, // REASON_SOFT_RESTART, Ne pas créer d'alertes pour les
             // réintialisations déclenchées par TReinitialisation. Ceci est
             // fait pour éviter de déranger le client pour une simple
             // réinitialisation périodique. Sinon, si elle survenait pendant la
             // nuit, elle pourrait le réveiller.
    "Le contrôleur s'est réveillé d'une mise en veille. Le contrôleur ne devra "
    "pourtant jamais se mettre en veille. Veuillez rapporter ceci aux "
    "développeurs.", // REASON_DEEP_SLEEP_AWAKE
    "Le contrôleur a été manuellement réinitialisé. Si vous n'avez pas "
    "manuellement réinitialisé le contrôleur en appuyant sur son bouton de "
    "réinitialisation ou en le reprogrammant, ceci est un bogue. Veuillez donc "
    "le rapporter aux développeurs. Sinon, ignorez ce message.", // REASON_EXT_SYS_RST
};

/**
 * @brief Initialise le réinitialisateur avec l'intervalle
 * de réinitialisation donné et créer une alerte avec la raison de la dernière
 * réinitialisation
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 */
TReinitialisation::TReinitialisation(
    //! Durée en ms avant la réinitialisation du contrôleur.
    TTemps duree,
    //! Heure de la réinitialisation entre 0 et 23.
    uint8_t heure,
    //! Liste d'alertes synchronisées.
    //! Ceci permet de générer une alerte au démarrage en
    //! fonction de la raison de la réinitialisation.
    TAlertesSynchronisees &alertesSync) :
    Duree(duree),
    Heure(heure) {
    const char *msgAlerte = MSG_ALERTES[ESP.getResetInfoPtr()->reason];
    if (msgAlerte != nullptr) {
        alertesSync.AjouterAlerte(TITRE_ALERTE, msgAlerte, uaBasse);
    }
}

/**
 * @brief Réinitialise le NodeMCU si le délai est écoulé et qu'il est
 * l'heure de la réinitialisation.
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 */
int TReinitialisation::Executer() {
    // Si le délai est écoulé.
    if (millis() >= Duree) {
        // S'il est l'heure de redémarré le contrôleur.
        time_t temps = temps();
        if ((uint8_t)localtime(&temps)->tm_hour == Heure) {
            ESP.restart();
        }
    }

    return 0;
}
