/**
 * @file TConnexionWiFi.cpp
 * @brief Implémentation de la classe TConnexionWiFi.
 */

#include "TConnexionWiFi.h"
#include <ESP8266WiFi.h>

/**
 * @brief Initialise le nom du routeur, le mot de passe et le le temps entre
 * chaque demande pour la connexion WiFi
 *
 */
TConnexionWiFi::TConnexionWiFi(
    //! SSID du routeur.
    const char *ssid,
    //! Mot de passe du routeur.
    const char *motDePasse,
    //! SSID du point d'accès à créé par le contrôleur lorsqu'il ne
    //! peut pas se connecter au réseau donné.
    const char *ssidPA,
    //! Temps entre chaque demande pour la connexion au routeur.
    TTemps periode) :
    ATachePeriodique(periode) {

    FSSID = ssid;
    FMotDePasse = motDePasse;
    FSSIDPointAcces = ssidPA;

    // Configurer et activer le point d'accès.
    WiFi.softAP(FSSIDPointAcces);
    FPointAccesActive = true;

    // Démarer une tentative de connexion.
    Serial.print("Connexion à: ");
    Serial.println(ssid);
    WiFi.begin(FSSID, FMotDePasse);

    // Initialiser à non envoyer.
    FIPEnvoye = false;
}

/**
 * @brief Tâche à faire periodiquement
 *
 * @return Un code d'erreur
 * @retval 0 Le contrôleur est connecté au WiFi
 *           Le point d'accès est désactivé.
 * @retval 1 Le contrôleur démarre une tentative de connexion au WiFi.
 *           Le point d'accès est activé.
 * @retval 2 Le contrôleur est en cour de tentative de connexion au réseau WiFi.
 *           Le point d'accès est activé.
 */
int TConnexionWiFi::ExecuterPeriodique() {
    int codeErreur = 0;
    int statut = WiFi.status();

    // Si on est connecté au WiFi
    if (statut == WL_CONNECTED) {
        // Désactiver le point d'accès.
        if (FPointAccesActive) {
            WiFi.softAPdisconnect(true);
            FPointAccesActive = false;
        }

        if (!FIPEnvoye) {
            Serial.println("Adresse IP: ");
            Serial.println(WiFi.localIP());
            FIPEnvoye = true;
        }
    }
    // Sinon si on est pas déjà en train de se connecter
    else if (statut != WL_IDLE_STATUS) {
        // Activer le point d'accès.
        if (!FPointAccesActive) {
            WiFi.softAP(FSSIDPointAcces);
            FPointAccesActive = true;
        }

        // Démarer une tentative de connexion.
        Serial.println("Tentative de connexion...");
        WiFi.begin(FSSID, FMotDePasse);

        codeErreur = 1;
    }
    // Sinon le contrôleur est déjà en train de se connecter
    else {
        codeErreur = 2;
    }

    return codeErreur;
}
