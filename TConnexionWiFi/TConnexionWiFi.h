/**
 * @file TConnexionWiFi.h
 * @brief Déclaration de classe TConnexionWiFi.
 */

#ifndef TCONNEXIONWIFI_H
#define TCONNEXIONWIFI_H

#include "../ATachePeriodique/ATachePeriodique.h"

//! Connecte le contrôleur au WiFi lors qu'il s'en déconnecte et
//! active un point d'accès temporaire tant qu'il n'est pas connecté.
class TConnexionWiFi : public ATachePeriodique {
private:
    //! Nom du reseau.
    const char *FSSID;
    //! Mot de passe du reseau.
    const char *FMotDePasse;
    //! Si l'IP a été écrit sur le port sériel.
    bool FIPEnvoye;

    //! SSID du point d'accès à créé par le contrôleur lorsqu'il ne
    //! peut pas se connecter au réseau donné.
    const char *FSSIDPointAcces;
    //! Si le point d'accès est activé ou non.
    bool FPointAccesActive;

public:
    TConnexionWiFi(const char *, const char *, const char *, TTemps);

    int ExecuterPeriodique();
};

#endif
