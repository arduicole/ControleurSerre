<!--
Avant de choisir ce modèle, assurez-vous que vous ne pourriez pas utiliser
un des autres modèles disponibles.

N'hésitez cependant pas à choisir celui-ci.
Il existe parce que les autres ne répondent pas à tous les besoins.

Vous pouvez entre autre l'utiliser si vous avez une question à poser en y ajoutant le label ~"Question".
-->
