<!--
Ce document est une adaptation de https://github.com/jekyll/jekyll/tree/master/.github/ISSUE_TEMPLATE

Afin que ce modèle ne vous empêche pas de proposer des fonctionnalités,
sachez que vous n'avez pas besoin de remplir tous les champs ci-dessous,
mais nous vous demandons de tous les laisser afin que nous puissions les remplir plus tard.

Assurez-vous que cette fonctionnalité n'ait pas déjà été proposée à ce lien: https://gitlab.com/arduicole/ControleurSerre/issues?scope=all&label_name[]=Fonctionnalit%C3%A9. Si c'est le cas, vous pouvez l'appuyer en mettant un pouce en l'air ou en demandant à ce qu'elle soit réouverte si elle a été fermée.
-->

## Description

<!--
Résumé de cette fonctionnalité en un seul paragraphe
-->

## Utilité

<!--
Expliquez:
  * Pourquoi voulez-vous cette fonctionnalité?
  * Comment sera-t-elle utilisée?
-->

## Explications d'utilisation

<!--
Expliquez cette fonctionnalité comme si elle était déjà implémentée et inclue
dans le projet et que vous étiez en train de l'enseigner à quelqu'un.
Cela signifie:
    * Introduire de nouveaux concepts
    * Expliquer la fonctionnalité en utilisant des exemples
    * Si applicable, expliquez comment migrer à cette nouvelle fonctionnalité
    * Si applicable, donner des exemples de messages d'erreur

Si ceci est une petite fonctionnalité, vous pouvez omettre cette section.
-->

## Explications d'implémentation

<!--
Ceci est la partie technique de la demande.
Expliquez comment implémenter cette fonctionnalité de manière à ce que:
    * Son interaction avec le reste des fonctionnalité est claire.
    * L'implémentation soit raisonnablement claire.
    * Les cas spéciaux sont expliquez avec des exemples

Si vous n'avez pas d'idée sur l'implémentation, vous pouvez omettre cette section.
-->

## Inconvénient

<!--
  Pourquoi ne devrait-on pas implémenter cette fonctionnalité?
  Ex.: Performance, Sécurité, etc.

  N'hésitez pas à soulever des problèmes ici, ce n'est qu'en en discutant qu'on parviendra à les résoudre.
-->

## Questions non résolues

<!--
Quels problèmes reliés à cette fonctionnalité ne sont pas couvert
par votre solution et devront être réglés plus tard.
-->

/label ~"Fonctionnalité"
