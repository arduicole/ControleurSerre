<!--
Afin que ce modèle ne vous empêche pas de soumettre des bugs,
sachez que vous n'avez pas besoin de remplir tous les champs ci-dessous,
mais nous vous demandons de tous les laisser afin que nous puissions les remplir plus tard.

Assurez-vous que le bug que vous voulez signaler n'existe pas déjà en consultant ce lien: https://gitlab.com/arduicole/ControleurSerre/issues?scope=all&label_name[]=Bug
-->

## Résumé

<!--
Résumé du problème rencontré
-->

## Étapes pour le reproduire

<!--
Ceci est très important, mais n'hésitez pas à créer cet issue si vous n'avez pas trouver de procédure pour reproduire ce problème.

Cette section peut inclure du code ou des liens vers du code si nécessaire.
-->

1. Étape 1
2. Étape 2
3. etc.

## Logs, capture d'écran ou photos

<!--
Mettez le texte des logs entre des blocs de code (```) pour qu'ils soient plus lisibles
-->

## Solutions possibles

<!--
Donnez nous des pistes de solutions si vous avez une idéé.
Mettez également des liens vers les lignes de codes qui pourraient causer ce problème.
-->

/label ~"Bug"
