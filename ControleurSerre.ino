/**
  Contrôleur de serre
  Name: ControleurSerre
  Purpose:
  Target: NodeMCU v1.0

  @author TSO Cégep de l'Outaouais
  @version 0.9.0
*/

#pragma GCC diagnostic error "-Wall"
#pragma GCC diagnostic error "-Wextra"

#include "AActionneur/AActionneur.h"
#include "ATachePeriodique/ATachePeriodique.cpp"
#include "Broches/Broches.h"
#include "Config.h"
#include "ITache/ITache.h"
#include "TActionneurBool/TActionneurBool.cpp"
#include "TAlertesSynchronisees/TAlertesSynchronisees.cpp"
#include "TCapteur/TCapteur.cpp"
#include "TClientAlertesFCM/TClientAlertesFCM.cpp"
#include "TClientHistorique/TClientHistorique.cpp"
#include "TConnexionWiFi/TConnexionWiFi.cpp"
#include "TConsigne/TConsigne.h"
#include "TCycle/TCycle.h"
#include "TFournisseurConsigne/TFournisseurConsigne.cpp"
#include "TGestionnaireMode/TGestionnaireMode.cpp"
#include "TGroupeCapteurs/TGroupeCapteurs.cpp"
#include "TJour/TJour.h"
#include "TModeAsservissement/TModeAsservissement.cpp"
#include "TModeCalibration/TModeCalibration.cpp"
#include "TModeControleDistance/TModeControleDistance.cpp"
#include "TModeCycle/TModeCycle.cpp"
#include "TModeManuel/TModeManuel.cpp"
#include "TMoteur/TMoteur.cpp"
#include "TReinitialisation/TReinitialisation.cpp"
#include "TRoute/TRoute.cpp"
#include "TRoute/TRouteImpl.cpp"
#include "TSelectionMoteur/TSelectionMoteur.cpp"
#include "TServeur/TServeur.cpp"
#include "TSynchroniseurTemps/TSynchroniseurTemps.cpp"
#include "TVerifTempCritique/TVerifTempCritique.cpp"
#include "Temps/Temps.cpp"

// Valider l'intervalle de réinitialisation.
static_assert(INTERVALLE_REINITIALISATION <= (UINT32_MAX - JOUR_MS),
              "L'intervalle de réinitialisation doit être inférieur à "
              "4 208 567 295 ms (2^32 - JOUR_MS) soit environ 48.7 jours");

// Valider l'heure de la réinitialisation.
static_assert(
    HEURE_REINITIALISATION < 24,
    "L'heure de la réinitialisation doit être entre 0 et 23 inclusivement");

TMoteur moteurs[NB_MOTEURS] = {
    TMoteur(DIRECTION_MOTEURS, DUREE_OUVERTURE_ROLLUP_1,
            DUREE_FERMETURE_ROLLUP_1, DELAI_CHANGEMENT_DIRECTION_ROLLUP_1,
            HYSTERESIS_ROLLUP_1),
    TMoteur(DIRECTION_MOTEURS, DUREE_OUVERTURE_ROLLUP_2,
            DUREE_FERMETURE_ROLLUP_2, DELAI_CHANGEMENT_DIRECTION_ROLLUP_2,
            HYSTERESIS_ROLLUP_2),
};
TSelectionMoteur selectionMoteur(moteurs, SELECTION_MOTEUR,
                                 DELAI_CHANGEMENT_OPTIONNEL_SELECTION_MOTEUR);
TFournaise fournaise(CHAUFFAGE);
TVentilateur ventilateur(VENTILATION);
TConnexionWiFi
    ConnexionWiFi(NOM_WIFI, MOT_DE_PASSE_WIFI, NOM_POINT_ACCES,
                  15 * SECONDE_MS // Ne pas trop diminuer ce délai, car sinon la
                                  // connexion au point d'accès sera impossible
    );
TSynchroniseurTemps Synchronisation(FUSEAU_HORAIRE, DECALAGE_HEURE_ETE);
TAlertesSynchronisees AlertesSync(Synchronisation);

#if ALERTES_UTILISER_FCM
TClientAlertesFCM clientAlertes(AlertesSync, CLE_SERVEUR_FCM, TOPIC_FCM,
                                DELAI_ENVOI_ALERTES);
#endif

TCapteur capteurs[NB_CAPTEURS] = {
    TCapteur(NOM_CAPTEUR_1, CAPTEURS[0], AlertesSync),
    TCapteur(NOM_CAPTEUR_2, CAPTEURS[1], AlertesSync),
};
TGroupeCapteurs groupeCapteurs(capteurs, 2 * SECONDE_MS);
TReinitialisation Reinitialisation(INTERVALLE_REINITIALISATION,
                                   HEURE_REINITIALISATION, AlertesSync);
TClientHistorique historique(capteurs, INTERVALLE_SYNCHRONISATION);
TFournisseurConsigne fournisseurConsigne(Synchronisation);
TVerifTempCritique verifTempCritique(30 * SECONDE_MS, groupeCapteurs,
                                     fournisseurConsigne, AlertesSync);

//---------------------------------------------------------------
// Déclaration des différents modes.
//---------------------------------------------------------------
TGestionnaireMode gestionnaireMode;
TModeCalibration modeCalibration(selectionMoteur);
TModeManuel modeManuel(selectionMoteur, fournaise, ventilateur);
TModeControleDistance modeCtrlDistance(selectionMoteur, fournaise, ventilateur);
TModeCycle modeCycle(selectionMoteur, fournaise, ventilateur);
TModeAsservissement modeAsservissement(INTERVALLE_ASSERVISSEMENT,
                                       selectionMoteur, fournaise, ventilateur,
                                       fournisseurConsigne, AlertesSync);

//---------------------------------------------------------------
// Déclaration du serveur et de ses routes
//---------------------------------------------------------------
TServeur serveur;
TRouteCertificat routeCertificat(serveur, "/");
TRouteMode routeMode(gestionnaireMode, serveur, "/mode");
TRouteCapteurs routeCapteurs(groupeCapteurs, serveur, "/capteurs");
TRouteActionneurs routeActionneurs(gestionnaireMode, modeCtrlDistance,
                                   selectionMoteur, ventilateur, fournaise,
                                   serveur, "/actionneurs");
TRouteAlertes routeAlertes(AlertesSync, serveur, "/alertes");
TRouteTypeJour routeTypeJour(fournisseurConsigne, serveur, "/type-jour");
TRouteConsignes routeConsignes(fournisseurConsigne, serveur, "/consignes");
TRouteCycle routeCycle(fournisseurConsigne, modeCycle, gestionnaireMode,
                       serveur, "/cycle");

//! Taille du EEPROM.
#define TAILLE_EEPROM 4096

void setup() {
    // Initialiser la communication sérielle.
    // Serial.begin(115200); // Il faut désactiver la communication sérielle
    // afin de pouvoir utiliser les broches D9 et D10
    // (RX et TX)

    // Initialiser le EEPROM.
    EEPROM.begin(TAILLE_EEPROM);

    // Restaurer les données du EEPROM.
    fournisseurConsigne.Restaurer();
    modeCtrlDistance.Restaurer();

    Serial.println();
    // Se connecter au WiFi.
    Serial.print("Connexion à ");
    Serial.println(NOM_WIFI);

    // Ajouter les modes au gestionnaire de mode.
    gestionnaireMode.AjouterMode(modeManuel);
    gestionnaireMode.AjouterMode(modeCalibration);
    gestionnaireMode.AjouterMode(modeCtrlDistance);
    gestionnaireMode.AjouterMode(modeCycle);
    gestionnaireMode.AjouterMode(modeAsservissement);

    // Enregistrer les routes pour le serveur.
    serveur.addHandler(&routeCertificat);
    serveur.addHandler(&routeMode);
    serveur.addHandler(&routeCapteurs);
    serveur.addHandler(&routeActionneurs);
    serveur.addHandler(&routeAlertes);
    serveur.addHandler(&routeTypeJour);
    serveur.addHandler(&routeConsignes);
    serveur.addHandler(&routeCycle);

    // Démarrer le serveur.
    serveur.begin();
}

void loop() {
    ConnexionWiFi.Executer();
    Synchronisation.Executer();
    Lecture();

    gestionnaireMode.Executer();

    Reinitialisation.Executer();

    verifTempCritique.Executer();

#if ALERTES_UTILISER_FCM
    clientAlertes.Executer();
#endif

    if (historique.Executer() != 0) {
        AlertesSync.AjouterAlerte(
            "Impossible d'enregistrer les données des capteurs sur GDrive",
            "Les données des capteurs n'ont pas pu être enregistrées sur "
            "votre GDrive",
            uaBasse);
    }
    serveur.Executer();
}

void Lecture() {
    groupeCapteurs.Executer();
    modeAsservissement.Temperature =
        groupeCapteurs.SommaireTemperatureCourtTerme();
}
