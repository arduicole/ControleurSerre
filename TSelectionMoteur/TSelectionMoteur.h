/**
 * @file TSelectionMoteur.h
 * @brief Déclaration de TSelectionMoteur
 */

#ifndef TSelectionMoteur_h
#define TSelectionMoteur_h

#include "../AActionneur/AActionneur.h"
#include "../ITache/ITache.h"
#include "../TMoteur/TMoteur.h"

//! Nombre de moteurs dans la sélection
#define NB_MOTEURS 2

//! Gère la sélection du moteur à déplacer
class TSelectionMoteur : public ITache, public AActionneur<unsigned int> {
private:
    //! Heure à laquelle le moteur actuel a été sélectionné
    TTemps FDebutSelection;

public:
    //! Broche pour sélectionné le moteur
    int BrocheSelection;
    //! Liste des moteurs à sélectionner
    TMoteur (&Moteurs)[NB_MOTEURS];
    //! Délai en millisecondes entre les changements optionnels de direction
    TTemps DelaiChangementsOptionnels;

    TSelectionMoteur(TMoteur (&moteurs)[NB_MOTEURS], int brocheSelection,
                     TTemps delaiChangementsOptionnels);

    using AActionneur<unsigned int>::Etat;
    int Etat(unsigned int moteur);

    int Executer();
};

#endif
