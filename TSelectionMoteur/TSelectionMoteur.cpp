/**
 * @file TSelectionMoteur.cpp
 * @brief Implémentation de TSelectionMoteur
 */

#include "TSelectionMoteur.h"
#include <Arduino.h>

/**
 * @brief Créé un gestionnaire de sélection de moteur et des moteurs avec les
 * mêmes broches de direction données et sélectionne le premier moteur
 *
 */
TSelectionMoteur::TSelectionMoteur(
    //! Liste des moteurs à sélectionner
    TMoteur (&moteurs)[NB_MOTEURS],
    //! Broche pour sélectionner le moteur
    int brocheSelection,
    //! Délai en millisecondes entre les changements optionnels de direction
    TTemps delaiChangementsOptionnels) :
    FDebutSelection(0),
    BrocheSelection(brocheSelection), Moteurs(moteurs),
    DelaiChangementsOptionnels(delaiChangementsOptionnels) {
    // Configurer la broche de sélection en sortie.
    pinMode(brocheSelection, OUTPUT);

    // Sélectionner le premier moteur.
    FEtat = -1;
    Etat(0);
}

/**
 * @brief Change l'index du moteur sélectionné
 * @note Implémentation de int AActionneur::Etat(TEtat)
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 Index invalide
 */
int TSelectionMoteur::Etat(
    //! Index du moteur à sélectionner
    unsigned int moteur) {
    // Initialiser un code d'erreur au code de réussite.
    int codeErreur = 0;

    if (moteur != FEtat) {
        if (moteur < NB_MOTEURS) {
            digitalWrite(BrocheSelection, moteur == 0 ? LOW : HIGH);

            FEtat = moteur;
            FDebutSelection = millis();
        } else {
            codeErreur = 1;
        }
    }

    return codeErreur;
}

/**
 * @brief Déplace le moteur sélectionné. Si le moteur sélectionné a fini son
 * déplacement, sélectionne l'autre moteur
 * @note Implémentation de ITache::Executer()
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 */
int TSelectionMoteur::Executer() {
    // Obtenir le moteur sélectionné.
    TMoteur &moteur = Moteurs[FEtat];

    // Exécuter le moteur sélectionné.
    // Si le moteur n'a pas besoin de se déplacer.
    if (moteur.Executer() <= 0) {
        // Obtenir l'autre moteur et son index.
        int indexAutreMoteur = FEtat == 0 ? 1 : 0;
        TMoteur &autreMoteur = Moteurs[indexAutreMoteur];

        if (autreMoteur.BesoinDeplacer() ||
            (millis() - FDebutSelection > DelaiChangementsOptionnels)) {
            // Selectionner l'autre moteur.
            Etat(indexAutreMoteur);
            autreMoteur.Executer();
        }
    }

    return 0;
}
