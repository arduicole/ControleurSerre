/**
 * @file TModeManuel.h
 * @brief Déclaration de TModeManuel
 */

#ifndef TModeManuel_h
#define TModeManuel_h

#include "../Broches/Broches.h"
#include "../TActionneurBool/TActionneurBool.h"
#include "../TGestionnaireMode/TGestionnaireMode.h"
#include "../TSelectionMoteur/TSelectionMoteur.h"
#include <Arduino.h>

//! Le mode manuel est le plus important de tous.
//! C'est le mode dans lequel se trouve le système lorsque l'interrupteur de
//! mode manuel est enclenché. Le système ne fait alors que déterminer
//! l'état des actionneurs en fonction de l'état des interrupteurs du
//! boîtier. Ce mode a préséance sur tous les autres.
class TModeManuel : public AMode {
private:
    //! Sélection du moteur.
    TSelectionMoteur &FSelectionMoteur;
    //! Fournaise.
    TFournaise FFournaise;
    //! Ventilateur.
    TVentilateur FVentilateur;

public:
    //! Nom de ce mode tel que retourné par #Nom.
    static const char *NOM_MODE;

    TModeManuel(TSelectionMoteur &selectionMoteurs, TFournaise &fournaise,
                TVentilateur &ventilateur);

    bool DesireSelection(bool premier) override;

    int Executer() override;
};

#endif
