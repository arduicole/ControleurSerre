/**
 * @file TModeManuel.cpp
 * @brief Implémentation de TModeManuel
 */

#include "TModeManuel.h"

// Définition des constantes statiques de TModeManuel.
const char *TModeManuel::NOM_MODE = "manuel";

/**
 * @brief Initialise le mode manuel avec #NOM_MODE comme nom et avec les
 * actionneurs donnés
 * @note Les broches configurées dans Broches.h sont utilisées pour les
 * interrupteurs
 *
 */
TModeManuel::TModeManuel(
    //! Sélection du moteur.
    TSelectionMoteur &selectionMoteurs,
    //! Fournaise.
    TFournaise &fournaise,
    //! Ventilateur.
    TVentilateur &ventilateur) :
    AMode(NOM_MODE),
    FSelectionMoteur(selectionMoteurs), FFournaise(fournaise),
    FVentilateur(ventilateur) {
    // Configurer les broches d'interrupteurs en entrée.
    pinMode(SW_MODE, INPUT);
    pinMode(SW_DIRECTION_MOTEUR, INPUT);
    pinMode(SW_SELECTION_MOTEUR, INPUT);
    pinMode(SW_CHAUFFAGE, INPUT);
    pinMode(SW_VENTILATION, INPUT);
}

/**
 * @brief Retourne si l'interrupteur de mode manuel est enclenché
 *
 * @return Si l'interrupteur de mode manuel est enclenché
 * @retval true L'interrupteur est enclenché
 * @retval false L'interrupteur n'est pas enclenché
 */
bool TModeManuel::DesireSelection(bool) {
    return digitalRead(SW_MODE) == MODE_MANUEL;
}

/**
 * @brief Lit l'état de chaque interrupteur et l'utilise pour contrôler
 * l'état des actionneurs
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur n'est survenue
 * @retval -1 Le mode manuel doit être éteint
 */
int TModeManuel::Executer() {
    // Obtenir l'index du moteur à sélectionner.
    int indexMoteur = digitalRead(SW_SELECTION_MOTEUR) == MOTEUR1_SELECTIONNE;

    // Arrêter le moteur qui n'est pas sélectionné.
    FSelectionMoteur.Moteurs[!indexMoteur].Etat(dmNeutre);

    // Sélectionner le moteur à l'index.
    FSelectionMoteur.Etat(indexMoteur);

    // Obtenir la direction de moteurs sélectionnée.
    TDirectionMoteur direction = dmOuvrir;
    int etatSwDirection = analogRead(SW_DIRECTION_MOTEUR);
    delay(5); // Voir
              // https://gitlab.com/arduicole/ControleurSerre/issues/25
    if (etatSwDirection >= ETAT_DIRECTION_NEUTRE)
        direction = dmNeutre;
    else if (etatSwDirection >= ETAT_DIRECTION_FERMER)
        direction = dmFermer;

    // Diriger le moteur sélectionné dans la direction sélectionnée.
    FSelectionMoteur.Moteurs[indexMoteur].Etat(direction);

    // Changer l'état du ventilateur en fonction de l'état de l'interrupteur
    // de ventilation.
    FVentilateur.Etat(digitalRead(SW_VENTILATION) == SW_ALLUMER);

    // Changer l'état de la fournaise en fonction de l'état de
    // l'interrupteur de chauffage.
    FFournaise.Etat(digitalRead(SW_CHAUFFAGE) == SW_ALLUMER);

    return DesireSelection(false) ? 0 : -1;
}
