/**
 * @file AActionneur.h
 * @brief Implémentation de AActionneur
 */

#ifndef AActionneur_h
#define AActionneur_h

//! Contrôle l'état d'un actionneur et fournit une méthode pour changer l'état
//! du capteur
template <typename TEtat> class AActionneur {
protected:
    //! Etat de l'actionneur
    TEtat FEtat;

public:
    TEtat Etat();

    /**
     * @brief Change l'état de l'actionneur
     *
     * @return Code d'erreur
     * @retval 0 Aucune erreur
     * @retval * Voir l'implémentation de la classe
     */
    virtual int Etat(
        //! Nouvel état de l'actionneur
        TEtat etat) = 0;
};

/**
 * @brief Retourne l'état de l'actionneur
 *
 * @return État de l'actionneur
 */
template <typename TEtat> inline TEtat AActionneur<TEtat>::Etat() {
    return FEtat;
}

#endif
