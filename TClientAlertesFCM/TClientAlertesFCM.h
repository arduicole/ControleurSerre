/**
 * @file TClientAlertesFCM.h
 * @brief Déclaration de TClientAlertesFCM
 */

#ifndef TClientAlertesFCM_h
#define TClientAlertesFCM_h

#include "../ATachePeriodique/ATachePeriodique.h"
#include "../TAlertesSynchronisees/TAlertesSynchronisees.h"
#include <WiFiClientSecure.h>
#include <stdint.h>

//! Envoi périodiquement les nouvelles alertes à un topic donné.
class TClientAlertesFCM : public ATachePeriodique {
private:
    //! Liste d'alertes.
    TAlertesSynchronisees &FAlertesSync;
    //! Heure de la dernière alerte envoyée.
    uint64_t FDerniereAlerteEnvoyee;

    //! Session SSL du client FCM.
    BearSSL::Session FSession;

public:
    //! Clé du serveur pour avoir l'autorisation d'envoyer des messages FCM.
    const char *CleServeur;
    //! Topic auquel envoyer les alertes.
    const char *Topic;

    TClientAlertesFCM(TAlertesSynchronisees &alertesSync,
                      const char *cleServeur, const char *topic,
                      TTemps periode);

protected:
    int ExecuterPeriodique() override;
};

#endif /* TClientAlertesFCM_h */
