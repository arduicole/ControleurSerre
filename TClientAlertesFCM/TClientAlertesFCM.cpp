/**
 * @file TClientAlertesFCM.cpp
 * @brief Implémentation de TClientAlertesFCM
 *
 */

// Sérialize NaN à null plutôt qu'à NaN qui n'est pas compatible avec le
// standard JSON et plusieurs librairies de désérialization
#define ARDUINOJSON_ENABLE_NAN 0
// Active l'utilisation de nombres de 64 bits
#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>

using ARDUINOJSON_NAMESPACE::EscapeSequence;
using ARDUINOJSON_NAMESPACE::TextFormatter;
using ARDUINOJSON_NAMESPACE::Writer;

#include "../Config.h"
#include "TClientAlertesFCM.h"
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>

//! URL pour envoyer les messages d'alertes.
static const char URL_FCM[] = "https://fcm.googleapis.com/fcm/send";

/**
 * @brief Créer un nouvel objet TClientAlertesFCM
 *
 */
TClientAlertesFCM::TClientAlertesFCM(
    //! Liste d'alertes.
    TAlertesSynchronisees &alertesSync,
    //! Clé du serveur pour avoir l'autorisation d'envoyer des messages FCM.
    const char *cleServeur,
    //! Topic auquel envoyer les alertes.
    const char *topic,
    //! Interval d'envoi des nouvelles alertes.
    TTemps periode) :
    ATachePeriodique(periode),
    FAlertesSync(alertesSync), FDerniereAlerteEnvoyee(0),
    CleServeur(cleServeur), Topic(topic) {
}

/**
 * @brief Envoi un message FCM contenant la liste d'alertes survenues depuis
 * le dernier envoi des alertes.
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur, les nouvelles alertes ont pues être envoyées à FCM
 * @retval 1 La connexion au serveur de Firebase n'a pas pue être établie. Le
 * problème peut être dû au réseau ou il peut être dû à un manque de mémoire.
 * @retval 2 Le serveur a retourné un code d'erreur autre que 200
 */
int TClientAlertesFCM::ExecuterPeriodique() {
    int codeErreur = 0;

    auto debut = FAlertesSync.AlertesDepuis(FDerniereAlerteEnvoyee);
    auto fin = FAlertesSync.Alertes().end();

    if (WiFi.status() == WL_CONNECTED && FAlertesSync.EstSynchronise() &&
        debut != fin) {
        WiFiClientSecure clientWifi;
        HTTPClient clientHttp;

        clientWifi.setInsecure();
        clientWifi.setSession(&FSession);
        clientWifi.setBufferSizes(4096, 1024);
        if (clientHttp.begin(clientWifi, URL_FCM)) {
            uint64_t dateDerniereAlerte = 0;
            TUrgenceAlerte urgenceMax = uaBasse;
            String alertesJson;
            for (auto alerte = debut; alerte != fin; alerte++) {
                TUrgenceAlerte urgence = alerte->Urgence();
                dateDerniereAlerte = alerte->Date();

                if (alerte != debut) {
                    alertesJson += ',';
                }
                alertesJson += "{\"titre\":\"";
                alertesJson += alerte->Titre.c_str();
                alertesJson += "\",\"message\":\"";
                alertesJson += alerte->Message.c_str();
                alertesJson += "\",\"urgence\":\"";
                alertesJson += NIVEAUX_URGENCES[urgence];
                alertesJson += "\",\"date\":";
                TextFormatter<Writer<String>>(Writer<String>(alertesJson))
                    .writePositiveInteger(dateDerniereAlerte);
                alertesJson += "}";

                if (urgence > urgenceMax) {
                    urgenceMax = urgence;
                }
            }

            String message = "{\"priority\":\"high\",\"data\":{\"alertes\":\"[";
            const char *c = alertesJson.c_str();
            while (*c != 0) {
                char carSpecial = EscapeSequence::escapeChar(*c);
                if (carSpecial) {
                    message += '\\';
                    message += carSpecial;
                } else {
                    message += *c;
                }
                c++;
            }
            alertesJson.clear();
            message += "]\"},\"to\":\"/topics/";
            message += Topic;
            message += "\"}";

            clientHttp.addHeader("Authorization", String("key=") + CleServeur);
            clientHttp.addHeader("Content-Type", "application/json");

            // Si la requête n'a pas été reçu
            if (clientHttp.POST(message) != 200) {
                codeErreur = 2;
            } else if (dateDerniereAlerte != 0) {
                FDerniereAlerteEnvoyee = dateDerniereAlerte;
            }
            clientHttp.end();
            clientWifi.stop();
        } else {
            codeErreur = 1;
        }
    }

    return codeErreur;
}
