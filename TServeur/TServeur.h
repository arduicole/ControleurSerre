/**
 * @file TServeur.h
 * @brief Déclaration de TServeur
 */

#ifndef TServeur_h
#define TServeur_h

#include "../ITache/ITache.h"
#include <ESP8266WebServerSecure.h>
#include <ESP8266WiFi.h>

// Sérialize NaN à null plutôt qu'à NaN qui n'est pas compatible avec le
// standard JSON et plusieurs librairies de désérialization
#define ARDUINOJSON_ENABLE_NAN 0
// Active l'utilisation de nombres de 64 bits
#define ARDUINOJSON_USE_LONG_LONG 1
#include <ArduinoJson.h>

//! Type de contenu (mime type) pour du texte brut
static const char *TYPE_CONTENU_TEXTE = "text/plain";
//! Type de contenu (mime type) pour du JSON
static const char *TYPE_CONTENU_JSON = "application/json";

//=============================================================================
// Codes d'erreurs HTTP. Voir
// https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
//=============================================================================
//! Code d'erreur interne du serveur
static const int CODE_ERREUR_INTERNE = 500;
//! Code d'erreur retourné lorsque la requête reçue est malformée ou contient
//! des données invalides
static const int CODE_REQUETE_INVALIDE = 400;
//! Code d'erreur d'autorisation, le client a entré le mauvais mot de passe
//! pour accéder au serveur
static const int CODE_NON_AUTHORISE = 401;
//! Code d'erreur retourné lorsque l'élément demandé n'a pas été trouvé ou les
//! paramètres passés sont invalides
static const int CODE_NON_TROUVE = 404;
//! Code de méthode invalide
static const int CODE_METHODE_INVALIDE = 405;
//! Code de conflit. Ceci indique qu'une requête est valide, mais que l'état du
//! serveur.
static const int CODE_CONFLIT = 409;
//! Code de réussite avec contenu
static const int CODE_OK = 200;
//! Code de réussite sans contenu
static const int CODE_AUCUN_CONTENU = 204;

//! Gère les communication avec l'application Android
class TServeur : public ITache, public ESP8266WebServerSecure {
public:
    using TJsonDoc = StaticJsonDocument<1024>;

    TServeur();
    int Executer();

    int EnvoyerDocument();
    int RecevoirDocument();

    int EnvoyerTexte(const char *texte = NULL, const int codeErreur = CODE_OK,
                     const char *typeContenu = TYPE_CONTENU_TEXTE);
    int EnvoyerCorpsVide();
    int MethodeIncorrecte();
    int ErreurInterne();

    TJsonDoc &Document();

private:
    //! Buffer utilisé pour décoder les requêtes en JSON et pour formatter
    //! les réponses en JSON
    TJsonDoc FDocument;
};

/**
 * @brief Retourne le document JSON du serveur
 *
 * @return Le document JSON du serveur
 */
inline TServeur::TJsonDoc &TServeur::Document() {
    return FDocument;
}

#endif
