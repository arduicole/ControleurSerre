REM Genere les cles d'encryption pour le serveur ainsi que le fichier Cles.h
REM Utiliser winget ou chocolatey pour installer openssl
REM winget: https://github.com/microsoft/winget-cli
REM chocolatey: https://chocolatey.org/docs/installation

set /a duree = 30 * 365

REM Generer les cles
openssl ecparam -out privee.key -name prime256v1 -genkey
openssl req -new -key privee.key -out certificat.csr -sha256
openssl x509 -req -days %duree% -in certificat.csr -signkey privee.key -out certificat.crt -sha256

REM Generer Cles.h
echo static const char CLE_PRIVEE[] = R^"EOF( > Cles.h
type privee.key >> Cles.h
echo )EOF^"; >> Cles.h

echo. >> Cles.h

echo static const char CERTIFICAT[] = R^"EOF( >> Cles.h
type certificat.crt >> Cles.h
echo )EOF^"; >> Cles.h
