/**
 * @file TServeur.cpp
 * @brief Implémentation de TServeur
 */

#include "TServeur.h"
#include "../Config.h"
#include "Cles.h"

//! Clé privée du serveur.
static const BearSSL::PrivateKey clePrivee(CLE_PRIVEE);
//! Certificat du serveur.
static const BearSSL::X509List certificat(CERTIFICAT);

/**
 * @brief Initialise le serveur sur le port dans la configuration et configure
 * sa clé d'encryption.
 * @note Ne pas oublier d'ensuite le démarrer avec la méthode 'begin'.
 *
 */
TServeur::TServeur() :
    //! Initialiser le serveur sur le port dans la configuration
    ESP8266WebServerSecure(PORT_SERVEUR) {

    // Configurer les clés du serveur.
    getServer().setECCert(&certificat, BR_KEYTYPE_KEYX | BR_KEYTYPE_SIGN,
                          &clePrivee);
}

/**
 * @brief Envoie le document au client ou une erreur si le document n'a pas pu
 * être formatté
 *
 * @return Un code de statut HTTP
 * @retval 200 Aucune erreur n'est survenue.
 * @retval 500 Le document JSON n'a pas pu être formatté.
 */
int TServeur::EnvoyerDocument() {
    int codeErreur;

    // Sérialiser le document.
    // Si le document n'a pas pu être sérialisé
    String serialise;
    if (serializeJson(FDocument, serialise) > 0) {
        // Envoyer le document sérialisé.
        codeErreur =
            EnvoyerTexte(serialise.c_str(), CODE_OK, TYPE_CONTENU_JSON);
    }
    // Sinon
    else {
        // Envoyer un code d'erreur interne au serveur.
        codeErreur = ErreurInterne();
    }

    return codeErreur;
}

/**
 * @brief Envoie le texte donné comme corps de la réponse avec un code de
 * réussite au client
 *
 * @return Un code de statut HTTP
 */
int TServeur::EnvoyerTexte(
    //! Texte à envoyer
    const char *texte,
    //! Statut HTTP de la réponse
    const int codeStatut,
    //! Type du contenu à envoyer (Content-Type), "text/plain" par défaut
    const char *typeContenu) {
    // Envoyer le texte au client.
    send(codeStatut, typeContenu, texte);
    return codeStatut;
}

/**
 * @brief Envoie un code HTTP de réussite sans corps au client
 *
 * @return Un code de statut HTTP
 * @retval CODE_AUCUN_CONTENU
 */
int TServeur::EnvoyerCorpsVide() {
    send(CODE_AUCUN_CONTENU);
    return CODE_AUCUN_CONTENU;
}

/**
 * @brief Retourne une erreur de méthode HTTP incorrecte au client
 *
 * @return Un code de statut HTTP
 * @retval CODE_METHODE_INVALIDE
 */
int TServeur::MethodeIncorrecte() {
    send(CODE_METHODE_INVALIDE);
    return CODE_METHODE_INVALIDE;
}

/**
 * @brief Retourne une erreur d'erreur interne au client
 *
 * @return Un code de statut HTTP
 * @retval CODE_ERREUR_INTERNE
 */
int TServeur::ErreurInterne() {
    send(CODE_ERREUR_INTERNE);
    return CODE_ERREUR_INTERNE;
}

/**
 * @brief Reçoit le document Json du client
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval CODE_REQUETE_INVALIDE Le document n'a pas été reçu ou n'a pas pu être
 * décodé
 */
int TServeur::RecevoirDocument() {
    int codeErreur = 0;

    String contenu = arg("plain");
    DeserializationError erreur = deserializeJson(FDocument, contenu);

    // Si le document n'est pas reçu
    if (erreur) {
        // Envoyer le code d'echec au programme appellant
        codeErreur = CODE_REQUETE_INVALIDE;
        send(codeErreur);
    }

    return codeErreur;
}

/**
 * @brief Traite une requête au serveur HTTP s'il y en a une
 * @note Implémentation de ITache::Executer
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 */
int TServeur::Executer() {
    handleClient();
    return 0;
}
