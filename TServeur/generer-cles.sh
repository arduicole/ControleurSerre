#!/bin/bash

# Génère les clés d'encryption pour le serveur ainsi que le fichier Cles.h

duree=$(expr 30 \* 365)

# Générer les clés
openssl ecparam -out privee.key -name prime256v1 -genkey
openssl req -new -key privee.key -out certificat.csr -sha256
openssl x509 -req -days $duree -in certificat.csr -signkey privee.key -out certificat.crt -sha256

# Générer Cles.h
echo "static const char CLE_PRIVEE[] = R\"EOF(" > Cles.h
cat privee.key >> Cles.h
echo ")EOF\";" >> Cles.h

echo "" >> Cles.h

echo "static const char CERTIFICAT[] = R\"EOF(" >> Cles.h
cat certificat.crt >> Cles.h
echo ")EOF\";" >> Cles.h
