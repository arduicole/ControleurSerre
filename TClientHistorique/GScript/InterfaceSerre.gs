//====================================================================================
// Created by Abdel-Gany Jr Odedele, 2019
//====================================================================================

/**
 * Titres des colonnes
 */
const TITRES_COLONNES = [
    ["Historique", ["Temps"]],
    ["Capteur 1/3", ["Température (°C)", "Humidité (HR)"]],
    ["Capteur 2/3", ["Température (°C)", "Humidité (HR)"]],
];

/**
 * Log le message d'erreur donné puis créer un TextOutput avec ce
 * message qui peut être retourné au client.
 *
 * @param message Message d'erreur
 */
function creerErreur(message) {
    Logger.log(message);
    return ContentService.createTextOutput(message);
}

/**
 * Valide les requêtes reçues et ajoute leurs informations au tableur donné
 *
 * @param e Requête reçue
 */
function doPost(e) {
    // Décoder le document JSON du corps de la requête.
    let donneesDecodees;
    try {
        donneesDecodees = JSON.parse(e.postData.contents);
    } catch (f) {
        return creerErreur(
            `Impossible de décoder le document JSON dans le corps de la requête: ${f.message}`
        );
    }
    if (!donneesDecodees) {
        return creerErreur(
            "La requête ne semble pas contenir de document JSON"
        );
    }

    // Obtenir la version de la requête.
    const version = donneesDecodees.format ? donneesDecodees.format : 0;

    // Obtenir le tableur.
    const tableur = SpreadsheetApp.openById(
        donneesDecodees.idTableur ? donneesDecodees.idTableur : ""
    );
    if (!tableur) {
        return creerErreur(
            `Le tableur avec l'ID "${donneesDecodees.idTableur}" n'a pas pu être ouvert`
        );
    }

    // Obtenir la bonne page du tableur.
    const page = tableur.getSheetByName(
        donneesDecodees.pageTableur ? donneesDecodees.pageTableur : ""
    );
    if (!page) {
        return creerErreur(
            `La page nommée "${donneesDecodees.pageTableur}" n'a pas pu être trouvée danse le tableur avec l'ID "${donneesDecodees.idTableur}"`
        );
    }

    // Si la page est vide.
    const derniereRangee = page.getLastRow();
    if (derniereRangee <= 2) {
        // Écrire le titre des colonnes au sommet.
        let i = 1;
        for (let colonne of TITRES_COLONNES) {
            const titre = colonne[0];
            const sousTitres = colonne[1];

            page.getRange(1, i).setValue(titre);
            if (sousTitres.length > 1) {
                page.getRange(1, i, 1, sousTitres.length).mergeAcross();
            }

            if (sousTitres.length > 0) {
                page.getRange(2, i, 1, sousTitres.length).setValue(sousTitres);
            }

            i += Math.max(sousTitres.length, 1);
        }
    }

    // Valider les valeurs.
    const valeurs = donneesDecodees.valeurs ? donneesDecodees.valeurs : [];
    const valeursValidees = [new Date()];
    for (let capteur of valeurs) {
        for (let mesure of capteur) {
            if (typeof mesure === "number") {
                valeursValidees.push(mesure);
            } else if (mesure === null) {
                valeursValidees.push(NaN);
            }
        }
    }
    if (valeursValidees.length != 5) {
        return creerErreur(
            `Les données reçues sont invalides: ${donneesDecodees.valeurs}"`
        );
    }

    // Enregistrer les données dans le tableur.
    const rangeeValeurs = Math.max(derniereRangee + 1, 3);
    page.getRange(rangeeValeurs, 1, 1, valeursValidees.length).setValues([
        valeursValidees,
    ]);
    SpreadsheetApp.flush();

    return ContentService.createTextOutput("Succès!");
}
