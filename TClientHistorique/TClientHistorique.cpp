/**
 * @file TClientHistorique.cpp
 * @brief Implémentation de TClientHistorique
 */

#include "TClientHistorique.h"
#include "../Config.h"
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

//! URL pour exécuter le GScript.
const String url = "https://script.google.com/macros/s/" ID_GSCRIPT "/exec";

//! Données à utiliser pour accéder au GScript.
const String JSON = "{\"idTableur\":\"" ID_TABLEUR "\","
                    "\"pageTableur\":\"" NOM_PAGE_TABLEUR "\","
                    "\"valeurs\":[";

/**
 * @brief Initialise une tâche pour envoyer l'historique des capteurs
 * à un intervalle donné au serveur Google Drive
 *
 */
TClientHistorique::TClientHistorique(
    //! Liste de capteurs dont il faut enregistrer l'historique
    TCapteur (&capteurs)[NB_CAPTEURS],
    //! Intervalle d'exécution de l'envoi de l'historique au serveur
    TTemps periode) :
    ATachePeriodique(periode),
    Capteurs(capteurs) {
}

/**
 * @brief Formatte le nombre donné en JSON. Si la valeur donnée n'est pas un
 * nombre, la valeur JSON "null" est retournée.
 *
 * @return Nombre formatté en JSON ou "null"
 */
inline String formatterNbJson(
    //! Nombre à formatter.
    TMesureCapteur nb) {
    String resultat;
    if (isnan(nb)) {
        resultat = "null";
    } else {
        resultat = nb;
    }
    return resultat;
}

/**
 * @brief Envoie l'historique de la température de chaque capteur au serveur
 * pour le stockage à long terme
 * @note Implémentation de ATachePeriodique::ExecuterPeriodique
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 Erreur de connexion
 * @retval 2 Erreur d'envoi
 */
int TClientHistorique::ExecuterPeriodique() {
    int codeErreur = 0;
    if (WiFi.status() == WL_CONNECTED) {
        WiFiClientSecure clientWifi;
        HTTPClient clientHttp;

        clientWifi.setInsecure();
        if (clientHttp.begin(clientWifi, url)) { // HTTPS
            String payload = JSON;
            for (int i = 0; i < NB_CAPTEURS; i++) {
                TCapteur &capteur = Capteurs[i];

                payload += '[';
                payload +=
                    formatterNbJson(capteur.TemperatureHistorique.Moyenne());
                payload += ',';
                payload +=
                    formatterNbJson(capteur.HumiditeHistorique.Moyenne());
                payload += ']';
                if (i < NB_CAPTEURS - 1) {
                    payload += ',';
                }
            }
            payload += "]}";

            // Si la requête n'a pas été reçu
            if (clientHttp.POST(payload) != 302) {
                codeErreur = 2;
            } else {
                // Réinitialiser l'historique de mesures des capteurs.
                for (TCapteur &capteur : Capteurs) {
                    capteur.TemperatureHistorique.Reinitialiser();
                    capteur.HumiditeHistorique.Reinitialiser();
                }
            }
            clientHttp.end();
        } else {
            codeErreur = 1;
        }
    }

    return codeErreur;
}
