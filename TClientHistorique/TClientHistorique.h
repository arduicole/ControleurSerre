/**
 * @file TClientHistorique.h
 * @brief Déclaration de TClientHistorique
 */

#ifndef TClientHistorique_h
#define TClientHistorique_h

#include "../ATachePeriodique/ATachePeriodique.h"
#include "../TCapteur/TCapteur.h"
#include "../TGroupeCapteurs/TGroupeCapteurs.h"

//! Gère les données moyennes de la dernière heure des capteurs et les envoie
//! sur Google Drive pour leur stockage à long terme
class TClientHistorique : public ATachePeriodique {
public:
    //! Liste de capteurs dont il faut enregistrer l'historique.
    TCapteur (&Capteurs)[NB_CAPTEURS];

    TClientHistorique(TCapteur (&capteurs)[NB_CAPTEURS], TTemps periode);

protected:
    int ExecuterPeriodique();
};

#endif
