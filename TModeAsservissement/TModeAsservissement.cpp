/**
 * @file TModeAsservissement.cpp
 * @brief Implémentation de TModeAsservissement
 */

#include "TModeAsservissement.h"
#include "../TConsigne/TConsigne.h"
#include "../Temps/Temps.h"

// Définition des constantes statiques de TModeAsservissement.
const char *TModeAsservissement::NOM_MODE = "asservissement";

/**
 * @brief Initialise la tâche d'asservissement avec les actionneurs donnés
 *
 */
TModeAsservissement::TModeAsservissement(
    //! Délai entre les exécution de l'asservissement
    TTemps periode,
    //! Sélection du moteur
    TSelectionMoteur &selectionMoteur,
    //! Fournaise
    TFournaise &fournaise,
    //! Ventilateur
    TVentilateur &ventilateur,
    //! Fournisseur de consigne
    TFournisseurConsigne &fournisseurConsigne,
    //! Gestionnaire d'alertes
    TAlertesSynchronisees &alertes) :
    AMode(NOM_MODE),
    ATachePeriodique(periode), FSelectionMoteur(selectionMoteur),
    FFournaise(fournaise), FVentilateur(ventilateur),
    FFournisseurConsigne(fournisseurConsigne), FAlertes(alertes), FEtat(0),
    FCommande(0.0) {
}

/**
 * @brief Retourne toujours que ce mode désire être sélectionné.
 *
 * @return Si le mode désire être sélectionné
 * @retval true Le mode désire être sélectionné
 */
bool TModeAsservissement::DesireSelection(bool) {
    return true;
}

/**
 * @brief Assigne des consignes aux actionneurs afin d'asservir la température
 * @note Implémentation de ITache::Executer
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 */
int TModeAsservissement::Executer() {
    // Exécuter la tâche.
    int codeErreur = ATachePeriodique::Executer();

    // Contrôler les moteurs.
    FSelectionMoteur.Executer();

    return codeErreur;
}

/**
 * @brief Déplace tous les moteurs à la position du moteur le moins ouvert et
 * exécute immédiatement la logique d'asservissement
 *
 */
void TModeAsservissement::EffectuerSelection() {
    FCommande = 100.0;
    // Trouver la plus petite position des moteurs.
    for (int i = 0; i < NB_MOTEURS; i++) {
        double pos = FSelectionMoteur.Moteurs[i].Position();
        if (pos < FCommande) {
            FCommande = pos;
        }
    }
    // Déplacer les moteurs à cette position.
    for (int i = 0; i < NB_MOTEURS; i++) {
        FSelectionMoteur.Moteurs[i].Commande(FCommande);
    }

    // Exécuter immédiatement l'asservissement.
    Reinitialiser();
}

/**
 * @brief Assigne des consignes aux actionneurs afin d'asservir la température
 * @note Implémentation de ATachePeriodique::ExecuterPeriodique
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 */
int TModeAsservissement::ExecuterPeriodique() {
    // Obtenir la consigne à appliquer.
    TConsigne consigne = FFournisseurConsigne.DeterminerConsigne();

    // Mettre à jour les commandes des actionneurs.
    double nouvelleCommande = FCommande;
    bool etatFournaise = TFournaise::ETEINDRE;
    bool etatVentilateur = TVentilateur::ETEINDRE;

    // Si aucun des capteurs ne fonctionne
    if (!isnormal(Temperature.Moyenne)) {
        nouvelleCommande = 0.0;
    }
    // Si la température est trop élevée
    else if (Temperature.Moyenne > consigne.TempCible + consigne.Hysteresis) {
        FEtat = 2;
        etatVentilateur = TVentilateur::ALLUMER;

        if (nouvelleCommande < 100.0)
            nouvelleCommande += INCREMENT_POSITION_ROLLUPS;
    }
    // Si la température est trop basse
    else if (Temperature.Moyenne < consigne.TempCible - consigne.Hysteresis) {
        FEtat = 1;
        if (nouvelleCommande > 0.0)
            nouvelleCommande -= INCREMENT_POSITION_ROLLUPS;

        // Allumer la fournaise seulement si les rollups sont fermés.
        if (nouvelleCommande <= 0.0)
            etatFournaise = TFournaise::ALLUMER;
    }
    // Si la température était trop basse et qu'elle est remontée au dessus de
    // la température cible ou
    // Si la température était trop élevée et qu'elle est redescendue sous la
    // température cible
    else if ((FEtat == 1 && Temperature.Moyenne > consigne.TempCible) ||
             (FEtat == 2 && Temperature.Moyenne < consigne.TempCible)) {
        FEtat = 0;
    } else {
        // Sinon, garder le même état pour la fournaise et le ventilateur.
        etatFournaise = FFournaise.Etat();
        etatVentilateur = FVentilateur.Etat();
    }

    FFournaise.Etat(etatFournaise);
    FVentilateur.Etat(etatVentilateur);

    // Si la commande des moteurs a changée
    if (nouvelleCommande != FCommande) {
        // S'assurer que la commande est entre 0 et 100%.
        // Enregistrer la commande.
        FCommande = constrain(nouvelleCommande, 0.0, 100.0);

        // Appliquer la commande à tous les moteurs.
        for (int i = 0; i < NB_MOTEURS; i++)
            FSelectionMoteur.Moteurs[i].Commande(FCommande);
    }

    return 0;
}
