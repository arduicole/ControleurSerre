/**
 * @file TModeAsservissement.h
 * @brief Déclaration de TModeAsservissement
 */

#ifndef TModeAsservissement_h
#define TModeAsservissement_h

#include "../ATachePeriodique/ATachePeriodique.h"
#include "../TActionneurBool/TActionneurBool.h"
#include "../TAlertesSynchronisees/TAlertesSynchronisees.h"
#include "../TFournisseurConsigne/TFournisseurConsigne.h"
#include "../TGestionnaireMode/TGestionnaireMode.h"
#include "../TSelectionMoteur/TSelectionMoteur.h"

//! Asservit la température dans la serre selon une consigne en
//! contrôlant les actionneurs.
//! Lorsque tous les autres modes ne sont pas enclenchés, c'est ce mode
//! automatique qui l'est. Le contrôleur va alors utiliser les consignes
//! configurées avec l'application pour asservir la température de la serre.
class TModeAsservissement : public AMode, public ATachePeriodique {
private:
    //! Sélection du moteur
    TSelectionMoteur &FSelectionMoteur;
    //! Fournaise
    TFournaise &FFournaise;
    //! Ventilateur
    TVentilateur &FVentilateur;
    //! Fournisseur de consigne
    TFournisseurConsigne &FFournisseurConsigne;
    //! Gestionnaire d'alertes
    TAlertesSynchronisees &FAlertes;

    //! État de l'asservissement.
    //! * Si la valeur est 0, la température n'a pas dépassée la zone
    //! d'hystérésis.
    //! * Si la valeur est 1, la température était ou est inférieure à
    //! l'hystérésis et n'est pas encore remontée en haut de la température
    //! cible.
    //! * Si la valeur est 2, la température était ou est supérieure à
    //! l'hystérésis et n'est pas encore redescendue sous la température cible.
    int FEtat;
    //! Commande des moteurs.
    double FCommande;

public:
    //! Nom du mode.
    static const char *NOM_MODE;

    //! Température lue des capteurs.
    TSommaireMesures Temperature;

    TModeAsservissement(TTemps periode, TSelectionMoteur &selectionMoteur,
                        TFournaise &fournaise, TVentilateur &ventilateur,
                        TFournisseurConsigne &fournisseurConsigne,
                        TAlertesSynchronisees &alertes);

    bool DesireSelection(bool premier) override;

    int Executer() override;

protected:
    void EffectuerSelection() override;
    int ExecuterPeriodique() override;
};

#endif
