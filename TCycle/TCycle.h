/**
 * @file TCycle.h
 * @brief Déclaration de TCycle
 */

#ifndef TCycle_h
#define TCycle_h

//! Contient les données sur un cycle
struct TCycle {
    //! Nombre de répétitions de la durée de chauffage et de ventilation
    unsigned int Repetitions;

    //! Durée du chauffage
    unsigned long DureeChauffage;

    //! Durée de la ventilation
    unsigned long DureeVentilation;
};

#endif
