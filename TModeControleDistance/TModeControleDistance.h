/**
 * @file TModeControleDistance.h
 * @brief Déclaration de TModeControleDistance
 */

#ifndef TModeControleDistance_h
#define TModeControleDistance_h

#include "../TActionneurBool/TActionneurBool.h"
#include "../TGestionnaireMode/TGestionnaireMode.h"
#include "../TSelectionMoteur/TSelectionMoteur.h"
#include <Arduino.h>

//! Le mode de contrôle à distance est le deuxième plus important.
//! Comme pour le mode manuel, il permet au client de contrôler manuellement
//! l'état de chaque actionneur. Cependant, au lieu d'utiliser les interrupteurs
//! du boîtier, les actionneurs sont contrôlés à partir de l'application mobile.
//! Ceci peut donc permettre au client de reprendre le contrôle du système en
//! cas de problème avec l'asservissement, même s'il n'est pas physiquement prêt
//! du contrôleur.
class TModeControleDistance : public AMode {
private:
    //! Sélection du moteur.
    TSelectionMoteur &FSelectionMoteur;
    //! Fournaise.
    TFournaise &FFournaise;
    //! Ventilateur.
    TVentilateur &FVentilateur;

    //! État de la fournaise
    bool FEtatFournaise;
    //! État du ventilateur
    bool FEtatVentilateur;
    //! Positions des moteurs
    uint8_t FPositionsMoteurs[NB_MOTEURS];

    //! Si les commandes ont été changées.
    bool FCommandeChangee;

protected:
    void EffectuerSelection() override;
    bool EffectuerDeselection() override;

public:
    //! Nom de ce mode tel que retourné par #Nom.
    static const char *NOM_MODE;

    //! Si ce mode devrait ou non être activé.
    //! true: Ce mode devrait être activé.
    //! false: Ce mode ne doit pas être activé.
    bool Activer;

    TModeControleDistance(TSelectionMoteur &selectionMoteurs,
                          TFournaise &fournaise, TVentilateur &ventilateur);

    void Restaurer();

    int Controler(bool etatFournaise, bool etatVentilateur,
                  uint8_t *positionsMoteurs);

    bool DesireSelection(bool premier) override;

    int Executer() override;
};

#endif
