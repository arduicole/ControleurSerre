/**
 * @file TModeControleDistance.cpp
 * @brief Implémentation de TModeControleDistance
 */

#include "TModeControleDistance.h"

//! Adresse du EEPROM à la quelle écrire le code permettant de savoir si la
//! mémoire est corrompue ou non.
#define ADRESSE_VERIF_MEMOIRE_ 203
//! Adresse où est écrit si le mode doit être réactivé ou pas au démarrage
#define ADRESSE_ACTIVER_MODE (ADRESSE_VERIF_MEMOIRE_ + 1)
//! Adresse où l'état de la fournaise est écrit
#define ADRESSE_ETAT_FOURNAISE (ADRESSE_ACTIVER_MODE + 1)
//! Adresse où l'état du ventilateur est écrit.
#define ADRESSE_ETAT_VENTILATEUR (ADRESSE_ETAT_FOURNAISE + 1)
//! Adresse où la liste de positions de moteurs est écrite.
#define ADRESSE_POSITIONS_MOTEURS (ADRESSE_ETAT_VENTILATEUR + 1)

//! Valeur écrite à l'adresse ADRESSE_VERIF_MEMOIRE_ pour signaler que la valeur
//! en mémoire peut être utilisée. Cette valeur est une alternance de bits `0`
//! et `1`.
#define MEMOIRE_OK_ 0b01010101
//! Valeur écrite à l'adresse ADRESSE_VERIF_MEMOIRE_ pour signaler qu'une
//! vérification est en cours. Cette valeur est MEMOIRE_OK_, mais avec tous ses
//! bits inversés.
#define MEMOIRE_MODIFICATION_EN_COURS_ (~MEMOIRE_OK_)

// Définition des constantes statiques de TModeControleDistance.
const char *TModeControleDistance::NOM_MODE = "controle-distance";

/**
 * @brief Initialise le mode de contrôle à distance avec #NOM_MODE
 * comme nom et avec les actionneurs donnés
 *
 */
TModeControleDistance::TModeControleDistance(
    //! Sélection du moteur.
    TSelectionMoteur &selectionMoteurs,
    //! Fournaise.
    TFournaise &fournaise,
    //! Ventilateur.
    TVentilateur &ventilateur) :
    AMode(NOM_MODE),
    FSelectionMoteur(selectionMoteurs), FFournaise(fournaise),
    FVentilateur(ventilateur), FCommandeChangee(false), Activer(false) {
}

/**
 * @brief Restaure l'état de l'objet en le lisant du EEPROM.
 * L'état initialisé dans le constructeur est conservé si la valeur dans le
 * EEPROM est corrompue.
 * @note Ceci permet de réactivé ce mot au démarrage s'il était activé lorsque
 * le contrôleur a été éteint
 *
 */
void TModeControleDistance::Restaurer() {
    // Si la mémoire n'est pas corrompue
    if (EEPROM.read(ADRESSE_VERIF_MEMOIRE_) == MEMOIRE_OK_) {
        Activer = EEPROM.read(ADRESSE_ACTIVER_MODE) != 0;
        if (Activer) {
            FEtatFournaise = EEPROM.read(ADRESSE_ETAT_FOURNAISE) != 0;
            FEtatVentilateur = EEPROM.read(ADRESSE_ETAT_VENTILATEUR) != 0;
            for (size_t i = 0; i < NB_MOTEURS; i++) {
                uint8_t &position = FPositionsMoteurs[i];
                position = EEPROM.read(ADRESSE_POSITIONS_MOTEURS + i);
                if (position > 100)
                    position = 100;
            }
        }
    }
}

/**
 * @brief Change les commandes des actionneurs pour celles données
 * et active le mode
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur ne s'est produite
 * @retval 1 Le paramètre positionMoteurs est nul
 * @retval 2 Une des position de moteur est invalide
 */
int TModeControleDistance::Controler(
    //! Nouvel état de la fournaise
    bool etatFournaise,
    //! Nouvel état du ventilateur
    bool etatVentilateur,
    //! Nouvelles positions des moteurs
    //! La taille de cette liste doit être #NB_MOTEURS
    uint8_t *positionsMoteurs) {
    int codeErreur = 1;

    // Valider le pointeur de positions de moteurs.
    if (positionsMoteurs != nullptr) {
        codeErreur = 0;

        for (uint8_t i = 0; i < NB_MOTEURS && codeErreur == 0; i++) {
            uint8_t position = positionsMoteurs[i];
            if (position > 100) {
                codeErreur = 2;
            }
        }

        // Enregistrer les commandes données.
        if (codeErreur == 0) {
            FEtatFournaise = etatFournaise;
            FEtatVentilateur = etatVentilateur;
            memcpy(FPositionsMoteurs, positionsMoteurs,
                   NB_MOTEURS * sizeof(uint8_t));

            Activer = true;
            FCommandeChangee = true;

            // Enregistrer les commandes et faire en sorte que ce mode soit
            // activé au prochain démarrage.
            EEPROM.write(ADRESSE_VERIF_MEMOIRE_,
                         MEMOIRE_MODIFICATION_EN_COURS_);
            EEPROM.commit();
            EEPROM.write(ADRESSE_ACTIVER_MODE, Activer);
            EEPROM.write(ADRESSE_ETAT_FOURNAISE, FEtatFournaise);
            EEPROM.write(ADRESSE_ETAT_VENTILATEUR, FEtatVentilateur);
            EEPROM.put(ADRESSE_POSITIONS_MOTEURS, FPositionsMoteurs);
            EEPROM.write(ADRESSE_VERIF_MEMOIRE_, MEMOIRE_OK_);
            EEPROM.commit();
        }
    }

    return codeErreur;
}

/**
 * @brief Active le mode si ce n'était pas déjà le cas
 * @note Ceci fait en sorte que le mode ne se désactive pas automatiquement
 * lorsqu'il est le premier mode sélectionné
 *
 */
void TModeControleDistance::EffectuerSelection() {
    Activer = true;
    FCommandeChangee = true;
}

/**
 * @brief Assigne false au membre #Activer et accepte d'être déselectionné
 *
 * @return Que ce mode accepte d'être déselectionné
 * @retval true Ce mode accepte d'être déselectionné
 */
bool TModeControleDistance::EffectuerDeselection() {
    Activer = false;

    // Faire en sorte que ce mode ne soit pas activé au prochain démarrage.
    EEPROM.write(ADRESSE_ACTIVER_MODE, Activer);
    EEPROM.commit();

    return true;
}

/**
 * @brief Retourne si le mode de contrôle à distance doit être activé
 * @note Utiliser la variable #Activer pour changer si ce mode doit être activé
 * ou non
 *
 * @return Si ce mode désire être sélectionné sélectionné
 * (la valeur de #Activer)
 */
bool TModeControleDistance::DesireSelection(bool) {
    return Activer;
}

/**
 * @brief Applique les commandes configurées avec #Controler
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur n'est survenue
 * @retval -1 Le mode de contrôle à distance doit être désactivé
 */
int TModeControleDistance::Executer() {
    // Si une commande à changer.
    if (FCommandeChangee) {
        FCommandeChangee = false;

        // Appliquer les nouvelles commandes.
        FVentilateur.Etat(FEtatVentilateur);
        FFournaise.Etat(FEtatFournaise);
        for (uint8_t i = 0; i < NB_MOTEURS; i++) {
            FSelectionMoteur.Moteurs[i].Commande(FPositionsMoteurs[i]);
        }
    }

    FSelectionMoteur.Executer();

    return Activer ? 0 : -1;
}
