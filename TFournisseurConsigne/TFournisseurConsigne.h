/**
 * @file TFournisseurConsigne.h
 * @brief Déclaration de TFournisseurConsigne et THeurePeriode
 */

#ifndef TFournisseurConsigne_H
#define TFournisseurConsigne_H

#include "../TJour/TJour.h"
#include "../TSynchroniseurTemps/TSynchroniseurTemps.h"

//! Nombre de types de jour d'asservissement.
#define NB_TYPES_JOUR 3

//! Type de jour par défaut pour l'asservissement.
#define TYPE_JOUR_DEFAUT 0
//! Température par défaut.
#define TEMP_DEFAUT 20
//! Température critique minimale assignée à
//! toutes les consignes dans le constructeur
#define TEMP_MIN_DEFAUT 16
//! Température critique maximale assignée à
//! toutes les consignes dans le constructeur
#define TEMP_MAX_DEFAUT 24
//! Hystérésis par défaut.
#define HYSTERESIS_DEFAUT 2

//! Représente l'heure du début d'une période de la journée.
struct THeurePeriode {
public:
    //! Heure de la journée entre 0 et 23 inclusivement.
    uint8_t Heure;
    //! Minute de l'heure.
    //! Entre 0 et 59 inclusivement.
    uint8_t Minute;

    THeurePeriode();
    THeurePeriode(uint8_t heure, uint8_t minute);
    THeurePeriode(uint16_t minutes);

    bool EstInvalide() const;
    uint16_t MinutesTotales() const;

    bool operator<=(const THeurePeriode &rhs) const;
};

//! Possède la liste de consigne et détermine laquelle asservir
//! en fonction de l'ensoleillement sélectionné par l'utilisateur
//! et de l'heure.
class TFournisseurConsigne {
private:
    //! Synchroniseur de temps.
    TSynchroniseurTemps &FSyncTemps;

    //! Type de jour sélectionné pour l'asservissement.
    uint8_t FTypeJour;
    //! Heures du début de chaque période de la journée.
    //! L'ordre de ces heures est le suivant: pré-jour, jour, pré-nuit, nuit.
    THeurePeriode FHeures[NB_CONSIGNES_JOUR];

public:
    //! Consignes d'asservissement pour les différents types de jours.
    TJour TypesJour[NB_TYPES_JOUR];

    TFournisseurConsigne(TSynchroniseurTemps &syncTemps);
    void Restaurer();

    const TConsigne &DeterminerConsigne();

    int ChangerConsignes(const TJour (&jours)[NB_TYPES_JOUR],
                         const THeurePeriode (&heures)[NB_CONSIGNES_JOUR],
                         TMesureCapteur hysteresis,
                         TMesureCapteur variationCritique);

    uint8_t TypeJour();
    int TypeJour(uint8_t type);

    const THeurePeriode *Heures() const;
};

#endif /* TFournisseurConsigne_H */
