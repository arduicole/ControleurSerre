/**
 * @file TFournisseurConsigne.cpp
 * @brief Implémentation de TFournisseurConsigne et THeurePeriode
 */

#include "TFournisseurConsigne.h"
#include <EEPROM.h>

//! Adresse du EEPROM à la quelle écrire le code permettant de savoir si la
//! mémoire est corrompue ou non.
#define ADRESSE_VERIF_MEMOIRE 0
//! Adresse où est écrit le type de jour sélectionné.
#define ADRESSE_TYPE_JOUR (ADRESSE_VERIF_MEMOIRE + 1)
//! Adresse où la liste de consignes pour les différents
//! types de jour est écrite.
#define ADRESSE_JOURS (ADRESSE_TYPE_JOUR + 1)
//! Adresse où la liste de consignes pour les différents
//! types de jour est écrite.
#define ADRESSE_HEURES (ADRESSE_JOURS + sizeof(TFournisseurConsigne::TypesJour))

//! Valeur écrite à l'adresse ADRESSE_VERIF_MEMOIRE pour signaler que la valeur
//! en mémoire peut être utilisée. Cette valeur est une alternance de bits `0`
//! et `1`.
#define MEMOIRE_OK 0b01010101
//! Valeur écrite à l'adresse ADRESSE_VERIF_MEMOIRE pour signaler qu'une
//! vérification est en cours. Cette valeur est MEMOIRE_OK, mais avec tous ses
//! bits inversés.
#define MEMOIRE_MODIFICATION_EN_COURS (~MEMOIRE_OK)

//==============================================================================
// Implémentation de THeurePeriode
//==============================================================================

/**
 * @brief Initialise une heure de début d'une période avec l'heure donnée
 * @note Si les valeurs données sont invalides, #EstInvalide retourne true
 *
 */
THeurePeriode::THeurePeriode(
    //! Heure de la journée entre 0 et 23 inclusivement.
    //! L'opérateur modulo est utilisé pour restreindre la valeur donnée pour
    //! qu'elle soit valide.
    uint8_t heure,
    //! Minute de l'heure entre 0 et 59 inclusivement.
    //! L'opérateur modulo est utilisé pour restreindre la valeur donnée pour
    //! qu'elle soit valide.
    uint8_t minute) :
    Heure(heure),
    Minute(minute) {
}

/**
 * @brief Initialise une heure de début d'une période à minuit (00:00)
 *
 */
THeurePeriode::THeurePeriode() : THeurePeriode(0, 0) {
}

/**
 * @brief Converti l'heure en nombre de minutes depuis le début de la journée en
 * heures et en minutes pour les placer dans la structure.
 * @note Si la valeur donnée est invalide, #EstInvalide retourne true
 *
 */
THeurePeriode::THeurePeriode(
    //! Nombre total de minutes depuis le début de la journée.
    uint16_t minutes) :
    THeurePeriode(minutes / 60, minutes % 60) {
}

/**
 * @brief Retourne si l'heure de cette structure est invalide
 *
 * @return Si l'heure de cette structure est invalide
 */
bool THeurePeriode::EstInvalide() const {
    return Minute >= 60 || Heure >= 24;
}

/**
 * @brief Retourne le nombre total de minutes de cette heure
 *
 * @return Le nombre total de minutes de cette heure
 */
uint16_t THeurePeriode::MinutesTotales() const {
    return ((uint16_t)Heure) * 60 + Minute;
}

bool THeurePeriode::operator<=(const THeurePeriode &rhs) const {
    return (Heure < rhs.Heure) || (Heure == rhs.Heure && Minute <= rhs.Minute);
}

//==============================================================================
// Implémentation de TFournisseurConsigne
//==============================================================================

/**
 * @brief Initialise un fournisseur de consigne avec des consignes par défaut
 *
 */
TFournisseurConsigne::TFournisseurConsigne(TSynchroniseurTemps &syncTemps) :
    FSyncTemps(syncTemps),
    FTypeJour(TYPE_JOUR_DEFAUT), FHeures{THeurePeriode(4, 0),
                                         THeurePeriode(6, 0),
                                         THeurePeriode(18, 0),
                                         THeurePeriode(20, 0)} {

    // Utiliser des valeurs par défaut.
    for (TJour &jour : TypesJour) {
        for (TConsigne &consigne : jour.Consignes) {
            consigne.TempCible = TEMP_DEFAUT;
            consigne.TempMaxCritique = TEMP_MAX_DEFAUT;
            consigne.TempMinCritique = TEMP_MIN_DEFAUT;
            consigne.Hysteresis = HYSTERESIS_DEFAUT;
        }
    }
}

/**
 * @brief Restaure l'état de l'objet en le lisant du EEPROM.
 * L'état initialisé dans le constructeur est conservé si la valeur dans le
 * EEPROM est corrompue.
 *
 */
void TFournisseurConsigne::Restaurer() {
    // Si la mémoire n'est pas corrompue
    if (EEPROM.read(ADRESSE_VERIF_MEMOIRE) == MEMOIRE_OK) {
        // Restaurer l'état de la mémoire.
        TJour typesJour[NB_TYPES_JOUR];
        THeurePeriode heures[NB_CONSIGNES_JOUR];

        TypeJour(EEPROM.read(ADRESSE_TYPE_JOUR));
        EEPROM.get(ADRESSE_JOURS, typesJour);
        EEPROM.get(ADRESSE_HEURES, heures);

        TJour &jour0 = typesJour[0];
        TMesureCapteur hysteresisIni = jour0.Nuit.Hysteresis;
        TMesureCapteur variationIni =
            jour0.Nuit.TempMaxCritique - jour0.Nuit.TempCible;
        ChangerConsignes(typesJour, heures, hysteresisIni, variationIni);
    }
}

/**
 * @brief Détermine la consigne à appliquer en fonction de l'ensoleillement
 * sélectionné par l'utilisateur et de l'heure
 * @note Si l'heure n'est pas synchronisée, la consigne du préjour sera utilisée
 *
 * @return La consigne à asservir
 */
const TConsigne &TFournisseurConsigne::DeterminerConsigne() {
    // Déterminer le jour en fonction de l'ensoleillement
    // sélectionné par l'utilisateur.
    TJour &jour = TypesJour[FTypeJour];

    // Déterminer la consigne en fonction de l'heure
    // et de si elle est synchronisée.
    const TConsigne *consigne = &jour.Prejour;
    if (FSyncTemps.EstSynchronise()) {
        time_t temps = temps();
        tm *local = localtime(&temps);
        THeurePeriode heureActuelle(local->tm_hour, local->tm_min);

        consigne = &jour.Nuit;
        for (int i = NB_CONSIGNES_JOUR - 1; i >= 0; i--) {
            THeurePeriode &heure = FHeures[i];
            // Si l'heure est dans la période de la journée.
            if (heure <= heureActuelle) {
                // Utiliser la consigne de cette période.
                consigne = &jour.Consignes[i];
                // Sortir de la boucle.
                i = -1;
            }
        }
    }

    return *consigne;
}

/**
 * @brief Change le type de jour de l'asservissement
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 L'hystérésis n'est pas inférieur à la variation critique
 * @retval 2 Une des heures donnée est invalide
 * @retval 3 Les heures données ne sont pas strictement croissantes
 */
int TFournisseurConsigne::ChangerConsignes(
    //! Les consignes pour les différents types de jour
    const TJour (&typesJour)[NB_TYPES_JOUR],
    //! Heures des périodes de la journées telles que retournées par #Heure
    const THeurePeriode (&heures)[NB_CONSIGNES_JOUR],
    //! L'écart de la température cible qui est acceptable
    TMesureCapteur hysteresis,
    //! Variation critique des consignes
    TMesureCapteur variationCritique) {
    hysteresis = abs(hysteresis);
    variationCritique = abs(variationCritique);

    // Valider l'hystérésis et la variation critique.
    if (hysteresis >= variationCritique) {
        return 1;
    }

    // Valider les heures des périodes de la journée.
    // Les heures doivent être inféireures au nombre de minutes dans une journée
    // et elles doivent être strictement croissantes.
    THeurePeriode heurePrecedente = heures[0];
    if (heurePrecedente.EstInvalide()) {
        return 2;
    }
    for (int i = 1; i < NB_CONSIGNES_JOUR; i++) {
        const THeurePeriode heure = heures[i];
        if (heure.EstInvalide())
            return 2;
        if (heure <= heurePrecedente)
            return 3;

        heurePrecedente = heure;
    }

    // Copier la nouvelle consigne.
    for (int i = 0; i < NB_TYPES_JOUR; i++) {
        TJour &jour = TypesJour[i];
        const TJour &nouveauJour = typesJour[i];

        for (int j = 0; j < NB_CONSIGNES_JOUR; j++) {
            TConsigne &consigne = jour.Consignes[j];

            consigne.TempCible = nouveauJour.Consignes[j].TempCible;
            consigne.Hysteresis = hysteresis;
            consigne.TempMaxCritique = consigne.TempCible + variationCritique;
            consigne.TempMinCritique = consigne.TempCible - variationCritique;
        }
    }

    // Copier les heures des périodes de la journée.
    memcpy(FHeures, heures, sizeof(FHeures));

    // Écrire la séquence de modification de la mémoire en cours.
    EEPROM.write(ADRESSE_VERIF_MEMOIRE, MEMOIRE_MODIFICATION_EN_COURS);
    EEPROM.commit();

    // Écrire les types de jour en mémoire.
    EEPROM.put(ADRESSE_JOURS, TypesJour);
    // Écrire les heures en mémoire.
    EEPROM.put(ADRESSE_HEURES, FHeures);
    // Écrire le code de mémoire valide.
    EEPROM.write(ADRESSE_VERIF_MEMOIRE, MEMOIRE_OK);
    EEPROM.commit();

    return 0;
}

/**
 * @brief Donne le type de jour en exécution
 *
 * @return Type de jour
 * @retval 0 ensoleille
 * @retval 1 mi-naugeux
 * @retval 2 nuageux
 */
uint8_t TFournisseurConsigne::TypeJour() {
    return FTypeJour;
}

/**
 * @brief Change le type de jour de l'asservissement
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 Le type de jour n'existe pas
 */
int TFournisseurConsigne::TypeJour(
    //! Type de jour compris dans [0, 2]
    uint8_t type) {
    int codeErreur = 1;
    if (type < NB_TYPES_JOUR) {
        FTypeJour = type;

        // Enregistrer la nouvelle valeur.
        EEPROM.write(ADRESSE_TYPE_JOUR, type);
        EEPROM.commit();

        codeErreur = 0;
    }
    return codeErreur;
}

/**
 * @brief Retourne la liste des heures des périodes de la journée
 * @note L'ordre de ces heures est le suivant: pré-jour, jour, pré-nuit, nuit
 *
 * @return La liste des heures des périodes de la journée
 */
const THeurePeriode *TFournisseurConsigne::Heures() const {
    return FHeures;
}
