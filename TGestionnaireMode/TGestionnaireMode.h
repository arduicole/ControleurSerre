/**
 * @file TGestionnaireMode.h
 * @brief Déclaration de TGestionnaireMode et de AMode
 */

#ifndef TGestionnaireMode_h
#define TGestionnaireMode_h

#include "../ITache/ITache.h"
#include <list>

class TGestionnaireMode;

//! Mode de contrôle des actionneur.
class AMode : public ITache {
private:
    //! Nom de ce mode.
    const char *FNom;
    //! Si le mode est sélectionné par le TGestionnaireMode.
    bool FEstSelectionne;

    // Méthodes destinées au TGestionnaireMode.
    friend TGestionnaireMode;
    void Selectionner();
    bool Deselectionner();

protected:
    virtual void EffectuerSelection();
    virtual bool EffectuerDeselection();

    /**
     * @brief Méthode appelée par le #TGestionnaireMode pour savoir si ce mode
     * désire ou non être sélectionné.
     * @note Même si cette méthode est appelée, cela ne signifie pas que le mode
     * sera tout de suite sélectionné. Le code dans #EffectuerSelection est
     * celui qui sera exécuté lorsque le mode se fait sélectionner.
     *
     * @return Si le mode désire être sélectionné
     * @retval true Le mode désire être sélectionné
     * @retval false Le mode ne veut pas être déselectionné
     */
    virtual bool DesireSelection(
        //! Si c'est le premier mode sélectionné
        bool premier) = 0;

public:
    AMode(const char *nom);

    const char *Nom() const;

    bool EstSelectionne() const;

    /**
     * @brief Exécute la logique de contrôle des actionneurs
     *
     * @return Un code d'erreur
     * @retval 0 Aucune erreur
     * @retval -1 Le mode veut être déselectionné
     * @retval * Autre code d'erreur ignoré par TGestionnaireMode
     */
    virtual int Executer() override = 0;
};

//! Gère la sélection du AMode à exécuter et son exécution.
class TGestionnaireMode : public ITache {
private:
    //! Liste de tous les modes.
    std::list<AMode *> FListeModes;

    //! Mode en cours d'exécution ou nullptr si aucun mode n'est en cours
    //! d'exécution.
    AMode *FModeSelectionne;

    // Si c'est le premier mode qui a été sélectionné.
    bool FPremierMode;

public:
    TGestionnaireMode();

    void AjouterMode(AMode &mode);
    const AMode *ModeSelectionne() const;

    // Change le mode en cours si nécessaire et l'exécute.
    int Executer() override;
};

#endif
