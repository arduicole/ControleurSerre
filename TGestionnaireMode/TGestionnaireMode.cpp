/**
 * @file TGestionnaireMode.cpp
 * @brief Implémentation de TGestionnaireMode et de AMode
 */

#include "TGestionnaireMode.h"

//=============================================================================
// Implémentation de AMode
//=============================================================================

/**
 * @brief Initialise un mode déselectionné avec le nom donné.
 *
 */
AMode::AMode(
    //! Nom du mode qui sera retourné par #Nom
    const char *nom) :
    FNom(nom) {
}

/**
 * @brief Retourne le nom du mode tel que configuré dans le constructeur.
 *
 * @return Le nom du mode.
 */
const char *AMode::Nom() const {
    return FNom;
}

/**
 * @brief Retourne si le mode est actuellement sélectionné ou non par le
 * TGestionnaireMode
 *
 * @return Si le mode est actuellement sélectionné ou non par le
 * TGestionnaireMode
 */
bool AMode::EstSelectionne() const {
    return FEstSelectionne;
}

/**
 * @brief Méthode appelée par #TGestionnaireMode pour effectuer la sélection du
 * mode. Si le mode ne désire pas être sélectionné, cette méthode doit
 * retourné false.
 * @note #DesireSelection sera appelée avant cette méthode pour savoir si ce
 * mode désire être sélectionné.
 *
 * @return Si le mode accepte ou refuse d'être sélectionné
 * @retval true Le mode accepte d'être sélectionné
 * @retval false Le mode refuse d'être sélectionné
 */
void AMode::Selectionner() {
    EffectuerSelection();
    FEstSelectionne = true;
}

/**
 * @brief Méthode appelée par #TGestionnaireMode pour effectuer la désélection
 * du mode. Si le mode désire ne pas être déselectionné, cette méthode doit
 * retourné false.
 *
 * @return Si le mode accepte ou refuse d'être sélectionné
 * @retval true Le mode accepte d'être déselectionné
 * @retval false Le mode refuse d'être sélectionné
 */
bool AMode::Deselectionner() {
    bool resultat = EffectuerDeselection();

    if (resultat) {
        FEstSelectionne = false;
    }

    return resultat;
}

/**
 * @brief Méthode appelée par #Selectionner pour effectuer la sélection du
 * mode.
 * @note #DesireSelection sera appelé avant cette méthode pour savoir si ce
 * mode désire être sélectionné.
 * @note L'implémentation par défaut ne fait rien
 */
void AMode::EffectuerSelection() {
}

/**
 * @brief Méthode appelée par #Deselectionner pour effectuer la désélection
 * du mode. Si le mode désire ne pas être déselectionné, cette méthode doit
 * retourné false.
 * @note L'implémentation par défaut ne fait que retourné que le mode est prêt à
 * être déselectionné
 *
 * @return Si le mode accepte ou refuse d'être sélectionné
 * @retval true Le mode accepte d'être déselectionné
 * @retval false Le mode refuse d'être sélectionné
 */
bool AMode::EffectuerDeselection() {
    return true;
}

//=============================================================================
// Implémentation de TGestionnaireMode
//=============================================================================

/**
 * @brief Initialise un nouveau gestionnaire de mode sans aucun mode.
 *
 */
TGestionnaireMode::TGestionnaireMode() :
    FListeModes(), FModeSelectionne(nullptr), FPremierMode(true) {
}

/**
 * @brief Ajoute un mode au gestionnaire de modes. Ce mode aura une priorité
 * inférieure à tous ceux précédemment ajoutés.
 *
 */
void TGestionnaireMode::AjouterMode(
    //! Nouveau mode à ajouter.
    AMode &mode) {
    FListeModes.push_back(&mode);
}

/**
 * @brief Retourne un pointeur vers le mode sélectionné
 * ou nullptr si aucun mode n'est sélectionné
 *
 * @return Le mode sélectionné
 * @retval nullptr Aucun mode n'est sélectionné
 * @retval * Un pointeur vers le mode sélectionné
 */
const AMode *TGestionnaireMode::ModeSelectionne() const {
    return FModeSelectionne;
}

/**
 * @brief Change le mode en cours si nécessaire et l'exécute.
 *
 * @return Le code d'erreur du mode exécuté
 * @retval -1 Aucun mode n'est en cours d'exécution.
 * @retval * Code d'erreur du mode en cours d'exécution.
 */
int TGestionnaireMode::Executer() {
    int codeErreur = -1;

    for (auto it = FListeModes.begin();
         it != FListeModes.end() && codeErreur == -1; it++) {
        AMode &mode = **it;

        // Si le mode est le mode sélectionné.
        if (&mode == FModeSelectionne) {
            // Exécuter le mode.
            codeErreur = mode.Executer();

            // Déselectionner le mode s'il le demande.
            if (codeErreur == -1) {
                // Déselectionner le mode.
                if (mode.Deselectionner()) {
                    FModeSelectionne = nullptr;
                } else {
                    // Empêcher la sélection d'un autre mode si le
                    // mode n'a pas pu être déselectionné.
                    codeErreur = 0;
                }
            }
        }
        // Sinon si le mode désire être sélectionné.
        else if (mode.DesireSelection(FPremierMode)) {
            FPremierMode = false;

            // Déselectionner le mode actuellement sélectionné.
            bool peutSelectionner = true;
            if (FModeSelectionne != nullptr) {
                peutSelectionner = FModeSelectionne->Deselectionner();
            }

            if (peutSelectionner) {
                // Changer le mode sélectionné et l'exécuter.
                FModeSelectionne = &mode;
                mode.Selectionner();
                codeErreur = mode.Executer();
            }
        }
    }

    return codeErreur;
}
