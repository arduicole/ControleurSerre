/**
 * @file TMoteur.cpp
 * @brief Implémentation de TMoteur
 */

#include "TMoteur.h"
#include <Arduino.h>

//! États des broches en fonction de la direction
static const int ETATS_BROCHES[][2] = {
    {0, 0}, // dmNeutre
    {0, 1}, // dmOuvrir
    {1, 0}, // dmFermer
};

/**
 * @brief Initialise un moteur avec les broches de direction et les paramètres
 * donnés pour la configuration de son déplacement et du calcul de sa position
 * donné. Le moteur est également déplacé à la position 0.
 *
 */
TMoteur::TMoteur(
    //! Broche de contrôle de la direction du moteur
    const int (&brochesDirection)[2],
    //! Temps en ms que prend le rollup à passé de 0% à 100%
    uint32_t dureeOuverture,
    //! Temps en ms que prend le rollup à passé de 100% à 0%
    uint32_t dureeFermeture,
    //! Délai en millisecondes entre les changements de direction
    //! du moteur.
    uint32_t delaiChangementDirection,
    //! Plage de position dans laquelle doit se trouver le moteur pour s'arrêter
    double hysteresis) {
    DureeOuverture = dureeOuverture;
    DureeFermeture = dureeFermeture;
    DelaiChangementDirection = delaiChangementDirection;
    Hysteresis = hysteresis;

    // Initialiser la commande à 0.
    FCommande = 0.0;
    // Initialiser la position à 0.
    FPositionDebut = 0.0;
    // Pour les 2 broches
    for (int i = 0; i < 2; i++) {
        int broche = brochesDirection[i];

        // Assigner la broche à celle de l'objet.
        Broches[i] = broche;

        // Configurer la broche en sortie.
        pinMode(broche, OUTPUT);
    }

    // Arrêter le déplacement des moteurs.
    FEtat = dmFermer; // Cette ligne est nécessaire, puisque si FEtat est déjà à
                      // dmNeutre, il ne changera pas son état et les niveaux
                      // logiques des broches puisqu'il croira qu'il n'y a rien
                      // à faire.
    Etat(dmNeutre);

    // Réinitialiser la position de début à 0.
    FPositionDebut = 0.0; // Sans cela, la valeur de la position du début sera
                          // une valeur bizarre calculée par
                          // Etat(TDirectionMoteur) en utilisant des valeurs
                          // aléatoires.
}

/**
 * @brief Arrête le déplacement du moteur et change la position du moteur pour
 * celle donnée
 * @note Ceci ne change pas la position réelle du moteur, cela change uniquement
 * sa position dans le logiciel
 *
 */
void TMoteur::Reinitialiser(
    //! Position où déplacer le moteur
    double position) {
    // Arrêter le moteur.
    Etat(dmNeutre);
    // Changer la position du moteur pour celle donnée.
    FPositionDebut = position;
}

/**
 * @brief Calcul et retourne la position du moteur en pourcentage
 *
 * @return La position du moteur en pourcentage. Si la valeur de restreindre est
 * true, la valeur retournée sera restreinte entre 0.0% et 100.0%, sinon elle
 * pourra avoir n'importe quelle valeur.
 */
double TMoteur::Position(
    //! S'il faut restreindre la position entre 0 et 100%
    bool restreindre) {
    // Initialiser la position au début du déplacement dans la direction.
    double pos = FPositionDebut;

    // Si le moteur est en mouvement
    if (FEtat != dmNeutre) {
        // Déterminer la durée totale d'un déplacement d'un extrème
        // à l'autre en fonction de la direction.
        // NOTE: Le deuxième nombre est négatif afin qu'une soustraction soit
        // faite à la position lorsque le moteur est en direction de fermeture.
        double dureeDeplacement = FEtat == dmOuvrir ? (double)DureeOuverture
                                                    : -(double)DureeFermeture;

        // Calculer le temps écoulé depuis le début du déplacement dans la
        // direction.
        TTemps tempsEcoule = millis() - FDebutDirection;

        // Calculer et ajouter le pourcentage duquel le moteur s'est déplacé à
        // la position.
        pos += (tempsEcoule * 100) / dureeDeplacement;

        if (restreindre) {
            // Restreindre la position entre 0 et 100%.
            if (pos < 0.0)
                pos = 0.0;
            else if (pos > 100.0)
                pos = 100.0;
        }
    }

    return pos;
}

/**
 * @brief Obtenir la position où le moteur se dirige
 *
 * @return La position où le moteur se dirige
 */
double TMoteur::Commande() {
    return FCommande;
}

/**
 * @brief Changer la commande du moteur
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 La commande n'est pas entre 0.0 et 100.0
 */
int TMoteur::Commande(
    //! Position où diriger le moteur
    double commande) {
    int erreur = 1;
    if ((commande >= 0.0) && (commande <= 100.0)) {
        FCommande = commande;
        erreur = 0;
    }
    return erreur;
}

/**
 * @brief Initialise un contrôleur de direction avec les broches données et
 * l'état à neutre
 * @note Implémentation de AActionneur::Etat(TEtat)
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 Direction donnée invalide
 */
int TMoteur::Etat(
    //! Direction du moteur
    TDirectionMoteur direction) {
    // Initialiser le code d'erreur au code de direction invalide.
    int erreur = 1;

    // Si la direction est valide.
    if (direction >= dmPremier && direction <= dmDernier) {
        // Si on veut passer de ouvrir à fermer ou de fermer à ouvrir sans
        // passer par neutre
        if (direction != FEtat && direction != dmNeutre && FEtat != dmNeutre)
            // Obliger le passage par l'état neutre.
            direction = dmNeutre;

        // Si la direction est différente
        if (direction != FEtat) {
            // Si le délai est passé ou que la direction est neutre ou que la
            // direction est la même que la précédente
            if (millis() - FDebutDirection >= DelaiChangementDirection ||
                direction == dmNeutre || direction == FDirectionPrecedente) {
                // Obtenir l'état des broches en fonction de la direction.
                const int *etats = ETATS_BROCHES[direction];

                // Assigner l'état des broches aux broches.
                digitalWrite(Broches[0], etats[0]);
                digitalWrite(Broches[1], etats[1]);

                // Calculer la position actuelle et l'assigner à la position du
                // début du mouvement.
                FPositionDebut = Position();

                // Assigner l'heure actuelle au début de la direction.
                FDebutDirection = millis();

                // Assigner l'état à la direction précédente.
                FDirectionPrecedente = FEtat;

                // Assigner la direciton à l'état.
                FEtat = direction;
            }
        }

        // Assigner le code de réussite au code d'erreur.
        erreur = 0;
    }

    return erreur;
}

/**
 * @brief Détermine si le moteur a besoin ou non de se déplacer.
 * Un moteur n'a pas besoin de se déplacer si sa commande n'est
 * pas 0% ou 100% et que sa position est dans la zone d'hystérésis.
 * Il n'a pas non plus besoin de se déplacer si sa position est égale à sa
 * commande.
 * Un moteur n'a pas non plus encore besoin de se déplacer s'il doit changé de
 * direction et que son délai de changement de direction n'est pas écoulé.
 *
 * @return Si le moteur a besoin de se déplacer
 * @retval true Le moteur a besoin de se déplacer
 * @retval false Le moteur n'a pas besoin de se déplacer
 */
bool TMoteur::BesoinDeplacer() {
    double pos = Position();
    bool resultat = false;

    // Si le moteur n'a pas atteint la commande.
    if ((abs(pos - FCommande) > Hysteresis) ||
        (pos != FCommande && FCommande == 0.0 && FCommande == 100.0)) {
        // Si le moteur n'a plus besoin d'attendre la fin du délai de
        // changement de direction.
        TDirectionMoteur direction = pos > FCommande ? dmFermer : dmOuvrir;
        resultat = millis() - FDebutDirection >= DelaiChangementDirection ||
                   direction == FDirectionPrecedente;
    }
    return resultat;
}

/**
 * @brief Vérifie si le moteur a atteint sa destination et si oui, arrête le
 * moteur, sinon dirige le moteur dans la bonne direction pour s'y rendre
 * @note Implémentation de ITache::Executer()
 *
 * @return La direction du moteur et s'il a besoin de se déplacer
 * Le moteur a besoin de se déplacer s'il retourne une valeur supérieure à 0.
 * S'il retourne 0, il est déjà arrêté.
 * @retval -1 Ouvrir (-dmOuvrir) et prêt à s'arrêter
 * @retval -2 Fermer (-dmFermer) et prêt à s'arrêter
 * @retval 0 Neutre (dmNeutre)
 * @retval 1 Ouvrir (dmOuvrir), mais pas prêt à s'arrêter
 * @retval 2 Fermer (dmFermer), mais pas prêt à s'arrêter
 */
int TMoteur::Executer() {
    // Calculer la position du moteur.
    double pos = Position();

    bool besoinDeplacer = true;
    TDirectionMoteur direction = dmNeutre;

    if (FCommande == 0.0) {
        direction = dmFermer;
        // Si le moteur a atteint la commande.
        if (pos == 0.0) {
            besoinDeplacer = false;
        }
    } else if (FCommande == 100.0) {
        direction = dmOuvrir;
        // Si le moteur a atteint la commande.
        if (pos == 100.0) {
            besoinDeplacer = false;
        }
    }
    // Sinon si la différence entre le moteur et sa commande
    // est supérieure à l'hystérésis.
    else if (abs(pos - FCommande) > Hysteresis) {
        // Diriger le moteur dans la bonne direction.
        direction = pos > FCommande ? dmFermer : dmOuvrir;
    }
    // Sinon, arrêter le moteur.

    // Changer la direction du moteur.
    Etat(direction);

    return besoinDeplacer ? (int)direction : -(int)direction;
}
