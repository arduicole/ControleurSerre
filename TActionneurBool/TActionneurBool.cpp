/**
 * @file TActionneurBool.cpp
 * @brief Implémentation de TActionneurBool
 */

#include "TActionneurBool.h"
// digitalWrite
#include <Arduino.h>

/**
 * @brief Initialise un actionneur booléen avec la broche donné et l'éteint
 *
 */
TActionneurBool::TActionneurBool(
    //! Broche de contrôle de l'actionneur
    int broche,
    //! Si la logique de contrôle de l'actionneur doit être inversée.
    //!
    //! Si sa valeur est false, un niveau logique 1 sera envoyé pour allumer
    //! l'actionneur et un niveau logique 0 sera envoyé pour l'éteindre.
    //!
    //! Sinon si sa valeur est true, un niveau logique 0 sera envoyé pour
    //! allumer l'actionneur et un niveau logique 1 sera envoyé pour l'éteindre.
    bool inverser) :
    Broche(broche),
    Inverser(inverser) {
    // Configurer la broche en sortie.
    pinMode(Broche, OUTPUT);

    // Éteindre l'actionneur.
    Etat(ETEINDRE);
}

/**
 * @brief Modifie l'état de l'actionneur
 * @note Implémentation de AActionneur::Etat(bool)
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 */
int TActionneurBool::Etat(
    //! Nouvel état de l'actionneur
    bool etat) {
    // Changer l'état de l'actionneur à celui donné tout en prenant
    // ne compte l'inversement de la logique.
    digitalWrite(Broche, Inverser != etat);

    // Enregister l'état de l'actionneur.
    FEtat = etat;

    // Retourner le code de réussite à la routine appelante.
    return 0;
}
