/**
 * @file TActionneurBool.h
 * @brief Déclaration de TActionneurBool, TVentilateur et TFournaise
 */

#ifndef TActionneurBool_h
#define TActionneurBool_h

#include "../AActionneur/AActionneur.h"

//! Contrôle un actionneur booléen, c'est-à-dire tout type d'actionneur qui n'a
//! que deux états et qui peut être contrôlé à l'aide d'une seule broche en la
//! mettant à un niveau logique 0 ou 1.
class TActionneurBool : public AActionneur<bool> {
public:
    //! Broche de contrôle de l'actionneur
    const int Broche;
    //! Si la logique de contrôle de l'actionneur doit être inversée.
    //!
    //! Si sa valeur est false, un niveau logique 1 sera envoyé pour allumer
    //! l'actionneur et un niveau logique 0 sera envoyé pour l'éteindre.
    //!
    //! Sinon si sa valeur est true, un niveau logique 0 sera envoyé pour
    //! allumer l'actionneur et un niveau logique 1 sera envoyé pour l'éteindre.
    const bool Inverser;

    //! Valeur à passer à Etat(bool) pour allumer l'actionneur.
    static const bool ALLUMER = true;
    //! Valeur à passer à Etat(bool) pour éteindre l'actionneur.
    static const bool ETEINDRE = false;

    TActionneurBool(int broche, bool inverser = false);

    using AActionneur<bool>::Etat;
    int Etat(bool etat) override;
};

//! Contrôle un ventilateur pour l'aération de la serre
typedef TActionneurBool TVentilateur;

//! Contrôle une fournaise pour le chauffage de la serre
typedef TActionneurBool TFournaise;

#endif
