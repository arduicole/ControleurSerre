/**
 * @file TModeCycle.cpp
 * @brief Implémentation de TModeCycle
 */

#include "TModeCycle.h"
#include <Arduino.h>

//! Niveau logique pour allumer le ventilateur ou la fournaise
#define ALLUME 1
//! Niveau logique pour éteindre le ventilateur ou la fournaise
#define ETEINT 0

static const char *NOM_ETAPE[2] = {"chauffage", "ventilation"};

// Définition des constantes statiques de TModeCycle.
const char *TModeCycle::NOM_MODE = "cycle";

/**
 * @brief Initialise une tâche avec la période donnée et un cycle avec 0
 * répétitions
 *
 */
TModeCycle::TModeCycle(TSelectionMoteur &selectionMoteur, TFournaise &fournaise,
                       TVentilateur &ventilateur) :
    AMode(NOM_MODE),
    FSelectionMoteur(selectionMoteur), FFournaise(fournaise),
    FVentilateur(ventilateur) {
    FRepetition = 0;
    DerniereRepetition = 0;
    DebutEtape = 0;
    FTempsEcoule = 0;
}

/**
 * @brief Contrôle des actionneurs pour suivre le cycle configuré
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval -1 Le cycle a terminé d'être exécuté
 * @retval 1 En fonction
 * @retval 2 Chauffage
 * @retval 3 Ventilation
 */
int TModeCycle::Executer() {
    int codeErreur = 0;

    if (Cycle.Repetitions > 0 && FRepetition < Cycle.Repetitions) {
        codeErreur = 1;
        if (FRepetition == DerniereRepetition + 1 ||
            PremiereRepetition == true) {
            DebutEtape = millis();
            DerniereRepetition = FRepetition;
            PremiereRepetition = false;
        }
        FTempsEcoule = (millis() - DebutEtape) / MINUTE_MS;

        if (EtapeDepart == ecChauffage) {
            if (FTempsEcoule < Cycle.DureeChauffage) {
                Etape = ecChauffage;
                codeErreur = 2;
            } else if (FTempsEcoule <
                       (Cycle.DureeVentilation + Cycle.DureeChauffage)) {
                Etape = ecVentilation;
                codeErreur = 3;
            } else if (FTempsEcoule >=
                       (Cycle.DureeVentilation + Cycle.DureeChauffage)) {
                Etape = ecChauffage;
                FRepetition++;
                codeErreur = 1;
            }
        } else if (EtapeDepart == ecVentilation) {
            if (FTempsEcoule < Cycle.DureeVentilation) {
                Etape = ecVentilation;
                codeErreur = 3;
            } else if (FTempsEcoule <
                       (Cycle.DureeVentilation + Cycle.DureeChauffage)) {
                Etape = ecChauffage;
                codeErreur = 2;
            } else if (FTempsEcoule >=
                       (Cycle.DureeVentilation + Cycle.DureeChauffage)) {
                Etape = ecVentilation;
                FRepetition++;
                codeErreur = 1;
            }
        }

        FFournaise.Etat(Etape == ecChauffage ? TFournaise::ALLUMER
                                             : TFournaise::ETEINDRE);
        FVentilateur.Etat(Etape == ecVentilation ? TVentilateur::ALLUMER
                                                 : TVentilateur::ETEINDRE);

        double commandeRollups = Etape == ecVentilation ? 100.0 : 0.0;
        for (TMoteur &moteur : FSelectionMoteur.Moteurs) {
            moteur.Commande(commandeRollups);
        }
        FSelectionMoteur.Executer();
    } else {
        // Arrêter le cycle.
        NouveauCycle(0);
        codeErreur = -1;
    }

    return codeErreur;
}

/**
 * @brief Donne le nombre de repetitions
 *
 * @return Nombre de repetitions
 */
unsigned int TModeCycle::Repetition() {
    return FRepetition;
}

/**
 * @brief Donne l'étape en execution
 *
 * @return l'étape en execution
 */
const char *TModeCycle::EtapeActuelle() {
    return NOM_ETAPE[Etape];
}

/**
 * @brief Donne le temps ecoule depuis le début d'une étape
 *
 * @return Temps écoulé depuis le début d'une étape
 */
TTemps TModeCycle::TempsEcoule(
    //! L'étape dont on veut le temps écoulé
    TEtapeCycle etape) {
    TTemps resultat = 0;
    if (Cycle.Repetitions > 0) {
        if (etape == ecChauffage) {
            if (EtapeDepart == ecChauffage) {
                if (Etape == ecChauffage) {
                    resultat = FTempsEcoule;
                } else if (Etape == ecVentilation) {
                    resultat = Cycle.DureeChauffage;
                }
            } else if (EtapeDepart == ecVentilation) {
                if (Etape == ecChauffage) {
                    resultat = FTempsEcoule - Cycle.DureeVentilation;
                } else if (Etape == ecVentilation) {
                    resultat = 0;
                }
            }
        } else if (etape == ecVentilation) {
            if (EtapeDepart == ecChauffage) {
                if (Etape == ecChauffage) {
                    resultat = 0;
                } else if (Etape == ecVentilation) {
                    resultat = FTempsEcoule - Cycle.DureeChauffage;
                }
            } else if (EtapeDepart == ecVentilation) {
                if (Etape == ecChauffage) {
                    resultat = Cycle.DureeVentilation;
                } else if (Etape == ecVentilation) {
                    resultat = FTempsEcoule;
                }
            }
        }
    }
    return resultat;
}

/**
 * @brief Supprime le cycle en cours et accepte la déselection.
 *
 * @return Si le mode accepte ou refuse d'être sélectionné
 * @retval true Le mode accepte d'être déselectionné
 */
bool TModeCycle::EffectuerDeselection() {
    Cycle.Repetitions = 0;
    return true;
}

/**
 * @brief Méthode appelée par le #TGestionnaireMode pour savoir si ce mode
 * désire ou non être sélectionné.
 * @note Même si cette méthode est appelée, cela ne signifie pas que le mode
 * sera tout de suite sélectionné. Le code dans #EffectuerSelection est
 * celui qui sera exécuté lorsque le mode se fait sélectionner.
 *
 * @return Si le mode désire être sélectionné
 * @retval true Le mode désire être sélectionné
 * @retval false Le mode ne veut pas être déselectionné
 */
bool TModeCycle::DesireSelection(bool) {
    return Cycle.Repetitions > 0;
}

/**
 * @brief Changer le nombre de repetitions et reinitialiser le nombre de
 * repetition executer
 *
 */
void TModeCycle::NouveauCycle(unsigned int Repetitions) {
    Cycle.Repetitions = Repetitions;
    Cycle.DureeChauffage = 0;
    Cycle.DureeVentilation = 0;
    FRepetition = 0;
    DerniereRepetition = 0;
    PremiereRepetition = true;
}
