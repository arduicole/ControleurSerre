/**
 * @file TModeCycle.h
 * @brief Déclaration de TModeCycle et TEtapeCycle
 */

#ifndef TModeCycle_h
#define TModeCycle_h

#include "../TActionneurBool/TActionneurBool.h"
#include "../TCycle/TCycle.h"
#include "../TGestionnaireMode/TGestionnaireMode.h"
#include "../TSelectionMoteur/TSelectionMoteur.h"

//! Étape dans un cycle
enum TEtapeCycle {
    //! Chauffage de la serre
    ecChauffage = 0,
    //! Ventilation de la serre
    ecVentilation = 1,

    ecPremier = ecChauffage,
    ecDernier = ecVentilation,
};

//! Gère l'exécution d'un cycle.
//! Le mode de cycle ne peut également être activé qu'à partir de
//! l'application. Il permet d'exécuter chauffer la serre puis de la
//! ventiler pour quelques répétitions ce qui peut entre autre permettre de
//! la déshumidifier.
class TModeCycle : public AMode {
private:
    //! Nombre de fois qu'un cycle s'est répété
    unsigned int FRepetition;
    //! Étape à laquelle on est rendu dans le cycle
    TEtapeCycle Etape;
    //! Temps au quel la répétition a commencé
    TTemps DebutEtape;
    //! Dernière fois que le cycle à été répété
    unsigned int DerniereRepetition;
    //! Temps écoulé
    unsigned int FTempsEcoule;
    //! Premiere repetition
    bool PremiereRepetition;

    //! Sélection du moteur.
    TSelectionMoteur &FSelectionMoteur;
    //! Fournaise.
    TFournaise &FFournaise;
    //! Ventilation.
    TVentilateur &FVentilateur;

public:
    //! Cycle à exécuter
    TCycle Cycle;
    //! Étape de départ
    TEtapeCycle EtapeDepart;

    //! Nom de ce mode.
    static const char *NOM_MODE;

    TModeCycle(TSelectionMoteur &selectionMoteur, TFournaise &fournaise,
               TVentilateur &ventilateur);

    void NouveauCycle(unsigned int Repetitions);
    unsigned int Repetition();
    const char *EtapeActuelle();
    unsigned int TempsEcoule(TEtapeCycle Etape);

    bool EffectuerDeselection() override;
    bool DesireSelection(bool premier) override;

    int Executer() override;
};

#endif
