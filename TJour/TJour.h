/**
 * @file TJour.h
 * @brief Déclaration de TJour
 */

#ifndef TJour_h
#define TJour_h

#include "../TConsigne/TConsigne.h"

//! Nombre de consignes dans un jour.
#define NB_CONSIGNES_JOUR 4

//! Représente une liste de consignes pour les différentes périodes d'une
//! journée.
union TJour {
    //! Liste de toutes les consignes dans l'ordre
    //! suivant: Prejour, Jour, Prenuit, nuit
    TConsigne Consignes[NB_CONSIGNES_JOUR];

    struct {
        //! Consigne au levé du soleil
        TConsigne Prejour;
        //! Consigne pendant le jour
        TConsigne Jour;
        //! Consigne au couché du soleil
        TConsigne Prenuit;
        //! Consigne pendant la nuit
        TConsigne Nuit;
    };
};

#endif
