/**
 * @file TModeCalibration.cpp
 * @brief Implémentation de TModeCalibration
 */

#include "TModeCalibration.h"

// Définition des constantes statiques de TModeCalibration.
const char *TModeCalibration::NOM_MODE = "calibration";

/**
 * @brief Initialise le mode de calibration avec #NOM_MODE
 * comme nom
 *
 */
TModeCalibration::TModeCalibration(
    //! Sélection du moteur.
    TSelectionMoteur &selectionMoteurs) :
    AMode(NOM_MODE),
    FMoteursCalibres(0), FSelectionMoteur(selectionMoteurs) {
}

/**
 * @brief Réinitialise la position de tous les moteurs qui n'ont pas
 * encore été calibrés à 100 %, change leur commande à 0% et sélectionne
 * le premier moteur à calibrer.
 *
 */
void TModeCalibration::EffectuerSelection() {
    if (DesireSelection(false)) {
        // Réinitialiser la position de tous les moteurs qui n'ont pas
        // encore été calibrés.
        for (int i = FMoteursCalibres; i < NB_MOTEURS; i++) {
            TMoteur &moteur = FSelectionMoteur.Moteurs[i];
            moteur.Reinitialiser(100.0);
            moteur.Commande(0.0);
        }
        // Sélectionner le premier moteur à calibrer.
        FSelectionMoteur.Etat(FMoteursCalibres);
    }
}

/**
 * @brief Enregistre le nombre de moteurs qui ont pu être calibrés et accepte
 * d'être déselectionné
 *
 * @return Si le mode accepte ou refuse d'être sélectionné
 * @retval true Ce mode accepte d'être déselectionné
 */
bool TModeCalibration::EffectuerDeselection() {
    if (FMoteursCalibres < NB_MOTEURS) {
        FMoteursCalibres = FSelectionMoteur.Etat();
    }
    return true;
}

/**
 * @brief Retourne si la calibration est terminée ou non
 *
 * @return Si ce mode désire être sélectionné
 */
bool TModeCalibration::DesireSelection(
    //! Si c'est le premier mode sélectionné
    bool) {
    return FMoteursCalibres < NB_MOTEURS;
}

/**
 * @brief Désactive le mode lorsque
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur n'est survenue
 * @retval -1 Le mode de calibration peut être désactivé
 */
int TModeCalibration::Executer() {
    FSelectionMoteur.Executer();

    int codeErreur = -1;
    if (DesireSelection(false)) {
        for (int i = FMoteursCalibres; i < NB_MOTEURS; i++) {
            TMoteur &moteur = FSelectionMoteur.Moteurs[i];
            // Si le moteur n'est pas encore fermé
            if (moteur.Position() != 0.0) {
                // Garder le mode activé.
                codeErreur = 0;
            }
        }

        // Si tous les moteurs sont calibrés, s'assurer que ce mode ne soit pas
        // réactivé.
        if (codeErreur == -1) {
            FMoteursCalibres = NB_MOTEURS;
        }
    }

    return codeErreur;
}
