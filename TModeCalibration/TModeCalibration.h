/**
 * @file TModeCalibration.h
 * @brief Déclaration de TModeCalibration
 */

#ifndef TModeCalibration_h
#define TModeCalibration_h

#include "../TGestionnaireMode/TGestionnaireMode.h"
#include "../TSelectionMoteur/TSelectionMoteur.h"
#include <Arduino.h>

//! Le mode de calibration après le démarrage avant tous les modes autres que le
//! mode manuel.
//! Il permet de complètement fermer les moteurs sans avoir besoin
//! de le faire dans `setup()` et d'empêcher le reste des tâches de s'exécuter.
class TModeCalibration : public AMode {
private:
    //! Nombre de moteurs calibrés.
    //! Les moteurs se calibrent un à un en commençant par le moteur à l'index
    //! 0.
    //! Cette variable représente donc aussi l'index du moteur en train
    //! d'être calibré.
    //! Lorsqu'elle est égale au nombre de moteurs, cela signifie que tous les
    //! moteurs sont calibrés.
    uint8_t FMoteursCalibres;

    //! Sélection du moteur.
    TSelectionMoteur &FSelectionMoteur;

protected:
    virtual void EffectuerSelection();
    virtual bool EffectuerDeselection();

public:
    //! Nom de ce mode tel que retourné par #Nom.
    static const char *NOM_MODE;

    TModeCalibration(TSelectionMoteur &selectionMoteurs);

    bool DesireSelection(bool premier) override;

    int Executer() override;
};

#endif
