/**
 * @file TVerifTempCritique.cpp
 * @brief Implémentation de TVerifTempCritique
 */

#include "TVerifTempCritique.h"

//! Message de l'alerte lorsque la température a atteint un niveau critique.
static const char *MSG_ALERTE_TEMP_CRITIQUE =
    "Plusieurs erreurs pourraient causer ce problème. "
    "La fournaise, le ventilateur ou un rollup pourrait ne pas "
    "fonctionner. Il est aussi possible qu'un des capteurs "
    "retourne de mauvaises mesures. Le plus probable est cependant "
    "que la température ne puisse pas être asservit, car la "
    "température extérieure est trop chaude ou froide."
    "Vous pouvez manuellement tenter de contrôler la température "
    "de votre serre à l'aide du mode de contrôle à distance ou du "
    "mode de contrôle manuel.";

//! Titres des différentes alertes.
static const char *TITRES_ALERTES[] = {
    "La température est sous contrôle",
    "La température est inférieure au seuil critique",
    "La température est supérieure au seuil critique",
    "Aucun des capteurs ne semble fonctionner",
};

//! Messages des différentes alertes.
static const char *MSGS_ALERTES[] = {
    "La température est maintenant dans la zone d'hystérésis.",
    MSG_ALERTE_TEMP_CRITIQUE,
    MSG_ALERTE_TEMP_CRITIQUE,
    "La température ne peut pas être surveillée, car aucun des capteurs ne "
    "semble fonctionner. Il est donc impossible pour la température de la "
    "serre d'être surveillée et d'être maintenue.",
};

/**
 * @brief Initialise un vérificateur de température
 *
 */
TVerifTempCritique::TVerifTempCritique(
    //! Intervalle entre les vérifications des seuils de température
    TTemps periode,
    //! Capteurs dont la température doit être surveillée
    TGroupeCapteurs &capteurs,
    //! Fournisseur de consigne
    TFournisseurConsigne &fournisseurConsigne,
    //! Gestionnaire d'alertes
    TAlertesSynchronisees &alertesSync) :
    ATachePeriodique(periode),
    FCapteurs(capteurs), FFournisseurConsigne(fournisseurConsigne),
    FAlertesSync(alertesSync), FCodeErreurPrecedente(0),
    FIdAlerte(ID_ALERTE_PERMANENTE) {
}

/**
 * @brief Performe la vérification de la température et
 * la création d'une alerte si nécessaire
 * @note Si les températures des capteurs ne peuvent pas être lues, le code
 * d'erreur retourné sera 0, car les erreurs de lecture de capteurs créent déjà
 * des alertes
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval 1 La température d'un capteur est inférieure au seuil critique bas
 * @retval 2 La température d'un capteur est supérieure au seuil critique haut
 */
int TVerifTempCritique::ExecuterPeriodique() {
    // Déterminer la consigne.
    const TConsigne &consigne = FFournisseurConsigne.DeterminerConsigne();

    // Déterminer si la température d'un capteur dépasse les seuils critiques.
    int codeErreur = 3;
    for (TCapteur &capteur : FCapteurs.Capteurs) {
        TMesureCapteur temp = capteur.TemperatureCourtTerme.Sommaire().Moyenne;
        if (isnormal(temp)) {
            if (temp < consigne.TempMinCritique) {
                codeErreur = 1;
            } else if (temp > consigne.TempMaxCritique) {
                codeErreur = 2;
            } else if (codeErreur == 3) {
                codeErreur = 0;
            }
        }
    }

    // Si le code d'erreur a changé
    if (FCodeErreurPrecedente != codeErreur) {
        // S'assurer que si la température est maintenant entre les seuils
        // critiques qu'elle soit aussi dans la zone de l'hystérésis.
        bool publierAlerte = true;
        if (codeErreur == 0 && FCodeErreurPrecedente != 3) {
            for (TCapteur &capteur : FCapteurs.Capteurs) {
                TMesureCapteur temp =
                    capteur.TemperatureCourtTerme.Sommaire().Moyenne;
                if (isnormal(temp)) {
                    if (FCodeErreurPrecedente == 1) {
                        if (temp < (consigne.TempCible - consigne.Hysteresis)) {
                            publierAlerte = false;
                        }
                    } else if (temp >
                               (consigne.TempCible + consigne.Hysteresis)) {
                        publierAlerte = false;
                    }
                }
            }
        }

        if (publierAlerte) {
            // Rendre l'alerte précédente temporaire.
            TAlerte *alertePrecedente = FAlertesSync.AlerteId(FIdAlerte);
            if (alertePrecedente != nullptr) {
                alertePrecedente->RendreTemporaire();
            }

            // Créer la nouvelle alerte.
            if (!(FCodeErreurPrecedente == 3 && codeErreur == 0)) {
                FIdAlerte = FAlertesSync.AjouterAlerte(
                    TITRES_ALERTES[codeErreur], MSGS_ALERTES[codeErreur],
                    uaElevee,
                    codeErreur == 0 ? ID_ALERTE_TEMPORAIRE
                                    : ID_ALERTE_PERMANENTE);
            }

            FCodeErreurPrecedente = codeErreur;
        }
    }

    return FCodeErreurPrecedente;
}
