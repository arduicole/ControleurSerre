/**
 * @file TVerifTempCritique.h
 * @brief Implémentation de TVerifTempCritique
 */

#ifndef TVerifTempCritique_H
#define TVerifTempCritique_H

#include "../ATachePeriodique/ATachePeriodique.h"
#include "../TAlertesSynchronisees/TAlertesSynchronisees.h"
#include "../TFournisseurConsigne/TFournisseurConsigne.h"
#include "../TGroupeCapteurs/TGroupeCapteurs.h"

//! Vérifie que les seuils de température critique
//! minimale et maximale n'ont pas changés.
class TVerifTempCritique : public ATachePeriodique {
    //! Capteurs dont la température doit être surveillée.
    TGroupeCapteurs &FCapteurs;

    //! Fournisseur de consigne
    TFournisseurConsigne &FFournisseurConsigne;
    //! Gestionnaire d'alertes
    TAlertesSynchronisees &FAlertesSync;

    //! Code d'erreur précédemment retourné par ExecuterPeriodique.
    //! Ceci représente également le type de la dernière alerte.
    //! Voir ExecuterPeriodique pour la signification de sa valeur.
    int FCodeErreurPrecedente;
    //! ID de la dernière alerte créée.
    uint32_t FIdAlerte;

public:
    TVerifTempCritique(TTemps periode, TGroupeCapteurs &capteurs,
                       TFournisseurConsigne &fournisseurConsigne,
                       TAlertesSynchronisees &alertesSync);

protected:
    int ExecuterPeriodique() override;
};

#endif /* TVerifTempCritique_H */
