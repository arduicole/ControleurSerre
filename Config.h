/**
 * @file Config.h
 * @brief Configuration du contrôleur
 *
 * Ceci est le fichier de configuration de votre contrôleur.
 * Même si vous n'avez pas d'expérience en programmation, ce fichier est fait
 * pour que vous puissiez facilement l'éditer pour changer la configuration de
 * votre contrôleur.
 *
 * Certaines des informations dans ce fichier, comme les mots de passe, doivent
 * rester secrètes. Vous ne devez même pas les montrer aux développeurs du
 * projet. Si vous les divulguer par accident, veuillez rapidement les changer
 * et reprogrammer votre contrôleur.
 *
 * Pour plus d'informations sur comment modifier ce fichier, veuillez consulter
 * le manuel d'usager: https://arduicole.gitlab.io/manuel-usager
 */

#include "Temps/Temps.h"

//==============================================================================
// Réseau WiFi
//==============================================================================

//! Nom de votre réseau WiFi tel qu'affiché sur vos appareils qui y sont
//! connectés.
//!
//! ATTENTION! La valeur que vous entrez ici doit être identique au nom de
//! votre réseau. Il faut donc qu'elle ait les mêmes lettres majuscules, les
//! mêmes accents, la même ponctuation, etc.
#define NOM_WIFI "Votre Réseau"

//! Mot de passe de votre réseau WiFi.
//!
//! ATTENTION! La valeur que vous entrez ici doit être identique au mot de passe
//! de votre réseau. Il faut donc qu'elle ait les mêmes lettres majuscules, les
//! mêmes accents, la même ponctuation, etc.
#define MOT_DE_PASSE_WIFI "Votre mot de passe"

//! Nom du point d'accès que crééra votre serre lorsqu'elle est déconnectée de
//! votre réseau nommé NOM_WIFI.
//!
//! ATTENTION! Veuillez donner un nom différent à chacune de vos serres. Si
//! elles ont toutes le même nom, vous serrez incapable de les différencier.
//!
//! Le réseau créé par votre serre ne sera disponible que lorsqu'elle ne sera
//! pas connectée à votre réseau et ne nécessitera aucun mot de passe pour vous
//! y connecter.
#define NOM_POINT_ACCES "Serre #1"

//==============================================================================
// Synchronisation des capteurs avec Google Sheet
//==============================================================================

//! Identifiant du GScript utilisé pour publier les informations des capteurs
//! périodiquement dans votre Google Sheet.
//!
//! ATTENTION! La valeur déjà présente est utilisée pour faire des tests.
//! Vous devez absolument la remplacer par l'identifiant de votre propre
//! GScript.
#define ID_GSCRIPT "AKfycbzow4RKbP1QCVnKt0uSCneli87HMKISXzx6JSR8gy7IHcRXMsVY"

//! Identifiant du tableur dans lequel écrire les données.
//!
//! ATTENTION! La valeur déjà présente est utilisée pour faire des tests.
//! Vous devez absolument la remplacer par l'identifiant de votre propre
//! tableur.
#define ID_TABLEUR "1z8vQ3vUCyoi0sj1BVqjNvF7AQlTIBJuVAWZDBD1RBRY"

//! Nom de la page dans le tableur dans laquelle écrire les données.
//!
//! ATTENTION! Les données déjà présentes sont utilisés pour faire des tests.
//! Vous devez absolument les remplacer par vos propres données.
#define NOM_PAGE_TABLEUR "Serre #1"

//! Intervalle en secondes de synchronisation des données de vos capteurs dans
//! votre Google Sheets.
//!
//! Ex.: Si vous mettez une valeur de une heure, toutes les heures les données
//! des capteurs seront enregistrées dans votre Google Sheet.
#define INTERVALLE_SYNCHRONISATION (1 * HEURE_MS)

//==============================================================================
// Accès au contrôleur de serre avec l'application
//==============================================================================

//! Mot de passe de l'application qui donne le droit à un usager de faire
//! n'importe quelle opération.
//!
//! Si vous divulgez ce mot de passe à quelqu'un d'autre par accident, il pourra
//! se connecter sur votre serre et faire tout ce qu'il veut. Ceci pourrait
//! potentiellement vous faire perdre votre récolte.
//!
//! Assurez-vous également d'utiliser le mot de passe le plus compliqué possible
//! afin qu'il ne puisse pas être facilement brisé. Voir
//! https://fr.wikipedia.org/wiki/Robustesse_d%27un_mot_de_passe.
//!
//! Vous pouvez utiliser n'importe quelle combinaison de lettres, de chiffres et
//! de caractères spéciaux. Nous vous recommandons d'utiliser un générateur de
//! mot de passe. ex.: https://www.lastpass.com/fr/password-generator
#define MOT_DE_PASSE_SUPER_USAGER "Changer ce mot de passe!"

//! Mot de passe de l'application qui donne le droit à un usager de lire l'état
//! de la serre, mais pas de modifier sa configuration.
//! Ceux utilisant ce mot de passe ne pourront faire que des GET requests.
//!
//! Si vous ne comptez pas utiliser ce mot de passe, nous vous encourageons à le
//! désactiver. Pour le désactiver, changer sa valeur pour "".
#define MOT_DE_PASSE_LECTURE_SEULEMENT "Changer aussi ce mot de passe!"

//! Port du serveur.
//!
//! Toutes vos serres peuvent avoir le même port sans que ça cause le moindre
//! problème. Nous ne vous recommandons donc pas de modifier ce paramètre.
#define PORT_SERVEUR 443

//==============================================================================
// Alertes
//==============================================================================

//! S'il faut utiliser pour les alertes.
//! Si la valeur de cette constante est 0, FCM ne sera pas utiliser, sinon FCM
//! sera utilisé.
//!
//! Utiliser FCM permet de recevoir presque instantannément les alertes dans
//! l'application et de les recevoir tant qu'on a une connexion à Internet.
//! L'application va tout de même pouvoir obtenir les alertes de la serre sans
//! FCM, mais FCM n'a que des avantages et aucun inconvénient.
#define ALERTES_UTILISER_FCM 0

//! Clé du serveur pour le projet FCM.
//! Ceci est un identifiant unique qui permet de s'authentifier auprès de FCM
//! pour avoir l'autorisation d'envoyer des messages à des appareils.
//! Cette même clé peut être réutilisée pour plusieurs serres.
//!
//! NOTE: Ceci n'est applicable que si ALERTES_UTILISER_FCM n'est pas 0.
#define CLE_SERVEUR_FCM "CHANGER"

//! Nom du canal qui sera utilisé pour envoyer les alertes à l'application
//! mobile et pour que l'application mobile reconnaisse que les alertes
//! proviennent de cette serre.
//!
//! Il est important que 2 serres n'utilisent pas la même valeur pour ce
//! paramètre, sinon l'application ne pourra pas différiencier les alertes des
//! deux serres.
//!
//! NOTE: Ceci n'est applicable que si ALERTES_UTILISER_FCM n'est pas 0.
#define TOPIC_FCM "CHANGER"

//! Délai en millisecondes entre les envois des alertes par FCM.
//!
//! NOTE: Ceci n'est applicable que si ALERTES_UTILISER_FCM n'est pas 0.
#define DELAI_ENVOI_ALERTES (15 * SECONDE_MS)

//==============================================================================
// Rollups
//==============================================================================

//! Temps en millisecondes que prend le premier rollup pour passer
//! de 0% d'ouverture à 100% d'ouverture.
#define DUREE_OUVERTURE_ROLLUP_1 10000

//! Temps en millisecondes que prend le premier rollup pour passer
//! de 100% d'ouverture à 0% d'ouverture.
#define DUREE_FERMETURE_ROLLUP_1 8000

//! Délai en millisecondes entre les changements de direction du moteur du
//! premier rollup.
//!
//! ATTENTION! Si ce délai est trop court, le moteur va changer brusquement
//! de direction ce qui va créer un pic de tension qui, à la longue, va
//! endommager votre contrôleur.
//! Les changements trop brusques de direction nuisent également au calcul de la
//! position des rollups. Ceci fera donc en sorte que le pourcentage
//! d'ouverture de vos rollups dans l'application mobile ne correspondera pas
//! exactement à la valeur réelle.
//!
//! Voici une procédure pour vous aider à déterminer cette valeur:
//! 1. Placer votre contrôleur en mode manuel.
//! 2. Faire ouvrir complètement le rollup.
//! 3. Faire fermer le rollup.
//! 4. Remettre le rollup à neutre et calculer le temps qu'il a pris pour
//! s'arrêter. Il est possible que cette valeur soit très petite. ex.: moins de
//! 1 seconde.
//! 5. Ajouter un délai de sécurité de 1 seconde à cette valeur et l'utiliser
//! comme DELAI_CHANGEMENT_DIRECTION_ROLLUP_1.
//!
//! Si vous voulez être prudent, vous pouvez ajouter un délai de sécurité plus
//! grand. Il n'y a aucune conséquence à ce que ce délai soit trop grand. Vos
//! changements de direction vont seulement prendre plus de temps.
#define DELAI_CHANGEMENT_DIRECTION_ROLLUP_1 5000

//! Différence acceptable entre la commande du premier rollup et la position à
//! laquelle il se trouve. Cette valeur peut être un nombre décimale.
//!
//! Ex.: Si vous avez un hystérésis de 5%, lorsque vous utiliserez le mode de
//! contrôle manuel à distance, par exemple, et que vous enverrez la commande au
//! rollup de s'ouvrir à 50%, il s'ouvrira entre 45% et 55%. Cet hystérésis n'a
//! cependant aucun effet lorsque vous réglez le pourcentage d'ouverture à 0% ou
//! 100%.
//!
//! ATTENTION! Si cette valeur est trop basse, votre rollup va constamment
//! être en marche ce qui va dépenser de l'énergie pour rien. De plus, il n'y
//! aura qu'un seul rollup qui s'ajustera.
#define HYSTERESIS_ROLLUP_1 5.0

//! Temps en millisecondes que prend le deuxième rollup pour passer
//! de 0% d'ouverture à 100%.
#define DUREE_OUVERTURE_ROLLUP_2 10000

//! Temps en millisecondes que prend le deuxième rollup pour passer
//! de 100% d'ouverture à 0%.
#define DUREE_FERMETURE_ROLLUP_2 8000

//! Délai en millisecondes entre les changements de direction du moteur du
//! premier rollup.
//! Voir DELAI_CHANGEMENT_DIRECTION_ROLLUP_1
#define DELAI_CHANGEMENT_DIRECTION_ROLLUP_2 5000

//! Différence acceptable entre la commande du premier rollup et la position à
//! laquelle il se trouve. Cette valeur peut être un nombre décimale.
//! Voir HYSTERESIS_ROLLUP_1
#define HYSTERESIS_ROLLUP_2 5.0

//! Délai en millisecondes entre les changements non nécessaires de sélection de
//! moteurs. Ceci n'affecte pas le mode de contrôle manuel.
//! Les changements non nécessaires surviennent lorsque les deux moteurs doivent
//! être complètement ouverts ou fermés et qu'ils vont, à tour de rôle, être
//! sélectionnés pour se diriger dans la bonne direction même si ce n'est pas
//! nécessaire.
#define DELAI_CHANGEMENT_OPTIONNEL_SELECTION_MOTEUR 60000

//==============================================================================
// Fuseau horaire et synchronisation de l'heure
//==============================================================================

//! Décalage en secondes du fuseau horaire dans lequel se trouve votre serre
//! par rapport à l'heure de Greenwich. Ce nombre doit être négatif si vous êtes
//! à l'Ouest du méridien de Greenwich et positif sinon.
//! ATTENTION! Ce décalage ne doit pas inclure l'heure d'été.
//!
//! Ex.: Si votre serre est à New York, le décalage sera de -5h.
#define FUSEAU_HORAIRE (-5 * HEURE)

//! Décalage en seconde, positif ou négatif, de l'heure d'été.
//! Si votre région n'utilise pas l'heure d'été, assignez `0` à cette valeur.
//! ATTENTION! Ce décalage n'est appliqué que pendant l'été.
//!
//! Ex.: À New York, l'heure d'été est un décalage de 1h vers l'heure UTC.
#define DECALAGE_HEURE_ETE (1 * HEURE)

//! Liste des serveurs NTP officiels de votre région en ordre d'importance.
//! Le serveur #1 est le plus important et le #3 est le moins important.
//!
//! Ces serveurs permettent la synchronisation de l'heure du contrôleur.
//! Pour trouver quels serveurs se trouvent dans votre région,
//! voir https://www.ntppool.org/fr/zone/@.
#define SERVEUR_NTP_1 "0.ca.pool.ntp.org"
#define SERVEUR_NTP_2 "1.ca.pool.ntp.org"
#define SERVEUR_NTP_3 "2.ca.pool.ntp.org"

//==============================================================================
// Noms des capteurs et des actionneurs tels qu'ils
// seront affichés dans l'application
//==============================================================================

//! Nom du premier rollup.
#define NOM_ROLLUP_1 "Rollup Est"

//! Nom du deuxième rollup.
#define NOM_ROLLUP_2 "Rollup Ouest"

//! Nom de la fournaise.
#define NOM_FOURNAISE "Fournaise"

//! Nom du ventilateur.
#define NOM_VENTILATEUR "Ventilateur"

//! Nom du premier capteur.
#define NOM_CAPTEUR_1 "Capteur 1/3"

//! Nom du deuxième capteur.
#define NOM_CAPTEUR_2 "Capteur 2/3"

//==============================================================================
// Asservissement
//==============================================================================

//! Incrément de la position des rollups en pourcentage.
#define INCREMENT_POSITION_ROLLUPS 25.0

//! Délai en secondes entre les asservissements automatiques des actionneurs.
//!
//! Ceci ne s'applique que si le mode d'asservissement est activé.
#define INTERVALLE_ASSERVISSEMENT (1 * MINUTE_MS)

//==============================================================================
// Paramètres de la réinitialisation du contrôleur
//==============================================================================

//! Délai minimal en millisecondes entre les réinitialisations du contrôleur.
//! ATTENTION ce délai doit être inférieur à 4 208 567 295 ms (2^32 - JOUR_MS)
//! soit environ 48.7 jours.
//!
//! Valeur par défaut: 7j
//!
//! Ce délai permet de réinitialiser périodiquemente le contrôleur de manière à
//! diminuer les chances qu'une erreur puisse survenir. C'est un peu comme
//! redémarrer votre ordinateur une fois de temps à autres parce qu'il a
//! ralentit.
//! Le délai par défaut devrait faire l'affaire, donc vous n'avez pas besoin d'y
//! toucher.
#define INTERVALLE_REINITIALISATION (7 * JOUR_MS)

//! Heure de la journée à laquelle le contrôleur est réinitialisé après que le
//! délai INTERVALLE_REINITIALISATION soit écoulé. Cette valeur doit être entre
//! 0 et 23 inclusivement.
//!
//! Valeur par défaut: 13.
//!
//! Si cette valeur est 14, le contrôleur va redémarré à 14h après que
//! l'intervalle de réinitialisation (INTERVALLE_REINITIALISATION) soit écoulé.
#define HEURE_REINITIALISATION 13

//==============================================================================
// Configations diverses
//==============================================================================

//! Nombre maximales de lectures qui peuvent être ratées d'affilée
//! avant qu'une alerte ne soit envoyée.
#define LECTURES_RATEES_MAX 30
