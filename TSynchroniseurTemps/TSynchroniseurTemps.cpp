/**
 * @file TSynchroniseurTemps.cpp
 * @brief Implémentation de TSynchroniseurTemps
 */

#include "TSynchroniseurTemps.h"

//! Délai entre deux mesures du temps à partir duquel on considère qu'une
//! synchronisation par NTP a été faite.
//! Nous pourrions utiliser settimeofday_cb pour recevoir une notification, mais
//! cette notification est parfois délayé ce qui fait en sorte qu'on est ensuite
//! incapable de calculer de combien de secondes le temps a été ajusté.
#define DELAI_SYNCHRONISATION (5 * MINUTE)

/**
 * @brief Initialise le synchroniseur de temps avec l'intervalle de
 * synchronisation donné
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 */
TSynchroniseurTemps::TSynchroniseurTemps(
    //! Fuseau horaire
    int fuseauHoraire,
    //! Décalage de l'heure d'été
    int heureEte) :
    FuseauHoraire(fuseauHoraire),
    HeureEte(heureEte) {
    FEnSynchronisation = false;

    struct timeval tv;
    gettimeofday(&tv, nullptr);
    FTempsPrecedent = tv.tv_sec;
}

/**
 * @brief Ajoute un nouvel observateur de l'heure.
 *
 * Un observateur de l'heure est un objet qui est notifié lorsque l'heure
 * change. La notification contient le nombre de secondes par laquelle l'heure a
 * été mise à jour.
 *
 */
void TSynchroniseurTemps::AjouterObservateur(
    //! Observateur à ajouter
    IObservateurHeure &observateur) {
    FObservateurs.push_front(&observateur);
}

/**
 * @brief Retourne si l'heure a été synchronisée par NTP
 *
 * @return Si l'heure a été synchronisée par NTP
 */
bool TSynchroniseurTemps::EstSynchronise() {
    return FSynchronise;
}

/**
 * @brief Synchronise le RTC avec le protocole NTP
 * @note Implémentation de ATachePeriodique::ExecuterPeriodique
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 */
int TSynchroniseurTemps::Executer() {
    if (WiFi.status() == WL_CONNECTED && !FEnSynchronisation) {
        // Démarrer la synchronisation par NTP.
        configTime(FuseauHoraire, HeureEte, SERVEUR_NTP_1, SERVEUR_NTP_2,
                   SERVEUR_NTP_3);
        FEnSynchronisation = true;
    }

    if (!FSynchronise) {
        struct timeval tv;
        gettimeofday(&tv, nullptr);
        int32_t delta = tv.tv_sec - FTempsPrecedent;

        // Si le delta depuis la dernière synchronisation dépasse le delta.
        if (delta > DELAI_SYNCHRONISATION) {
            // On considère que l'heure a été synchronisée.
            FSynchronise = true;

            // Notifier les observateurs.
            for (auto &observateur : FObservateurs) {
                observateur->SynchroniserHeure(delta);
            }
        }

        FTempsPrecedent = tv.tv_sec;
    }

    return 0;
}
