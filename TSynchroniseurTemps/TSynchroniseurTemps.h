/**
 * @file TSynchroniseurTemps.h
 * @brief Déclaration de TSynchroniseurTemps
 */

#ifndef TSynchroniseurTemps_h
#define TSynchroniseurTemps_h

#include "../ITache/ITache.h"
#include <forward_list>

//! Interface à implémenter pour s'enregistrer auprès d'un TSynchroniseurTemps
//! pour être notifié lorsque l'heure est synchronisée.
class IObservateurHeure {
public:
    /**
     * @brief Méthode appelée par TSynchroniseurTemps pour notifier un objet
     * d'une synchronisation de l'heure.
     *
     */
    virtual void SynchroniserHeure(
        //! Différence entre l'heure précédente et l'heure synchronisée.
        //! Le delta donné peut être ajouté à n'importe quelle heure qui n'était
        //! pas synchronisée pour qu'elle le soit.
        int32_t delta) = 0;
};

//! Synchronise le RTC avec le protocole NTP à un intervalle donné.
//! Ceci est un singleton.
class TSynchroniseurTemps : public ITache {
private:
    //! Liste des observateurs de l'heure.
    std::forward_list<IObservateurHeure *> FObservateurs;

    //! Si l'heure a été synchronisée.
    bool FSynchronise;
    //! Si la synchronisation par NTP est activée.
    bool FEnSynchronisation;
    //! Dernière heure enregistrée.
    //! Ceci est utilisé pour déterminer le delta lorsque
    //! SynchronisationCB est appelé.
    time_t FTempsPrecedent;

public:
    //! Fuseau horaire.
    const int FuseauHoraire;
    //! Décalage de l'heure d'été.
    //! Si l'heure d'été ne doit pas être utilisée, assigner cette valeur à 0.
    const int HeureEte;

    TSynchroniseurTemps(int fuseauHoraire, int heureEte);

    void AjouterObservateur(IObservateurHeure &observateur);

    bool EstSynchronise();

    int Executer() override;
};

#endif
