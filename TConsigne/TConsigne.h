/**
 * @file TConsigne.h
 * @brief Déclaration de TConsigne
 */

#ifndef TConsigne_h
#define TConsigne_h

#include "../TCapteur/TCapteur.h"

//! Contient les informations pour l'asservissement de la température de la
//! serre
struct TConsigne {
    //! Température à laquelle doit être maintenue la serre
    TMesureCapteur TempCible;
    //! Plage autour de la température cible où on tente de maintenir la
    //! température
    TMesureCapteur Hysteresis;
    //! Seuil critique haut de la température
    TMesureCapteur TempMaxCritique;
    //! Seuil critique bas de la temperature
    TMesureCapteur TempMinCritique;
};

#endif
