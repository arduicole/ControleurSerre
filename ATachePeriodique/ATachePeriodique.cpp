/**
 * @file ATachePeriodique.cpp
 * @brief Implémentation de ATachePeriodique
 */

#include "ATachePeriodique.h"

/**
 * @brief Initialise la tâche avec une période donnée
 *
 */
ATachePeriodique::ATachePeriodique(
    //! Intervalle d'exécution de la tâche périodique en millisecondes
    TTemps periode) :
    Periode(periode),
    FDerniereExecution(0) {
}

/**
 * @brief Initialise la tâche avec une période donnée
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval * Voir l'erreur retournée par l'implémentation
 * de ExecuterPeriodique
 */
int ATachePeriodique::Executer() {
    int codeErreur = 0;

    // Si la période est dépassée
    TTemps maintenant = millis();
    if (maintenant - FDerniereExecution > Periode) {
        // Exécuter la tâche périodique.
        codeErreur = ExecuterPeriodique();

        // Assigner l'heure actuelle à l'heure de la dernière exécution.
        FDerniereExecution = maintenant;
    }

    return codeErreur;
}

/**
 * @brief Réinitialise le délai d'exécution afin que la tâche soit exécutée
 * la prochaine fois que #Executer est appelé
 *
 */
void ATachePeriodique::Reinitialiser() {
    FDerniereExecution = millis() - Periode;
}
