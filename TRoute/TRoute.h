/**
 * @file TRoute.h
 * @brief Déclaration de TRoute
 */

#ifndef TRoute_h
#define TRoute_h

#include "../TServeur/TServeur.h"

//! Répond aux requêtes HTTP pour un chemin spécifique.
//! Les méthodes Get, Post, Put, Head, Delete, Patch et Options sont
//! appelées en fonction de la méthode HTTP de la requête. Elles possèdent
//! toute une implémentation par défaut dans laquelle elles retournent une
//! erreur de méthode invalide.
class TRoute : public esp8266webserver::RequestHandler<WiFiServerSecure> {
    using WebServerType =
        esp8266webserver::ESP8266WebServerTemplate<WiFiServerSecure>;

private:
    //! Serveur auquel la route s'attache
    TServeur &FServeur;
    //! Si le client doit être authentifier pour avoir accès à cette route
    bool FAuthentification;
    //! Chemin auquel répond cette route. ex.: /type-jour
    const char *FChemin;

public:
    TRoute(TServeur &serveur, const char *chemin, bool authentification = true);

    bool canHandle(HTTPMethod methode, String uri) override;
    bool handle(WebServerType &serveur, HTTPMethod method, String uri) override;

    int Authentifier(TServeur &serveur);

    virtual int Get(TServeur &serveur);
    virtual int Post(TServeur &serveur);
    virtual int Put(TServeur &serveur);
    virtual int Head(TServeur &serveur);
    virtual int Delete(TServeur &serveur);
    virtual int Patch(TServeur &serveur);
    virtual int Options(TServeur &serveur);

    bool Authentification();
};

/**
 * @brief Retourne si cette route requiert l'authentification du client
 *
 * @return Si cette route requiert l'authentification du client
 */
inline bool TRoute::Authentification() {
    return FAuthentification;
}

#endif /* TRoute_h */
