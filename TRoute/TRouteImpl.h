/**
 * @file TRouteImpl.h
 * @brief Déclaration d'implémentations de TRoute
 */

#ifndef TRouteImpl_h
#define TRouteImpl_h

#include "../TActionneurBool/TActionneurBool.h"
#include "../TMoteur/TMoteur.h"
#include "TRoute.h"

//! Retourne le certificat HTTPS sans authentification
class TRouteCertificat : public TRoute {
public:
    TRouteCertificat(TServeur &serveur, const char *chemin);

    int Get(TServeur &serveur) override;
};

#include "../TGestionnaireMode/TGestionnaireMode.h"

//! Retourne le mode d'exécution en cours
class TRouteMode : public TRoute {
private:
    //! Asservissement.
    TGestionnaireMode &FGestionnaireMode;

public:
    TRouteMode(TGestionnaireMode &gestionnaireMode, TServeur &serveur,
               const char *chemin);

    int Get(TServeur &serveur) override;
};

#include "../TGroupeCapteurs/TGroupeCapteurs.h"

//! Retourne les données de tous les capteurs
class TRouteCapteurs : public TRoute {
private:
    //! Groupe de capteurs dont il faut obtenir l'état.
    TGroupeCapteurs &FGroupeCapteurs;

public:
    TRouteCapteurs(TGroupeCapteurs &groupeCapteurs, TServeur &serveur,
                   const char *chemin);

    int Get(TServeur &serveur) override;

    JsonObject InsererCapteur(JsonArray &array, const char *nom,
                              TMesureCapteur mesure, const char *unite);
};

#include "../TModeControleDistance/TModeControleDistance.h"
#include "../TSelectionMoteur/TSelectionMoteur.h"

//! Retourne l'état de tous les actionneurs
class TRouteActionneurs : public TRoute {
private:
    //! Gestionnaire de mode.
    TGestionnaireMode &FGestionnaireMode;
    //! Mode de contrôle à distance des actionneurs.
    TModeControleDistance &FModeCtrlDistance;
    //! Moteurs des rollups.
    TSelectionMoteur &FSelectionMoteur;
    //! Ventilateur.
    TVentilateur &FVentilateur;
    //! Chauffage.
    TFournaise &FFournaise;

public:
    TRouteActionneurs(TGestionnaireMode &gestionnaireMode,
                      TModeControleDistance &modeCtrlDistance,
                      TSelectionMoteur &selectionMoteur,
                      TVentilateur &ventilateur, TFournaise &fournaise,
                      TServeur &serveur, const char *chemin);

    int Get(TServeur &serveur) override;
    int Post(TServeur &serveur) override;
    int Delete(TServeur &serveur) override;

    template <typename TEtat>
    JsonObject InsererActionneur(JsonArray &array, const char *type,
                                 const char *nom, TEtat etat);
};

#include "../TAlertesSynchronisees/TAlertesSynchronisees.h"

//! Retourne une liste d'alertes survenues
class TRouteAlertes : public TRoute {
private:
    //! Gestionnaire d'alertes.
    TAlertesSynchronisees &FAlertes;

public:
    TRouteAlertes(TAlertesSynchronisees &alertes, TServeur &serveur,
                  const char *chemin);

    int Get(TServeur &serveur) override;
};

#include "../TFournisseurConsigne/TFournisseurConsigne.h"

//! Retourne le type de jour de la serre et permet de le changer
class TRouteTypeJour : public TRoute {
private:
    //! Fournisseur de consigne
    TFournisseurConsigne &FFournisseurConsigne;

public:
    TRouteTypeJour(TFournisseurConsigne &fournisseurConsigne, TServeur &serveur,
                   const char *chemin);

    int Get(TServeur &serveur) override;
    int Post(TServeur &serveur) override;
};

//! Retourne les consignes de la serre et permet de les modifier
class TRouteConsignes : public TRoute {
private:
    //! Fournisseur de consigne
    TFournisseurConsigne &FFournisseurConsigne;

public:
    TRouteConsignes(TFournisseurConsigne &fournisseurConsigne,
                    TServeur &serveur, const char *chemin);

    int Get(TServeur &serveur) override;
    int Post(TServeur &serveur) override;

    JsonObject InsererJour(JsonObject &obj, const char *typeJour, TJour &jour);
};

#include "../TModeCycle/TModeCycle.h"

//! Retourne le cycle en cours, permet d'en schéduler un nouveau ou de l'arrêter
class TRouteCycle : public TRoute {
private:
    //! Fournisseur de consigne
    TFournisseurConsigne FFournisseurConsigne;
    //! Tâche du cycle.
    TModeCycle &FModeCycle;
    //! Gestionnaire de mode.
    TGestionnaireMode &FGestionnaireMode;

public:
    TRouteCycle(TFournisseurConsigne fournisseurConsigne, TModeCycle &modeCycle,
                TGestionnaireMode &gestionnaireMode, TServeur &serveur,
                const char *chemin);

    int Get(TServeur &serveur) override;
    int Post(TServeur &serveur) override;
    int Delete(TServeur &serveur) override;
};

#endif /* TRouteImpl_h */
