/**
 * @file TRoute.cpp
 * @brief Implémentation de TRoute
 */

#include "TRoute.h"
#include "../Config.h"

//! Mot de passe pour effectuer n'importe quelle opération sur le serveur.
static const char MDP_SUPER_USAGER[] = MOT_DE_PASSE_SUPER_USAGER;
//! Mot de passe pour uniquement effectuer des GET requests sur le serveur.
static const char MDP_LECTURE_SEULEMENT[] = MOT_DE_PASSE_LECTURE_SEULEMENT;

//=============================================================================
// Constantes pour l'authentification
//=============================================================================
//! Entête HTTP pour l'authentification
static const char *ENTETE_AUTHENTIFICATION = "Authorization";
//! Schéma d'authentification
static const char *SCHEMA_AUTHENTIFICATION = "Basic ";
//! Schéma d'authentification
static const int TAILLE_SCHEMA_AUTHENTIFICATION = 6;

//==============================================================================
// Implémentation de TRoute
//==============================================================================

/**
 * @brief Créer une nouvelle route avec une référence de document JSON statique
 * donné qui requiert ou non l'authentification du client
 *
 */
TRoute::TRoute(TServeur &serveur,
               //! Chemin de cette route
               const char *chemin,
               //! Si la route requiert l'authentification du client
               bool authentification) :
    FServeur(serveur),
    FAuthentification(authentification), FChemin(chemin) {
}

/**
 * @brief Authentifie la requête si nécessaire et appelle la méthode associée à
 * la méthode HTTP de la requête
 *
 * @return Si le URI correspond au chemin de cette route
 */
bool TRoute::canHandle(
    //! Méthode HTTP de la requête
    HTTPMethod methode,
    //! Chemin demandé pour la requête
    String uri) {
    (void)(methode);
    return uri == FChemin;
}

/**
 * @brief Authentifie la requête si nécessaire et appelle la méthode associée à
 * la méthode HTTP de la requête
 *
 * @return Si la méthode a répondu à la requête
 */
bool TRoute::handle(
    //! Serveur ayant reçu la requête
    WebServerType &_serveur,
    //! Méthode HTTP de la requête
    HTTPMethod methode,
    //! Chemin demandé pour la requête
    String uri) {
    (void)_serveur;
    TServeur &serveur = FServeur;

    // S'assurer que cette route est la bonne pour la requête.
    if (!canHandle(methode, uri)) {
        return false;
    }

    // Si le client doit être authentifié
    if (Authentification()) {
        // Authentifier le client.
        // Si le client n'a pas pu être authentifié, retourné de la fonction.
        if (Authentifier(serveur) != 0)
            return true;
    }

    // Supprimer le contenu du document JSON.
    serveur.Document().clear();

    // Trouver la méthode associée à la méthode HTTP, l'appeler et retourner son
    // résultat.
    switch (methode) {
    case HTTP_GET:
        Get(serveur);
        break;
    case HTTP_POST:
        Post(serveur);
        break;
    case HTTP_PUT:
        Put(serveur);
        break;
    /*case HTTP_HEAD: //TODO voir https://github.com/esp8266/Arduino/issues/6412
        return Head(serveur);
        break;*/
    case HTTP_DELETE:
        Delete(serveur);
        break;
    case HTTP_PATCH:
        Patch(serveur);
        break;
    case HTTP_OPTIONS:
        Options(serveur);
        break;
    default:
        serveur.MethodeIncorrecte();
        break;
    }

    return true;
}

/**
 * @brief Vérifie si le client a l'autorisation d'accéder au serveur et lui
 * envoie une erreur dans le cas contraire.
 *
 * @return Un code d'erreur
 * @retval 0 Le client a pu être authentifier
 * @retval CODE_NON_AUTHORISE Le client n'a pas pu être authentifier
 */
int TRoute::Authentifier(
    //! Serveur qui reçoit les requêtes
    TServeur &serveur) {
    // Initialiser le code d'erreur au code d'accès non autorisé.
    int codeErreur = CODE_NON_AUTHORISE;

    // Obtenir la valeur de l'entête authentification
    String valeur = serveur.header(ENTETE_AUTHENTIFICATION);

    // Si le schéma d'authentification est le bon
    if (valeur.startsWith(SCHEMA_AUTHENTIFICATION)) {
        // Obtenir le mot de passe à partir de la valeur.
        // i.e. Sauter le nom du schéma d'authenification.
        const char *mdp = &valeur.c_str()[TAILLE_SCHEMA_AUTHENTIFICATION];

        // Si le mot de passe donné est le même que celui du super utilisateur
        // ou que la methode est GET, que le mot de passe de lecture est activé
        // et qu'il est le même que le mot de passe donné
        if (strcmp(mdp, MDP_SUPER_USAGER) == 0 ||
            (serveur.method() == HTTP_GET &&
             sizeof(MDP_LECTURE_SEULEMENT) > 1 &&
             strcmp(mdp, MDP_LECTURE_SEULEMENT) == 0)) {
            // Assigner le code d'accès autorisé au code d'erreur.
            codeErreur = 0;
        }
    }

    // Si l'accès n'est pas autorisé
    if (codeErreur != 0) {
        // Retourner le code d'accès non autorisé au client.
        serveur.send(CODE_NON_AUTHORISE);
    }

    return codeErreur;
}

/**
 * @brief Répond aux requêtes pour la méthode HTTP GET.
 * Par défaut, retourne une erreur de méthode incorrecte au client.
 *
 * @return Un code de statut HTTP
 */
int TRoute::Get(
    //! Serveur qui reçoit les requêtes
    TServeur &serveur) {
    // Retourner une erreur de méthode HTTP incorrecte.
    return serveur.MethodeIncorrecte();
}

/**
 * @brief Répond aux requêtes pour la méthode HTTP POST.
 * Par défaut, retourne une erreur de méthode incorrecte au client.
 *
 * @return Un code de statut HTTP
 */
int TRoute::Post(
    //! Serveur qui reçoit les requêtes
    TServeur &serveur) {
    // Retourner une erreur de méthode HTTP incorrecte.
    return serveur.MethodeIncorrecte();
}

/**
 * @brief Répond aux requêtes pour la méthode HTTP PUT.
 * Par défaut, retourne une erreur de méthode incorrecte au client.
 *
 * @return Un code de statut HTTP
 */
int TRoute::Put(
    //! Serveur qui reçoit les requêtes
    TServeur &serveur) {
    // Retourner une erreur de méthode HTTP incorrecte.
    return serveur.MethodeIncorrecte();
}

/**
 * @brief Répond aux requêtes pour la méthode HTTP HEAD.
 * Par défaut, retourne une erreur de méthode incorrecte au client.
 *
 * @return Un code de statut HTTP
 */
int TRoute::Head(
    //! Serveur qui reçoit les requêtes
    TServeur &serveur) {
    // Retourner une erreur de méthode HTTP incorrecte.
    return serveur.MethodeIncorrecte();
}

/**
 * @brief Répond aux requêtes pour la méthode HTTP DELETE.
 * Par défaut, retourne une erreur de méthode incorrecte au client.
 *
 * @return Un code de statut HTTP
 */
int TRoute::Delete(
    //! Serveur qui reçoit les requêtes
    TServeur &serveur) {
    // Retourner une erreur de méthode HTTP incorrecte.
    return serveur.MethodeIncorrecte();
}

/**
 * @brief Répond aux requêtes pour la méthode HTTP PATCH.
 * Par défaut, retourne une erreur de méthode incorrecte au client.
 *
 * @return Un code de statut HTTP
 */
int TRoute::Patch(
    //! Serveur qui reçoit les requêtes
    TServeur &serveur) {
    // Retourner une erreur de méthode HTTP incorrecte.
    return serveur.MethodeIncorrecte();
}

/**
 * @brief Répond aux requêtes pour la méthode HTTP OPTIONS.
 * Par défaut, retourne une erreur de méthode incorrecte
 * au client.
 *
 * @return Un code de statut HTTP
 */
int TRoute::Options(
    //! Serveur qui reçoit les requêtes
    TServeur &serveur) {
    // Retourner une erreur de méthode HTTP incorrecte.
    return serveur.MethodeIncorrecte();
}
