/**
 * @file TRouteImpl.cpp
 * @brief Déclaration et implémentation des différentes routes du serveur
 */

#include "TRouteImpl.h"
#include "../Config.h"

//=============================================================================
// Implémentation de TRouteCertificat
//=============================================================================

/**
 * @brief Initialise une nouvelle route de certificat avec le chemin donné
 */
TRouteCertificat::TRouteCertificat(
    //! Serveur
    TServeur &serveur,
    //! Chemin de la route
    const char *chemin) :
    TRoute(serveur, chemin, false) {
}

/**
 * @brief Répond un corps vide avec le code HTTP CODE_AUCUN_CONTENU à toutes les
 * requêtes reçues
 *
 * @return Un code de statut HTTP
 * @retval CODE_AUCUN_CONTENU
 */
int TRouteCertificat::Get(
    //! Serveur ayant reçu la requête.
    TServeur &serveur) {
    return serveur.EnvoyerCorpsVide();
}

//=============================================================================
// Implémentation de TRouteMode
//=============================================================================

/**
 * @brief Initialise une nouvelle route de mode avec le chemin donné
 *
 */
TRouteMode::TRouteMode(TGestionnaireMode &gestionnaireMode, TServeur &serveur,
                       const char *chemin) :
    TRoute(serveur, chemin),
    FGestionnaireMode(gestionnaireMode) {
}

/**
 * @brief Retourne le nom du mode sélectionné ou le code d'erreur
 * CODE_NON_TROUVE au client
 *
 * @return Un code de statut HTTP
 * @retval CODE_OK Le nom du mode sélectionné a été envoyé
 * @retval CODE_NON_TROUVE Aucun mode n'est en cours d'exécution
 */
int TRouteMode::Get(
    //! Serveur ayant reçu la requête.
    TServeur &serveur) {
    int codeErreur;

    const AMode *mode = FGestionnaireMode.ModeSelectionne();
    if (mode != nullptr) {
        codeErreur = serveur.EnvoyerTexte(mode->Nom());
    } else {
        codeErreur = serveur.EnvoyerTexte(
            "Aucun mode n'est en cours d'exécution", CODE_NON_TROUVE);
    }

    return codeErreur;
}

//=============================================================================
// Implémentation de TRouteCertificat
//=============================================================================

//! Chaîne de caractères pour une capteur dont l'unité le degrés Celsius
static const char *UNITE_CELSIUS = "C";
//! Chaîne de caractères pour une capteur dont l'unité l'humidité relative
//! en pourcentage
static const char *UNITE_HUMIDITE_RELATIVE = "HR";

//! Noms des capteurs.
static const char *NOMS_CAPTEURS[] = {
    NOM_CAPTEUR_1,
    NOM_CAPTEUR_2,
};

/**
 * @brief Initialise une nouvelle route de capteurs avec le chemin donné
 *
 */
TRouteCapteurs::TRouteCapteurs(
    //! Groupe de capteurs
    TGroupeCapteurs &groupeCapteurs,
    //! Serveur
    TServeur &serveur,
    //! Chemin de la route
    const char *chemin) :
    TRoute(serveur, chemin),
    FGroupeCapteurs(groupeCapteurs) {
}

/**
 * @brief Envoi la liste des capteurs et de leurs mesures au client
 *
 * @return Un code de statut HTTP
 * @retval CODE_OK
 */
int TRouteCapteurs::Get(
    //! Serveur ayant reçu la requête.
    TServeur &serveur) {
    JsonArray array = serveur.Document().to<JsonArray>();

    // Pour chaque capteur.
    int i = 0;
    for (TCapteur &capteur : FGroupeCapteurs.Capteurs) {
        // Obtenir le nom du capteur.
        const char *nom = NOMS_CAPTEURS[i];

        // Créer un nouvel objet pour la température.
        InsererCapteur(array, nom,
                       capteur.TemperatureCourtTerme.Sommaire().Moyenne,
                       UNITE_CELSIUS);

        // Créer un nouvel objet pour l'humidité.
        InsererCapteur(array, nom,
                       capteur.HumiditeCourtTerme.Sommaire().Moyenne,
                       UNITE_HUMIDITE_RELATIVE);

        // Incrémenter l'index du capteur.
        i++;
    }

    // Envoyer le document.
    return serveur.EnvoyerDocument();
}

/**
 * @brief Créé et insère un objet de capteur avec les caractéristiques données
 * dans l'array fournie
 *
 * @return L'objet inséré
 */
JsonObject TRouteCapteurs::InsererCapteur(
    //! Objet où insérer les données du capteur.
    JsonArray &array,
    //! Nom du capteur.
    const char *nom,
    //! Mesure du capteur
    TMesureCapteur mesure,
    //! Unité du capteur
    const char *unite) {
    // Créer un nouvel objet dans l'array.
    JsonObject obj = array.createNestedObject();

    // Ajouter le nom du capteur.
    obj["nom"] = nom;

    // Ajouter la mesure du capteur.
    obj["mesure"] = mesure;

    // Ajouter l'unité du capteur.
    obj["unite"] = unite;

    // Retourner l'objet.
    return obj;
}

//=============================================================================
// Implémentation de TRouteActionneurs
//=============================================================================

//-----------------------------------------------------------------------------
// Types d'actionneurs
//-----------------------------------------------------------------------------
//! Nom du type d'un moteur
static const char *TYPE_MOTEUR = "moteur";
//! Nom du type d'un ventilateur
static const char *TYPE_VENTILATION = "ventilation";
//! Nom du type d'une fournaise
static const char *TYPE_CHAUFFAGE = "chauffage";

//-----------------------------------------------------------------------------
// Noms des actionneurs
//-----------------------------------------------------------------------------

//! Nom du type d'un moteur
static const char *NOMS_MOTEURS[NB_MOTEURS] = {
    NOM_ROLLUP_1,
    NOM_ROLLUP_2,
};

//-----------------------------------------------------------------------------
// États des actionneurs formattés
//-----------------------------------------------------------------------------
//! Noms des directions de moteur
static const char *DIRECTION_MOTEUR[] = {"neutre", "ouvrir", "fermer"};

/**
 * @brief Initialise une nouvelle route d'actionneurs au chemin donné
 *
 */
TRouteActionneurs::TRouteActionneurs(
    //! Gestionaire de mode
    TGestionnaireMode &gestionnaireMode,
    //! Mode de contrôle à distance
    TModeControleDistance &modeCtrlDistance,
    //! Moteurs des rollups
    TSelectionMoteur &selectionMoteur,
    //! Ventilateur
    TVentilateur &ventilateur,
    //! Fournaise
    TFournaise &fournaise,
    //! Serveur
    TServeur &serveur,
    //! Chemin de la route
    const char *chemin) :
    TRoute(serveur, chemin),
    FGestionnaireMode(gestionnaireMode), FModeCtrlDistance(modeCtrlDistance),
    FSelectionMoteur(selectionMoteur), FVentilateur(ventilateur),
    FFournaise(fournaise) {
}

/**
 * @brief Retourne un JSON array avec les propriétés de tous les actionneurs au
 * client.
 *
 * @return Un code de statut HTTP
 * @retval CODE_OK
 */
int TRouteActionneurs::Get(
    //! Serveur ayant reçu la requête
    TServeur &serveur) {
    JsonArray array = serveur.Document().to<JsonArray>();

    // Pour chaque moteur
    for (int i = 0; i < NB_MOTEURS; i++) {
        TMoteur &moteur = FSelectionMoteur.Moteurs[i];

        // Insérer l'actionneur dans l'array.
        JsonObject obj =
            InsererActionneur(array, TYPE_MOTEUR, NOMS_MOTEURS[i],
                              String(DIRECTION_MOTEUR[moteur.Etat()]));

        // Ajouter la position du moteur à l'objet.
        obj["position"] = moteur.Position();
    }

    // Insérer le ventilateur dans l'array.
    InsererActionneur(array, TYPE_VENTILATION, NOM_VENTILATEUR,
                      FVentilateur.Etat());

    // Insérer le ventilateur dans l'array.
    InsererActionneur(array, TYPE_CHAUFFAGE, NOM_FOURNAISE, FFournaise.Etat());

    // Envoyer le document.
    return serveur.EnvoyerDocument();
}

/**
 * @brief Retourne un array JSON avec les propriétés de tous les actionneurs au
 * client.
 *
 * @return Un code de statut HTTP
 * @retval CODE_AUCUN_CONTENU Aucune erreur n'est survenue
 * @retval CODE_CONFLIT Le mode manuel est activé
 * @retval CODE_REQUETE_INVALIDE Aucune erreur n'est survenue
 */
int TRouteActionneurs::Post(TServeur &serveur) {
    int codeErreur = 0;

    // S'assurer que le mode manuel n'est pas activé.
    const AMode *mode = FGestionnaireMode.ModeSelectionne();
    if (mode != nullptr) {
        if (strcmp(mode->Nom(), TModeCalibration::NOM_MODE) == 0 ||
            strcmp(mode->Nom(), TModeManuel::NOM_MODE) == 0) {
            codeErreur = serveur.EnvoyerTexte(
                "Le mode de contrôle à distance ne peut pas être activé si le "
                "mode de calibration ou le mode manuel est sélectionné",
                CODE_CONFLIT);
        }
    }

    // Lire le document JSON.
    if (codeErreur == 0) {
        if (serveur.RecevoirDocument() != 0) {
            codeErreur =
                serveur.EnvoyerTexte("Le document JSON n'a pas pu être décodé",
                                     CODE_REQUETE_INVALIDE);
        }
    }

    // Obtenir et valider l'état des actionneurs dans le document JSON.
    bool ventilateur = FVentilateur.Etat();
    bool fournaise = FFournaise.Etat();

    uint8_t positionsMoteurs[NB_MOTEURS];
    for (uint8_t i = 0; i < NB_MOTEURS; i++) {
        positionsMoteurs[i] = FSelectionMoteur.Moteurs[i].Position();
    }

    if (codeErreur == 0) {
        JsonArray array = serveur.Document().as<JsonArray>();
        for (auto it = array.begin(); it != array.end() && codeErreur == 0;
             ++it) {
            JsonObject obj = it->as<JsonObject>();

            const char *type = obj["type"];
            // Si l'actionneur est un moteur
            if (strcmp(type, "moteur") == 0) {
                uint8_t position = obj["position"];

                const char *nom = obj["nom"];
                size_t indexMoteur = 0;
                while (strcmp(NOMS_MOTEURS[indexMoteur], nom) != 0 &&
                       indexMoteur < NB_MOTEURS) {
                    indexMoteur++;
                }
                if (indexMoteur < NB_MOTEURS) {
                    positionsMoteurs[indexMoteur] = position;
                } else {
                    codeErreur = serveur.EnvoyerTexte(
                        "Un de moteurs donné a un nom invalide",
                        CODE_REQUETE_INVALIDE);
                }
            }
            // Sinon c'est un actionneur booléen
            else {
                // NOTE: Le nom est ignoré pour les actionneurs booléens,
                // puisqu'il n'y en a que 1 de chaque type pour l'instant.

                bool etat = obj["etat"];
                if (strcmp(type, "ventilation") == 0) {
                    ventilateur = etat;
                } else if (strcmp(type, "chauffage") == 0) {
                    fournaise = etat;
                } else {
                    codeErreur = serveur.EnvoyerTexte(
                        "Le type d'un des actionneurs est invalide",
                        CODE_REQUETE_INVALIDE);
                }
            }
        }
    }

    // Appliquer les nouveaux états pour les actionneurs.
    if (codeErreur == 0) {
        if (FModeCtrlDistance.Controler(fournaise, ventilateur,
                                        positionsMoteurs) == 0) {
            codeErreur = serveur.EnvoyerCorpsVide();
        } else {
            codeErreur = serveur.EnvoyerTexte(
                "Au moins une des positions de moteurs donnée est invalide",
                CODE_REQUETE_INVALIDE);
        }
    }

    return codeErreur;
}

/**
 * @brief Désactive le mode de contrôle à distance s'il est activé
 *
 * @return Un code de statut HTTP
 * @retval CODE_AUCUN_CONTENU Aucune erreur n'est survenue
 * @retval CODE_CONFLIT Le mode de contrôle à distance n'était pas activé
 */
int TRouteActionneurs::Delete(TServeur &serveur) {
    int codeErreur;
    if (FModeCtrlDistance.Activer) {
        FModeCtrlDistance.Activer = false;
        codeErreur = serveur.EnvoyerCorpsVide();
    } else {
        codeErreur = serveur.EnvoyerTexte(
            "Le mode de contrôle à distance n'était pas activé", CODE_CONFLIT);
    }

    return codeErreur;
}

/**
 * @brief Créer un objet d'actionneur dans l'array donné et configure ses
 * propriétés
 *
 * @tparam TEtat Type de l'état de l'actionneur
 * @return L'objet d'actionneur créé
 */
template <typename TEtat>
JsonObject TRouteActionneurs::InsererActionneur(
    //! Array dans laquelle insérer l'actionneur
    JsonArray &array,
    //! Type de l'actionneur
    const char *type,
    //! Nom de l'actionneur
    const char *nom,
    //! État de l'actionneur
    TEtat etat) {
    // Créer un nouvel objet dans l'array.
    JsonObject obj = array.createNestedObject();

    // Ajouter le type de l'actionneur.
    obj["type"] = type;

    // Ajouter le nom de l'actionneur.
    obj["nom"] = nom;

    // Ajouter l'état de l'actionneur.
    obj["etat"] = etat;

    return obj;
}

//=============================================================================
// Implémentation de TRouteAlertes
//=============================================================================

/**
 * @brief Initialise une nouvelle route d'alertes avec le chemin donné
 *
 */
TRouteAlertes::TRouteAlertes(
    //! Gestionnaire d'alertes.
    TAlertesSynchronisees &alertes,
    //! Serveur
    TServeur &serveur,
    //! Chemin de la route
    const char *chemin) :
    TRoute(serveur, chemin),
    FAlertes(alertes) {
}

/**
 * @brief Envoi la liste des alertes survenues au client
 *
 * @return Un code de statut HTTP
 */
int TRouteAlertes::Get(
    //! Serveur ayant reçu la requête
    TServeur &serveur) {
    uint64_t depuis = 0;
    // Si les alertes sont synchronisées, filtrer les alertes à l'aide de
    // l'argument `depuis` passé au serveur.
    if (FAlertes.EstSynchronise()) {
        // Décoder l'argument `depuis`.
        String sDepuis = serveur.arg("depuis");
        const char *cDepuis = sDepuis.c_str();
        while (*cDepuis != 0 && *cDepuis >= '0' && *cDepuis <= '9') {
            depuis *= 10;
            depuis += (*cDepuis - '0');
            cDepuis++;
        }
    }

    JsonArray array = serveur.Document().to<JsonArray>();
    auto fin = FAlertes.Alertes().end();
    for (auto alerte = FAlertes.AlertesDepuis(depuis); alerte != fin;
         alerte++) {
        JsonObject alertesJson = array.createNestedObject();
        alertesJson["titre"] = alerte->Titre.c_str();
        alertesJson["message"] = alerte->Message.c_str();
        alertesJson["urgence"] = NIVEAUX_URGENCES[alerte->Urgence()];
        alertesJson["date"] = FAlertes.EstSynchronise() ? alerte->Date() : 0;
    }
    return serveur.EnvoyerDocument();
}

//=============================================================================
// Implémentation de TRouteTypeJour
//=============================================================================

#include "../TConsigne/TConsigne.h"

//! Nombre de types de jours
#define NB_TYPE_JOUR 3
//! Noms des types de jour
static const char *TYPE_JOUR[NB_TYPE_JOUR] = {
    "ensoleille",
    "mi-nuageux",
    "nuageux",
};

/**
 * @brief Initialise une nouvelle route de type de jour avec le chemin donné
 *
 */
TRouteTypeJour::TRouteTypeJour(
    //! Fournisseur de consigne
    TFournisseurConsigne &fournisseurConsigne,
    //! Serveur
    TServeur &serveur,
    //! Chemin de la route.
    const char *chemin) :
    TRoute(serveur, chemin),
    FFournisseurConsigne(fournisseurConsigne) {
}

/**
 * @brief Envoi le nom du type de jour sélectionné pour l'asservissement
 *
 * @return Un code de statut HTTP
 * @retval CODE_OK
 */
int TRouteTypeJour::Get(
    //! Serveur ayant reçu la requête
    TServeur &serveur) {
    return serveur.EnvoyerTexte(TYPE_JOUR[FFournisseurConsigne.TypeJour()]);
}

/**
 * @brief Envoi la liste des alertes survenues au client
 *
 * @return Un code de statut HTTP
 * @retval CODE_AUCUN_CONTENU Aucune erreur n'est survenue
 * @retval CODE_NON_TROUVE Le type de jour est invalide
 */
int TRouteTypeJour::Post(
    //! Serveur ayant reçu la requête
    TServeur &serveur) {
    // Obtenir l'index du type de jour reçu
    String typeJourRecu = serveur.arg("plain");
    uint8_t indexTypeJour = 0;
    while (indexTypeJour < NB_TYPE_JOUR &&
           typeJourRecu != TYPE_JOUR[indexTypeJour])
        indexTypeJour++;

    int codeRetour;
    // Si le type de jour reçu est valide
    if (indexTypeJour < NB_TYPE_JOUR) {
        FFournisseurConsigne.TypeJour(indexTypeJour);
        codeRetour = serveur.EnvoyerCorpsVide();
    } else {
        codeRetour =
            serveur.EnvoyerTexte("Type de jour invalide", CODE_NON_TROUVE);
    }

    return codeRetour;
}

//=============================================================================
// Implémentation de TRouteConsignes
//=============================================================================

//! Noms des propriétés JSON des différentes périodes de la journée.
const char *NOMS_PERIODES_JOUR[NB_CONSIGNES_JOUR] = {
    "prejour",
    "jour",
    "prenuit",
    "nuit",
};

/**
 * @brief Initialise une nouvelle route de consignes avec le chemin donné
 *
 */
TRouteConsignes::TRouteConsignes(
    //! Fournisseur de consigne
    TFournisseurConsigne &fournisseurConsigne,
    //! Serveur
    TServeur &serveur,
    //! Chemin de la route.
    const char *chemin) :
    TRoute(serveur, chemin),
    FFournisseurConsigne(fournisseurConsigne) {
}

/**
 * @brief Retourne un objet JSON content les consignes pour chaque type de jour
 * au client
 *
 * @return Un code de statut HTTP
 * @retval CODE_OK
 */
int TRouteConsignes::Get(
    //! Serveur ayant reçu la requête.
    TServeur &serveur) {
    JsonObject objet = serveur.Document().to<JsonObject>();

    // Insérer les températures.
    for (int i = 0; i < NB_TYPES_JOUR; i++) {
        TJour &jour = FFournisseurConsigne.TypesJour[i];
        InsererJour(objet, TYPE_JOUR[i], jour);
    }

    // Insérer les heures des périodes de la journée.
    JsonObject objHeures = objet.createNestedObject("heures");
    const THeurePeriode *heures = FFournisseurConsigne.Heures();
    for (int i = 0; i < NB_CONSIGNES_JOUR; i++) {
        objHeures[NOMS_PERIODES_JOUR[i]] = heures[i].MinutesTotales();
    }

    TConsigne &consigne = FFournisseurConsigne.TypesJour[0].Jour;
    objet["hysteresis"] = consigne.Hysteresis;
    objet["variation-critique"] = consigne.TempMaxCritique - consigne.TempCible;

    // Envoyer le document.
    return serveur.EnvoyerDocument();
}

/**
 * @brief Reçoit un objet JSON du client contenant un nouvelle configuration
 * pour les consignes et l'applique.
 *
 * @return Un code de statut HTTP
 * @retval CODE_AUCUN_CONTENU Aucune erreur n'est survenue
 * @retval CODE_REQUETE_INVALIDE Le type de jour donné n'existe pas
 */
int TRouteConsignes::Post(TServeur &serveur) {
    int codeRetour = serveur.RecevoirDocument();
    if (codeRetour == 0) {
        TServeur::TJsonDoc &doc = serveur.Document();
        if (doc.containsKey(TYPE_JOUR[0]) && doc.containsKey(TYPE_JOUR[1]) &&
            doc.containsKey(TYPE_JOUR[2])) {
            // Obtenir les consignes.
            TJour cibles[NB_TYPE_JOUR];
            for (int i = 0; i < NB_TYPES_JOUR; i++) {
                // Changer les consigne de l'asservissement
                auto jour = doc[TYPE_JOUR[i]];

                TJour &cible = cibles[i];
                for (int i = 0; i < NB_CONSIGNES_JOUR; i++) {
                    cible.Consignes[i].TempCible = jour[NOMS_PERIODES_JOUR[i]];
                }
            }

            // Obtenir les heures des périodes de la journée.
            auto objHeures = doc["heures"];
            THeurePeriode heures[NB_CONSIGNES_JOUR];
            for (int i = 0; i < NB_CONSIGNES_JOUR; i++) {
                heures[i] =
                    THeurePeriode((uint16_t)objHeures[NOMS_PERIODES_JOUR[i]]);
            }

            TMesureCapteur hysteresis = doc["hysteresis"];
            TMesureCapteur variation = doc["variation-critique"];

            if (FFournisseurConsigne.ChangerConsignes(
                    cibles, heures, hysteresis, variation) == 0) {
                codeRetour = serveur.EnvoyerCorpsVide();
            } else {
                codeRetour = serveur.EnvoyerTexte(
                    "L'hystérésis doit être inférieure à la variation critique",
                    CODE_REQUETE_INVALIDE);
            }
        } else {
            codeRetour = serveur.EnvoyerTexte(
                "Le type de jour donné n'existe pas", CODE_REQUETE_INVALIDE);
        }
    }
    return codeRetour;
}

/**
 * @brief Créer et insère un objet JSON de jour avec les caractéristiques
 * données dans l'array fournies
 *
 * @return L'objet JSON créé
 */
JsonObject TRouteConsignes::InsererJour(
    //! Objet où insérer le jour
    JsonObject &obj,
    //! Nom du type de jour
    const char *typeJour,
    //! Consigne de prejour
    TJour &jour) {
    JsonObject jourJson = obj.createNestedObject(typeJour);
    for (int i = 0; i < NB_CONSIGNES_JOUR; i++) {
        jourJson[NOMS_PERIODES_JOUR[i]] = jour.Consignes[i].TempCible;
    }
    return jourJson;
}

//=============================================================================
// Implémentation de TRouteCycle
//=============================================================================

/**
 * @brief Initialise une nouvelle route de cycle avec le chemin donné
 *
 */
TRouteCycle::TRouteCycle(
    //! Fournisseur de consigne
    TFournisseurConsigne fournisseurConsigne,
    //! Tâche de cycle
    TModeCycle &modeCycle,
    //! Gestionnaire de mode
    TGestionnaireMode &gestionnaireMode,
    //! Serveur
    TServeur &serveur,
    //! Chemin de la route.
    const char *chemin) :
    TRoute(serveur, chemin),
    FFournisseurConsigne(fournisseurConsigne), FModeCycle(modeCycle),
    FGestionnaireMode(gestionnaireMode) {
}

/**
 * @brief Retourne la configuration et la progression du cycle en cours au
 * client ou un code d'erreur si aucun cycle n'est en cours d'exécution.
 *
 * @return Un code de statut HTTP
 * @retval CODE_OK Un cycle est en cours
 * @retval CODE_NON_TROUVE Aucun cycle n'est en cours d'exécution
 */
int TRouteCycle::Get(
    //! Serveur ayant reçu la requête.
    TServeur &serveur) {
    int codeRetour;

    // Si un cycle est en exécution.
    if (FModeCycle.Cycle.Repetitions > 0) {
        JsonObject objet = serveur.Document().to<JsonObject>();

        // Créer l'objet de cycle.
        JsonObject cycle = objet.createNestedObject("cycle");
        cycle["repetitions"] = FModeCycle.Cycle.Repetitions;
        cycle["duree-chauffage"] = FModeCycle.Cycle.DureeChauffage;
        cycle["duree-ventilation"] = FModeCycle.Cycle.DureeVentilation;

        // Créer l'objet de progression.
        JsonObject progression = objet.createNestedObject("progression");
        progression["repetition"] = FModeCycle.Repetition();
        progression["etape"] = FModeCycle.EtapeActuelle();
        progression["duree-chauffage"] = FModeCycle.TempsEcoule(ecChauffage);
        progression["duree-ventilation"] =
            FModeCycle.TempsEcoule(ecVentilation);

        // Envoyer le document JSON.
        codeRetour = serveur.EnvoyerDocument();
    } else {
        // Retourner une erreur au client.
        codeRetour = serveur.EnvoyerTexte("Aucun cycle n'est en exécution",
                                          CODE_NON_TROUVE);
    }

    return codeRetour;
}

/**
 * @brief Reçoit une nouvelle configuration pour un cycle dans un objet JSON et
 * l'exécute.
 *
 * @return Un code de statut HTTP
 * @retval CODE_AUCUN_CONTENU Aucune erreur n'est survenue, le cycle a pu être
 * appliqué
 * @retval CODE_REQUETE_INVALIDE Les données reçues sont invalides.
 * @retval CODE_CONFLIT Le contrôleur est en mode manuel et ne peut donc pas
 * répondre à la requête.
 */
int TRouteCycle::Post(TServeur &serveur) {
    int codeRetour = 0;

    // S'assurer que le mode manuel et le mode de
    // contrôle à distance ne soient pas activés
    const AMode *mode = FGestionnaireMode.ModeSelectionne();
    if (mode != nullptr) {
        const char *nomMode = mode->Nom();
        if (strcmp(nomMode, TModeAsservissement::NOM_MODE) != 0 &&
            strcmp(nomMode, TModeCycle::NOM_MODE) != 0) {
            codeRetour = serveur.EnvoyerTexte(
                "Le contrôleur peut uniquement exécuter un cycle s'il était en "
                "mode d'asservissement",
                CODE_CONFLIT);
        }
    }

    // Recevoir le document JSON.
    if (codeRetour == 0) {
        codeRetour = serveur.RecevoirDocument();
    }

    // Valider et appliquer le cycle reçu.
    if (codeRetour == 0) {
        TServeur::TJsonDoc &doc = serveur.Document();

        // Obtenir la première étape et valider.
        const char *sPremiereEtape = doc["premiere-etape"];
        TEtapeCycle premiereEtape = ecVentilation;
        if (strcmp(sPremiereEtape, "chauffage") == 0) {
            premiereEtape = ecChauffage;
        } else if (strcmp(sPremiereEtape, "ventilation") != 0) {
            codeRetour = serveur.EnvoyerTexte(
                "La première étape donnée est invalide", CODE_REQUETE_INVALIDE);
        }

        if (codeRetour == 0) {
            // Si le nombre de cycles de répétition est valide.
            int nbRepetitions = doc["repetitions"];
            if (nbRepetitions > 0) {
                // Appliquer le cycle reçu.
                FModeCycle.NouveauCycle(nbRepetitions);
                FModeCycle.Cycle.DureeChauffage = doc["duree-chauffage"];
                FModeCycle.Cycle.DureeVentilation = doc["duree-ventilation"];
                FModeCycle.EtapeDepart = premiereEtape;

                // Envoyer le code de réussite au client.
                codeRetour = serveur.EnvoyerCorpsVide();
            } else {
                codeRetour = serveur.EnvoyerTexte(
                    "Le nombre de répétitions donné est négatif",
                    CODE_REQUETE_INVALIDE);
            }
        }
    }

    return codeRetour;
}

/**
 * @brief Permet au client d'arrêter le cycle en cours.
 *
 * @return Un code de statut HTTP
 * @retval CODE_AUCUN_CONTENU Le cycle a été arrêté
 * @retval CODE_NON_TROUVE Aucun cycle n'était en cours d'exécution
 */
int TRouteCycle::Delete(
    //! Serveur ayant reçu la requête.
    TServeur &serveur) {
    int codeErreur;

    // Vérifier l'éxécution d'un cycle l'arêter si il y'en a. Envoyer une
    // erreur si non
    if (FModeCycle.Cycle.Repetitions == 0) {
        codeErreur = serveur.EnvoyerTexte("Aucun cycle n'était en exécution",
                                          CODE_NON_TROUVE);
    } else if (FModeCycle.Cycle.Repetitions > 0) {
        // Arrêter le cycle.
        FModeCycle.NouveauCycle(0);
        codeErreur = serveur.EnvoyerCorpsVide();
    }

    return codeErreur;
}
