#include "../../Broches/Broches.h"
#include <DHTesp.h>

#define NB_CAPTEURS 2
DHTesp capteurs[sizeof(CAPTEURS) / sizeof(int)];

void setup() {
    Serial.begin(115200);
    Serial.println();
    Serial.println("Broche\tStatus\tHumidity (%)\tTemperature "
                   "(C)\t(F)\tHeatIndex (C)\t(F)");
    Serial.println(ARDUINO_BOARD);

    // Initialiser chaque capteur.
    for (int i = 0; i < NB_CAPTEURS; i++)
        capteurs[i].setup(CAPTEURS[i], DHTesp::DHT22);
}

void loop() {
    delay(capteurs[0].getMinimumSamplingPeriod());

    // Obtenir et afficher les données de chaque capteur
    for (int i = 0; i < NB_CAPTEURS; i++) {
        DHTesp &dht = capteurs[i];

        float humidity = dht.getHumidity();
        float temperature = dht.getTemperature();

        Serial.print(CAPTEURS[i]);
        Serial.print("\t");
        Serial.print(dht.getStatusString());
        Serial.print("\t");
        Serial.print(humidity, 1);
        Serial.print("\t\t");
        Serial.print(temperature, 1);
        Serial.print("\t\t");
        Serial.print(dht.toFahrenheit(temperature), 1);
        Serial.print("\t\t");
        Serial.print(dht.computeHeatIndex(temperature, humidity, false), 1);
        Serial.print("\t\t");
        Serial.println(
            dht.computeHeatIndex(dht.toFahrenheit(temperature), humidity, true),
            1);
    }
}
