#include "../../Broches/Broches.h"

//! Liste des broches à tester
static const int BROCHES[] = {
    DIRECTION_MOTEURS[0], DIRECTION_MOTEURS[1], SELECTION_MOTEUR, CHAUFFAGE,
    VENTILATION,
};

//! Délai en ms entre le moment où le relais est ouvert et celui où le relais
//! est fermé
static const int DELAI = 200;

//! Niveau logique d'un relais éteint
static const int ETEINT = LOW;
//! Niveau logique d'un relais allumé
static const int ALLUME = HIGH;

void setup() {
    // Configurer toutes les broches en sortie et éteindre tous les relais.
    for (int broche : BROCHES) {
        pinMode(broche, OUTPUT);
        digitalWrite(broche, ETEINT);
    }
}

void loop() {
    // Allumer puis éteindre chaque relais avec un délai.
    for (int broche : BROCHES) {
        digitalWrite(broche, ALLUME);
        delay(DELAI);
        digitalWrite(broche, ETEINT);
        delay(DELAI);
    }
}
