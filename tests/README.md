# Tests

Ce répertoire contient tous les programmes de tests à effectuer sur le prototype. Pour plus de détail sur chaque test, lisez la section du README associée au test.

Après l'assemblage d'un prototype, ils peuvent être exécutés dans cet ordre:

1. [Capteurs](#capteurs)
2. [ControleRelais](#contrôle-des-relais)
3. [ControleManuel](#contrôle-manuel)

## Capteurs

Ce test est basé sur l'exemple de la librairie [`DHTesp`](https://github.com/beegee-tokyo/DHTesp), mais il est fait pour tester les deux capteurs en même temps. Il lit la température, l'humidité et l'état des deux capteurs et les affichent avec leur numéro de broche pour les différencier.

## Contrôle des relais

Ce test allume et éteint les relais les uns après les autres. Vous pouvez d'abord l'utiliser pour tester le fonctionnement de chacune des sorties avant même que les relais soient connectés en vérifiant la tension aux sorties. Vous pouvez ensuite souder les relais et réexecuter le test afin de vous assurez que les relais entre en commutation les uns après les autres, et jamais en même temps.

## Contrôle Manuel

Ce test est une version simplifiée du mode de contrôle manuel. Il a été conçu pour être exécuté une fois que le prototype est complètement soudé. Il permet de s'assurer que toutes les entrées et sorties fonctionnent.
