/**
 * @file TGroupeCapteurs.h
 * @brief Déclaration de TGroupeCapteurs
 */

#ifndef TGroupeCapteurs_h
#define TGroupeCapteurs_h

#include "../ATachePeriodique/ATachePeriodique.h"
#include "../TCapteur/TCapteur.h"

//! Nombre de capteurs dans un groupe de capteurs
#define NB_CAPTEURS 2

//! Possède tous les capteurs, gère leur exécution et fait le sommaire de leurs
//! résultats
class TGroupeCapteurs : public ATachePeriodique {
private:
    //! Index du prochain capteur à lire.
    uint8_t FIdxCapteur;

public:
    //! Capteurs du groupe
    TCapteur (&Capteurs)[NB_CAPTEURS];

    TGroupeCapteurs(TCapteur (&capteurs)[NB_CAPTEURS], TTemps periode);

    TSommaireMesures SommaireTemperatureCourtTerme();
    TSommaireMesures SommaireHumiditeCourtTerme();

protected:
    int ExecuterPeriodique();
};

#endif
