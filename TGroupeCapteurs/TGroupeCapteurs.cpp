/**
 * @file TGroupeCapteurs.cpp
 * @brief Implémentation de TGroupeCapteurs
 */

#include "TGroupeCapteurs.h"

/**
 * @brief Initialise le groupe de capteur avec les broches de capteurs et la
 * période donnée
 *
 */
TGroupeCapteurs::TGroupeCapteurs(
    //! Liste de capteurs à utiliser pour le groupe
    TCapteur (&capteurs)[NB_CAPTEURS],
    //! Intervalle de lecture des capteurs
    TTemps periode) :
    ATachePeriodique(periode / NB_CAPTEURS),
    FIdxCapteur(0), Capteurs(capteurs) {
}

/**
 * @brief Combine le sommaire de température de tous les capteurs
 *
 * @return Le sommaire des température à court terme
 */
TSommaireMesures TGroupeCapteurs::SommaireTemperatureCourtTerme() {
    TSommaireMesures sommaires[NB_CAPTEURS];

    // Pour chaque capteur
    for (int i = 0; i < NB_CAPTEURS; i++)
        // Obtenir le sommaire de température à court terme.
        sommaires[i] = Capteurs[i].TemperatureCourtTerme.Sommaire();

    // Combiner tous les sommaires et retourner le résultat.
    return TSommaireMesures(sommaires, NB_CAPTEURS);
}

/**
 * @brief Combine le sommaire d'humidité de tous les capteurs
 *
 * @return Le sommaire d'humidité à court terme
 */
TSommaireMesures TGroupeCapteurs::SommaireHumiditeCourtTerme() {
    TSommaireMesures sommaires[NB_CAPTEURS];

    // Pour chaque capteur
    for (int i = 0; i < NB_CAPTEURS; i++)
        // Obtenir le sommaire d'humidité à court terme.
        sommaires[i] = Capteurs[i].HumiditeCourtTerme.Sommaire();

    // Combiner tous les sommaires et retourner le résultat.
    return TSommaireMesures(sommaires, NB_CAPTEURS);
}

/**
 * @brief Lit les valeurs des capteurs
 *
 * @return Un code d'erreur
 * @retval 0 Aucune erreur
 * @retval * Voir TCapteur::Lire
 */
int TGroupeCapteurs::ExecuterPeriodique() {
    if (FIdxCapteur >= NB_CAPTEURS) {
        FIdxCapteur = 0;
    }
    int codeErreur = Capteurs[FIdxCapteur].Lire() != 0;
    FIdxCapteur++;

    return codeErreur;
}
